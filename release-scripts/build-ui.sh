#!/usr/bin/env bash
set -e

version="$1"

# Directory where the binaries produces by build-release.sh are stored.
binDir="$2"

# Directory of the I3v-UI Repo.
uiDir="$3"
if [ -z "$version" ] || [ -z "$binDir" ] || [ -z "$uiDir" ]; then
  echo "Usage: $0 VERSION BIN_DIRECTORY UI_DIRECTORY"
  exit 1
fi

echo Version: "${version}"
echo Binaries Directory: "${binDir}"
echo UI Directory: "${uiDir}"
echo ""

if [ "$I3V_SILENT_RELEASE" != "true" ]; then
	read -p "Continue (y/n)?" CONT
	if [ "$CONT" != "y" ]; then
		exit 1
	fi
fi
echo "Building I3v-UI...";

# Get the absolute paths to avoid funny business with relative paths.
uiDir=$(realpath "${uiDir}")
binDir=$(realpath "${binDir}")

# Remove previously built UI binaries.
rm -r "${uiDir}"/release/

cd "${uiDir}"

# Copy over all the i3vc/i3vd binaries.
mkdir -p bin/{linux,mac,win}
cp "${binDir}"/I3v-"${version}"-darwin-amd64/i3vc bin/mac/
cp "${binDir}"/I3v-"${version}"-darwin-amd64/i3vd bin/mac/

cp "${binDir}"/I3v-"${version}"-linux-amd64/i3vc bin/linux/
cp "${binDir}"/I3v-"${version}"-linux-amd64/i3vd bin/linux/

cp "${binDir}"/I3v-"${version}"-windows-amd64/i3vc.exe bin/win/
cp "${binDir}"/I3v-"${version}"-windows-amd64/i3vd.exe bin/win/

# Build yarn deps.
yarn

# Build each of the UI binaries.
yarn package-linux
yarn package-win
yarn package

# Copy the UI binaries into the binDir. Also change the name at the same time.
# The UI builder doesn't handle 4 digit versions correctly and if the UI hasn't
# been updated the version might be stale.
for ext in AppImage dmg exe; do 
  mv "${uiDir}"/release/*."${ext}" "${binDir}"/I3v-UI-"${version}"."${ext}"
	(
		cd "${binDir}"
		sha256sum I3v-UI-"${version}"."${ext}" >> I3v-"${version}"-SHA256SUMS.txt
	)
done
