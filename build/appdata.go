package build

import (
	"os"
	"path/filepath"
	"runtime"
)

// DefaultI3vDir returns the default data directory of i3vd. The values for
// supported operating systems are:
//
// Linux:   $HOME/.i3v
// MacOS:   $HOME/Library/Application Support/I3v
// Windows: %LOCALAPPDATA%\I3v
func DefaultI3vDir() string {
	switch runtime.GOOS {
	case "windows":
		return filepath.Join(os.Getenv("LOCALAPPDATA"), "I3v")
	case "darwin":
		return filepath.Join(os.Getenv("HOME"), "Library", "Application Support", "I3v")
	default:
		return filepath.Join(os.Getenv("HOME"), ".i3v")
	}
}

// APIPasswordFile returns the path to the API's password file given a I3v
// directory.
func APIPasswordFile(i3vDir string) string {
	return filepath.Join(i3vDir, "apipassword")
}
