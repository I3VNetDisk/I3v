package i3vtest

import (
	"gitlab.com/I3VNetDisk/I3v/modules"
)

type (
	// RemoteDir is a helper struct that represents a directory on the I3v
	// network.
	RemoteDir struct {
		i3vpath modules.I3vPath
	}
)

// I3vPath returns the i3vpath of a remote directory.
func (rd *RemoteDir) I3vPath() modules.I3vPath {
	return rd.i3vpath
}
