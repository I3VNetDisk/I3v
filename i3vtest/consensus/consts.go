package consensus

import (
	"os"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/I3v/i3vtest"
)

// consensusTestDir creates a temporary testing directory for a consensus. This
// should only every be called once per test. Otherwise it will delete the
// directory again.
func consensusTestDir(testName string) string {
	path := i3vtest.TestDir("consensus", testName)
	if err := os.MkdirAll(path, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return path
}
