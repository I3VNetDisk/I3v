package i3vtest

import (
	"os"
	"path/filepath"

	"gitlab.com/I3VNetDisk/I3v/persist"
)

var (
	// I3vTestingDir is the directory that contains all of the files and
	// folders created during testing.
	I3vTestingDir = filepath.Join(os.TempDir(), "I3vTesting")
)

// TestDir joins the provided directories and prefixes them with the I3v
// testing directory, removing any files or directories that previously existed
// at that location.
func TestDir(dirs ...string) string {
	path := filepath.Join(I3vTestingDir, "i3vtest", filepath.Join(dirs...))
	err := os.RemoveAll(path)
	if err != nil {
		panic(err)
	}
	return path
}

// i3vtestTestDir creates a testing directory for tests within the i3vtest
// module.
func i3vtestTestDir(testName string) string {
	path := TestDir("i3vtest", testName)
	if err := os.MkdirAll(path, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return path
}

// DownloadDir returns the LocalDir that is the testnodes download directory
func (tn *TestNode) DownloadDir() *LocalDir {
	return tn.downloadDir
}

// RenterDir returns the renter directory for the renter
func (tn *TestNode) RenterDir() string {
	return filepath.Join(tn.Dir, "renter")
}

// RenterFilesDir returns the renter's files directory
func (tn *TestNode) RenterFilesDir() string {
	return filepath.Join(tn.RenterDir(), "i3vfiles")
}

// FilesDir returns the LocalDir that is the testnodes upload directory
func (tn *TestNode) FilesDir() *LocalDir {
	return tn.filesDir
}
