package wallet

import (
	"os"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/I3v/i3vtest"
)

// walletTestDir creates a temporary testing directory for a wallet test. This
// should only every be called once per test. Otherwise it will delete the
// directory again.
func walletTestDir(testName string) string {
	path := i3vtest.TestDir("wallet", testName)
	if err := os.MkdirAll(path, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return path
}
