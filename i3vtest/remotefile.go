package i3vtest

import (
	"sync"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/modules"
)

type (
	// RemoteFile is a helper struct that represents a file uploaded to the I3v
	// network.
	RemoteFile struct {
		checksum crypto.Hash
		i3vPath  modules.I3vPath
		mu       sync.Mutex
	}
)

// Checksum returns the checksum of a remote file.
func (rf *RemoteFile) Checksum() crypto.Hash {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	return rf.checksum
}

// I3vPath returns the i3vPath of a remote file.
func (rf *RemoteFile) I3vPath() modules.I3vPath {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	return rf.i3vPath
}
