package contractor

import (
	"os"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/I3v/i3vtest"
)

// contractorTestDir creates a temporary testing directory for a contractor
// test. This should only every be called once per test. Otherwise it will
// delete the directory again.
func contractorTestDir(testName string) string {
	path := i3vtest.TestDir("renter/contractor", testName)
	if err := os.MkdirAll(path, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return path
}
