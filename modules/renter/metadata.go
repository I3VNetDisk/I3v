package renter

import (
	"fmt"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/I3VNetDisk/errors"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/filesystem"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vdir"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vfile"
)

// bubbleStatus indicates the status of a bubble being executed on a
// directory
type bubbleStatus int

// bubbleError, bubbleInit, bubbleActive, and bubblePending are the constants
// used to determine the status of a bubble being executed on a directory
const (
	bubbleError bubbleStatus = iota
	bubbleActive
	bubblePending
)

// managedPrepareBubble will add a bubble to the bubble map. If 'true' is returned, the
// caller should proceed by calling bubble. If 'false' is returned, the caller
// should not bubble, another thread will handle running the bubble.
func (r *Renter) managedPrepareBubble(i3vPath modules.I3vPath) bool {
	r.bubbleUpdatesMu.Lock()
	defer r.bubbleUpdatesMu.Unlock()

	// Check for bubble in bubbleUpdate map
	i3vPathStr := i3vPath.String()
	status, ok := r.bubbleUpdates[i3vPathStr]
	if !ok {
		r.bubbleUpdates[i3vPathStr] = bubbleActive
		return true
	}
	if status != bubbleActive && status != bubblePending {
		build.Critical("bubble status set to bubbleError")
	}
	r.bubbleUpdates[i3vPathStr] = bubblePending
	return false
}

// managedCalculateDirectoryMetadata calculates the new values for the
// directory's metadata and tracks the value, either worst or best, for each to
// be bubbled up
func (r *Renter) managedCalculateDirectoryMetadata(i3vPath modules.I3vPath) (i3vdir.Metadata, error) {
	// Set default metadata values to start
	metadata := i3vdir.Metadata{
		AggregateHealth:              i3vdir.DefaultDirHealth,
		AggregateLastHealthCheckTime: time.Now(),
		AggregateMinRedundancy:       math.MaxFloat64,
		AggregateModTime:             time.Time{},
		AggregateNumFiles:            uint64(0),
		AggregateNumStuckChunks:      uint64(0),
		AggregateNumSubDirs:          uint64(0),
		AggregateSize:                uint64(0),
		AggregateStuckHealth:         i3vdir.DefaultDirHealth,

		Health:              i3vdir.DefaultDirHealth,
		LastHealthCheckTime: time.Now(),
		MinRedundancy:       math.MaxFloat64,
		ModTime:             time.Time{},
		NumFiles:            uint64(0),
		NumStuckChunks:      uint64(0),
		NumSubDirs:          uint64(0),
		Size:                uint64(0),
		StuckHealth:         i3vdir.DefaultDirHealth,
	}
	// Read directory
	fileinfos, err := r.staticFileSystem.ReadDir(i3vPath)
	if err != nil {
		r.log.Printf("WARN: Error in reading files in directory %v : %v\n", i3vPath.String(), err)
		return i3vdir.Metadata{}, err
	}

	// Iterate over directory
	for _, fi := range fileinfos {
		// Check to make sure renter hasn't been shutdown
		select {
		case <-r.tg.StopChan():
			return i3vdir.Metadata{}, err
		default:
		}

		// Aggregate Fields
		var aggregateHealth, aggregateStuckHealth, aggregateMinRedundancy float64
		var aggregateLastHealthCheckTime, aggregateModTime time.Time
		var fileMetadata i3vfile.BubbledMetadata
		ext := filepath.Ext(fi.Name())
		// Check for I3vFiles and Directories
		if ext == modules.I3vFileExtension {
			// I3vFile found, calculate the needed metadata information of the i3vfile
			fName := strings.TrimSuffix(fi.Name(), modules.I3vFileExtension)
			fileI3vPath, err := i3vPath.Join(fName)
			if err != nil {
				r.log.Println("unable to join i3vpath with dirpath while calculating directory metadata:", err)
				continue
			}
			fileMetadata, err = r.managedCalculateAndUpdateFileMetadata(fileI3vPath)
			if err != nil {
				r.log.Printf("failed to calculate file metadata %v: %v", fi.Name(), err)
				continue
			}

			// If 75% or more of the redundancy is missing, register an alert
			// for the file.
			uid := string(fileMetadata.UID)
			if maxHealth := math.Max(fileMetadata.Health, fileMetadata.StuckHealth); maxHealth >= AlertI3vfileLowRedundancyThreshold {
				r.staticAlerter.RegisterAlert(modules.AlertIDI3vfileLowRedundancy(uid), AlertMSGI3vfileLowRedundancy,
					AlertCauseI3vfileLowRedundancy(fileI3vPath, maxHealth),
					modules.SeverityWarning)
			} else {
				r.staticAlerter.UnregisterAlert(modules.AlertIDI3vfileLowRedundancy(uid))
			}

			// Record Values that compare against sub directories
			aggregateHealth = fileMetadata.Health
			aggregateStuckHealth = fileMetadata.StuckHealth
			aggregateMinRedundancy = fileMetadata.Redundancy
			aggregateLastHealthCheckTime = fileMetadata.LastHealthCheckTime
			aggregateModTime = fileMetadata.ModTime

			// Update aggregate fields.
			metadata.AggregateNumFiles++
			metadata.AggregateNumStuckChunks += fileMetadata.NumStuckChunks
			metadata.AggregateSize += fileMetadata.Size

			// Update i3vdir fields.
			metadata.Health = math.Max(metadata.Health, fileMetadata.Health)
			if fileMetadata.LastHealthCheckTime.Before(metadata.LastHealthCheckTime) {
				metadata.LastHealthCheckTime = fileMetadata.LastHealthCheckTime
			}
			if fileMetadata.Redundancy != -1 {
				metadata.MinRedundancy = math.Min(metadata.MinRedundancy, fileMetadata.Redundancy)
			}
			if fileMetadata.ModTime.After(metadata.ModTime) {
				metadata.ModTime = fileMetadata.ModTime
			}
			metadata.NumFiles++
			metadata.NumStuckChunks += fileMetadata.NumStuckChunks
			metadata.Size += fileMetadata.Size
			metadata.StuckHealth = math.Max(metadata.StuckHealth, fileMetadata.StuckHealth)
		} else if fi.IsDir() {
			// Directory is found, read the directory metadata file
			dirI3vPath, err := i3vPath.Join(fi.Name())
			if err != nil {
				return i3vdir.Metadata{}, err
			}
			dirMetadata, err := r.managedDirectoryMetadata(dirI3vPath)
			if err != nil {
				return i3vdir.Metadata{}, err
			}

			// Record Values that compare against files
			aggregateHealth = dirMetadata.AggregateHealth
			aggregateStuckHealth = dirMetadata.AggregateStuckHealth
			aggregateMinRedundancy = dirMetadata.AggregateMinRedundancy
			aggregateLastHealthCheckTime = dirMetadata.AggregateLastHealthCheckTime
			aggregateModTime = dirMetadata.AggregateModTime

			// Update aggregate fields.
			metadata.AggregateNumFiles += dirMetadata.AggregateNumFiles
			metadata.AggregateNumStuckChunks += dirMetadata.AggregateNumStuckChunks
			metadata.AggregateNumSubDirs += dirMetadata.AggregateNumSubDirs
			metadata.AggregateSize += dirMetadata.AggregateSize

			// Update i3vdir fields
			metadata.NumSubDirs++
		} else {
			// Ignore everything that is not a I3vFile or a directory
			continue
		}
		// Track the max value of AggregateHealth and Aggregate StuckHealth
		metadata.AggregateHealth = math.Max(metadata.AggregateHealth, aggregateHealth)
		metadata.AggregateStuckHealth = math.Max(metadata.AggregateStuckHealth, aggregateStuckHealth)
		// Track the min value for AggregateMinRedundancy
		if aggregateMinRedundancy != -1 {
			metadata.AggregateMinRedundancy = math.Min(metadata.AggregateMinRedundancy, aggregateMinRedundancy)
		}
		// Update LastHealthCheckTime
		if aggregateLastHealthCheckTime.Before(metadata.AggregateLastHealthCheckTime) {
			metadata.AggregateLastHealthCheckTime = aggregateLastHealthCheckTime
		}
		// Update ModTime
		if aggregateModTime.After(metadata.AggregateModTime) {
			metadata.AggregateModTime = aggregateModTime
		}
	}
	// Sanity check on ModTime. If mod time is still zero it means there were no
	// files or subdirectories. Set ModTime to now since we just updated this
	// directory
	if metadata.AggregateModTime.IsZero() {
		metadata.AggregateModTime = time.Now()
	}
	if metadata.ModTime.IsZero() {
		metadata.ModTime = time.Now()
	}
	// Sanity check on Redundancy. If MinRedundancy is still math.MaxFloat64
	// then set it to -1 to indicate an empty directory
	if metadata.AggregateMinRedundancy == math.MaxFloat64 {
		metadata.AggregateMinRedundancy = -1
	}
	if metadata.MinRedundancy == math.MaxFloat64 {
		metadata.MinRedundancy = -1
	}

	return metadata, nil
}

// managedCalculateAndUpdateFileMetadata calculates and returns the necessary
// metadata information of a i3vfile that needs to be bubbled. The calculated
// metadata information is also updated and saved to disk
func (r *Renter) managedCalculateAndUpdateFileMetadata(i3vPath modules.I3vPath) (i3vfile.BubbledMetadata, error) {
	// Load the I3vfile.
	sf, err := r.staticFileSystem.OpenI3vFile(i3vPath)
	if err != nil {
		return i3vfile.BubbledMetadata{}, err
	}
	defer sf.Close()

	// Get offline and goodforrenew maps
	hostOfflineMap, hostGoodForRenewMap, _ := r.managedRenterContractsAndUtilities([]*filesystem.FileNode{sf})

	// Calculate file health
	health, stuckHealth, _, _, numStuckChunks := sf.Health(hostOfflineMap, hostGoodForRenewMap)

	// Set the LastHealthCheckTime
	sf.SetLastHealthCheckTime()

	// Calculate file Redundancy and check if local file is missing and
	// redundancy is less than one
	redundancy, _, err := sf.Redundancy(hostOfflineMap, hostGoodForRenewMap)
	if err != nil {
		return i3vfile.BubbledMetadata{}, err
	}
	if _, err := os.Stat(sf.LocalPath()); os.IsNotExist(err) && redundancy < 1 {
		r.log.Debugln("File not found on disk and possibly unrecoverable:", sf.LocalPath())
	}

	return i3vfile.BubbledMetadata{
		Health:              health,
		LastHealthCheckTime: sf.LastHealthCheckTime(),
		ModTime:             sf.ModTime(),
		NumStuckChunks:      numStuckChunks,
		Redundancy:          redundancy,
		Size:                sf.Size(),
		StuckHealth:         stuckHealth,
		UID:                 sf.UID(),
	}, sf.SaveMetadata()
}

// managedCompleteBubbleUpdate completes the bubble update and updates and/or
// removes it from the renter's bubbleUpdates.
//
// TODO: bubbleUpdatesMu is in violation of conventions, needs to be moved to
// its own object to have its own mu.
func (r *Renter) managedCompleteBubbleUpdate(i3vPath modules.I3vPath) {
	r.bubbleUpdatesMu.Lock()
	defer r.bubbleUpdatesMu.Unlock()

	// Check current status
	i3vPathStr := i3vPath.String()
	status, exists := r.bubbleUpdates[i3vPathStr]

	// If the status is 'bubbleActive', delete the status and return.
	if status == bubbleActive {
		delete(r.bubbleUpdates, i3vPathStr)
		return
	}
	// If the status is not 'bubbleActive', and the status is also not
	// 'bubblePending', this is an error. There should be a status, and it
	// should either be active or pending.
	if status != bubblePending {
		build.Critical("invalid bubble status", status, exists)
		delete(r.bubbleUpdates, i3vPathStr) // Attempt to reset the corrupted state.
		return
	}
	// The status is bubblePending, switch the status to bubbleActive.
	r.bubbleUpdates[i3vPathStr] = bubbleActive

	// Launch a thread to do another bubble on this directory, as there was a
	// bubble pending waiting for the current bubble to complete.
	err := r.tg.Add()
	if err != nil {
		return
	}
	go func() {
		defer r.tg.Done()
		r.managedPerformBubbleMetadata(i3vPath)
	}()
}

// managedDirectoryMetadata reads the directory metadata and returns the bubble
// metadata
func (r *Renter) managedDirectoryMetadata(i3vPath modules.I3vPath) (i3vdir.Metadata, error) {
	// Check for bad paths and files
	fi, err := r.staticFileSystem.Stat(i3vPath)
	if err != nil {
		return i3vdir.Metadata{}, err
	}
	if !fi.IsDir() {
		return i3vdir.Metadata{}, fmt.Errorf("%v is not a directory", i3vPath)
	}

	//  Open I3vDir
	i3vDir, err := r.staticFileSystem.OpenI3vDir(i3vPath)
	if err != nil && errors.Contains(err, filesystem.ErrNotExist) {
		// If i3vdir doesn't exist create one
		err = r.staticFileSystem.NewI3vDir(i3vPath, modules.DefaultDirPerm)
		if err != nil {
			return i3vdir.Metadata{}, err
		}
		i3vDir, err = r.staticFileSystem.OpenI3vDir(i3vPath)
		if err != nil {
			return i3vdir.Metadata{}, err
		}
	} else if err != nil {
		return i3vdir.Metadata{}, err
	}
	defer i3vDir.Close()

	// Grab the metadata.
	md, err := i3vDir.Metadata()
	if err != nil && errors.Contains(err, filesystem.ErrNotExist) {
		// If metadata doesn't exist create it.
		err = r.staticFileSystem.NewI3vDir(i3vPath, modules.DefaultDirPerm)
		if err != nil {
			return i3vdir.Metadata{}, err
		}
		// Try loading Metadata again.
		return i3vDir.Metadata()
	} else if err != nil {
		return i3vdir.Metadata{}, err
	}
	return md, nil
}

// managedUpdateLastHealthCheckTime updates the LastHealthCheckTime and
// AggregateLastHealthCheckTime fields of the directory metadata by reading all
// the subdirs of the directory.
func (r *Renter) managedUpdateLastHealthCheckTime(i3vPath modules.I3vPath) error {
	// Open dir and fetch current metadata.
	entry, err := r.staticFileSystem.OpenI3vDir(i3vPath)
	if err != nil {
		return err
	}
	metadata, err := entry.Metadata()
	if err != nil {
		return err
	}

	// Set the LastHealthCheckTimes to the current time.
	metadata.LastHealthCheckTime = time.Now()
	metadata.AggregateLastHealthCheckTime = time.Now()

	// Read directory
	fileinfos, err := r.staticFileSystem.ReadDir(i3vPath)
	if err != nil {
		r.log.Printf("WARN: Error in reading files in directory %v : %v\n", i3vPath.String(), err)
		return err
	}

	// Iterate over directory
	for _, fi := range fileinfos {
		// Check to make sure renter hasn't been shutdown
		select {
		case <-r.tg.StopChan():
			return err
		default:
		}
		// Check for I3vFiles and Directories
		if fi.IsDir() {
			// Directory is found, read the directory metadata file
			dirI3vPath, err := i3vPath.Join(fi.Name())
			if err != nil {
				return err
			}
			dirMetadata, err := r.managedDirectoryMetadata(dirI3vPath)
			if err != nil {
				return err
			}
			// Update AggregateLastHealthCheckTime.
			if dirMetadata.AggregateLastHealthCheckTime.Before(metadata.AggregateLastHealthCheckTime) {
				metadata.AggregateLastHealthCheckTime = dirMetadata.AggregateLastHealthCheckTime
			}
		} else {
			// Ignore everything that is not a directory since files should be updated
			// already by the ongoing bubble.
			continue
		}
	}
	// Write changes to disk.
	return entry.UpdateMetadata(metadata)
}

// callThreadedBubbleMetadata is the thread safe method used to call
// managedBubbleMetadata when the call does not need to be blocking
func (r *Renter) callThreadedBubbleMetadata(i3vPath modules.I3vPath) {
	if err := r.tg.Add(); err != nil {
		return
	}
	defer r.tg.Done()
	if err := r.managedBubbleMetadata(i3vPath); err != nil {
		r.log.Debugln("WARN: error with bubbling metadata:", err)
	}
}

// managedPerformBubbleMetadata will bubble the metadata without checking the
// bubble preparation.
func (r *Renter) managedPerformBubbleMetadata(i3vPath modules.I3vPath) (err error) {
	// Make sure we call callThreadedBubbleMetadata on the parent once we are
	// done.
	defer func() error {
		// Complete bubble
		r.managedCompleteBubbleUpdate(i3vPath)

		// Continue with parent dir if we aren't in the root dir already.
		if i3vPath.IsRoot() {
			return nil
		}
		parentDir, err := i3vPath.Dir()
		if err != nil {
			return errors.AddContext(err, "failed to defer callThreadedBubbleMetadata on parent dir")
		}
		go r.callThreadedBubbleMetadata(parentDir)
		return nil
	}()

	// Calculate the new metadata values of the directory
	metadata, err := r.managedCalculateDirectoryMetadata(i3vPath)
	if err != nil {
		e := fmt.Sprintf("could not calculate the metadata of directory %v", i3vPath.String())
		return errors.AddContext(err, e)
	}

	// Update directory metadata with the health information. Don't return here
	// to avoid skipping the repairNeeded and stuckChunkFound signals.
	i3vDir, err := r.staticFileSystem.OpenI3vDir(i3vPath)
	if err != nil {
		e := fmt.Sprintf("could not open directory %v", i3vPath.String())
		err = errors.AddContext(err, e)
	} else {
		defer i3vDir.Close()
		err = i3vDir.UpdateMetadata(metadata)
		if err != nil {
			e := fmt.Sprintf("could not update the metadata of the directory %v", i3vPath.String())
			err = errors.AddContext(err, e)
		}
	}

	// If we are at the root directory then check if any files were found in
	// need of repair or and stuck chunks and trigger the appropriate repair
	// loop. This is only done at the root directory as the repair and stuck
	// loops start at the root directory so there is no point triggering them
	// until the root directory is updated
	if i3vPath.IsRoot() {
		if metadata.AggregateHealth >= RepairThreshold {
			select {
			case r.uploadHeap.repairNeeded <- struct{}{}:
			default:
			}
		}
		if metadata.AggregateNumStuckChunks > 0 {
			select {
			case r.uploadHeap.stuckChunkFound <- struct{}{}:
			default:
			}
		}
	}
	return err
}

// managedBubbleMetadata calculates the updated values of a directory's metadata
// and updates the i3vdir metadata on disk then calls callThreadedBubbleMetadata
// on the parent directory so that it is only blocking for the current directory
func (r *Renter) managedBubbleMetadata(i3vPath modules.I3vPath) error {
	// Check if bubble is needed
	proceedWithBubble := r.managedPrepareBubble(i3vPath)
	if !proceedWithBubble {
		// Update the AggregateLastHealthCheckTime even if we weren't able to
		// bubble right away.
		return r.managedUpdateLastHealthCheckTime(i3vPath)
	}
	return r.managedPerformBubbleMetadata(i3vPath)
}
