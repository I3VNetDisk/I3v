package renter

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/I3VNetDisk/fastrand"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/filesystem"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vfile"
	"gitlab.com/I3VNetDisk/I3v/persist"
)

// newRenterTestFile creates a test file when the test has a renter so that the
// file is properly added to the renter. It returns the I3vFileSetEntry that the
// I3vFile is stored in
func (r *Renter) newRenterTestFile() (*filesystem.FileNode, error) {
	// Generate name and erasure coding
	i3vPath, rsc := testingFileParams()
	// create the renter/files dir if it doesn't exist
	i3vFilePath := r.staticFileSystem.FilePath(i3vPath)
	dir, _ := filepath.Split(i3vFilePath)
	if err := os.MkdirAll(dir, 0700); err != nil {
		return nil, err
	}
	// Create File
	up := modules.FileUploadParams{
		Source:      "",
		I3vPath:     i3vPath,
		ErasureCode: rsc,
	}
	err := r.staticFileSystem.NewI3vFile(up.I3vPath, up.Source, up.ErasureCode, crypto.GenerateI3vKey(crypto.RandomCipherType()), 1000, persist.DefaultDiskPermissionsTest, false)
	if err != nil {
		return nil, err
	}
	return r.staticFileSystem.OpenI3vFile(up.I3vPath)
}

// TestRenterFileListLocalPath verifies that FileList() returns the correct
// local path information for an uploaded file.
func TestRenterFileListLocalPath(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()
	id := rt.renter.mu.Lock()
	entry, _ := rt.renter.newRenterTestFile()
	if err := entry.SetLocalPath("TestPath"); err != nil {
		t.Fatal(err)
	}
	rt.renter.mu.Unlock(id)
	files, err := rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 1 {
		t.Fatal("wrong number of files, got", len(files), "wanted one")
	}
	if files[0].LocalPath != "TestPath" {
		t.Fatal("file had wrong LocalPath: got", files[0].LocalPath, "wanted TestPath")
	}
}

// TestRenterDeleteFile probes the DeleteFile method of the renter type.
func TestRenterDeleteFile(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()

	// Delete a file from an empty renter.
	i3vPath, err := modules.NewI3vPath("dne")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.DeleteFile(i3vPath)
	// NOTE: using strings.Contains because errors.Contains does not recognize
	// errors when errors.Extend is used
	if !strings.Contains(err.Error(), filesystem.ErrNotExist.Error()) {
		t.Errorf("Expected error to contain %v but got '%v'", filesystem.ErrNotExist, err)
	}

	// Put a file in the renter.
	entry, err := rt.renter.newRenterTestFile()
	if err != nil {
		t.Fatal(err)
	}
	// Delete a different file.
	i3vPathOne, err := modules.NewI3vPath("one")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.DeleteFile(i3vPathOne)
	// NOTE: using strings.Contains because errors.Contains does not recognize
	// errors when errors.Extend is used
	if !strings.Contains(err.Error(), filesystem.ErrNotExist.Error()) {
		t.Errorf("Expected error to contain %v but got '%v'", filesystem.ErrNotExist, err)
	}
	// Delete the file.
	i3vpath := rt.renter.staticFileSystem.FileI3vPath(entry)

	entry.Close()
	err = rt.renter.DeleteFile(i3vpath)
	if err != nil {
		t.Fatal(err)
	}
	files, err := rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 0 {
		t.Error("file was deleted, but is still reported in FileList")
	}
	// Confirm that file was removed from I3vFileSet
	_, err = rt.renter.staticFileSystem.OpenI3vFile(i3vpath)
	if err == nil {
		t.Fatal("Deleted file still found in staticFileSet")
	}

	// Put a file in the renter, then rename it.
	entry2, err := rt.renter.newRenterTestFile()
	if err != nil {
		t.Fatal(err)
	}
	i3vPath1, err := modules.NewI3vPath("1")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(rt.renter.staticFileSystem.FileI3vPath(entry2), i3vPath1) // set name to "1"
	if err != nil {
		t.Fatal(err)
	}
	i3vpath2 := rt.renter.staticFileSystem.FileI3vPath(entry2)
	entry2.Close()
	i3vpath2 = rt.renter.staticFileSystem.FileI3vPath(entry2)
	err = rt.renter.RenameFile(i3vpath2, i3vPathOne)
	if err != nil {
		t.Fatal(err)
	}
	// Call delete on the previous name.
	err = rt.renter.DeleteFile(i3vPath1)
	// NOTE: using strings.Contains because errors.Contains does not recognize
	// errors when errors.Extend is used
	if !strings.Contains(err.Error(), filesystem.ErrNotExist.Error()) {
		t.Errorf("Expected error to contain %v but got '%v'", filesystem.ErrNotExist, err)
	}
	// Call delete on the new name.
	err = rt.renter.DeleteFile(i3vPathOne)
	if err != nil {
		t.Error(err)
	}

	// Check that all .i3v files have been deleted.
	var walkStr string
	rt.renter.staticFileSystem.Walk(modules.RootI3vPath(), func(path string, _ os.FileInfo, _ error) error {
		// capture only .i3v files
		if filepath.Ext(path) == ".i3v" {
			rel, _ := filepath.Rel(rt.renter.staticFileSystem.Root(), path) // strip testdir prefix
			walkStr += rel
		}
		return nil
	})
	expWalkStr := ""
	if walkStr != expWalkStr {
		t.Fatalf("Bad walk string: expected %q, got %q", expWalkStr, walkStr)
	}
}

// TestRenterDeleteFileMissingParent tries to delete a file for which the parent
// has been deleted before.
func TestRenterDeleteFileMissingParent(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()

	// Put a file in the renter.
	i3vPath, err := modules.NewI3vPath("parent/file")
	if err != nil {
		t.Fatal(err)
	}
	dirI3vPath, err := i3vPath.Dir()
	if err != nil {
		t.Fatal(err)
	}
	i3vPath, rsc := testingFileParams()
	up := modules.FileUploadParams{
		Source:      "",
		I3vPath:     i3vPath,
		ErasureCode: rsc,
	}
	err = rt.renter.staticFileSystem.NewI3vFile(up.I3vPath, up.Source, up.ErasureCode, crypto.GenerateI3vKey(crypto.RandomCipherType()), 1000, persist.DefaultDiskPermissionsTest, false)
	if err != nil {
		t.Fatal(err)
	}
	// Delete the parent.
	err = rt.renter.staticFileSystem.DeleteFile(dirI3vPath)
	// NOTE: using strings.Contains because errors.Contains does not recognize
	// errors when errors.Extend is used
	if !strings.Contains(err.Error(), filesystem.ErrNotExist.Error()) {
		t.Errorf("Expected error to contain %v but got '%v'", filesystem.ErrNotExist, err)
	}
	// Delete the file. This should not return an error since it's already
	// deleted implicitly.
	if err := rt.renter.staticFileSystem.DeleteFile(up.I3vPath); err != nil {
		t.Fatal(err)
	}
}

// TestRenterFileList probes the FileList method of the renter type.
func TestRenterFileList(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()

	// Get the file list of an empty renter.
	files, err := rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 0 {
		t.Fatal("FileList has non-zero length for empty renter?")
	}

	// Put a file in the renter.
	entry1, _ := rt.renter.newRenterTestFile()
	files, err = rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 1 {
		t.Fatal("FileList is not returning the only file in the renter")
	}
	entry1SP := rt.renter.staticFileSystem.FileI3vPath(entry1)
	if !files[0].I3vPath.Equals(entry1SP) {
		t.Error("FileList is not returning the correct filename for the only file")
	}

	// Put multiple files in the renter.
	entry2, _ := rt.renter.newRenterTestFile()
	entry2SP := rt.renter.staticFileSystem.FileI3vPath(entry2)
	files, err = rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 2 {
		t.Fatalf("Expected %v files, got %v", 2, len(files))
	}
	files, err = rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if !((files[0].I3vPath.Equals(entry1SP) || files[0].I3vPath.Equals(entry2SP)) &&
		(files[1].I3vPath.Equals(entry1SP) || files[1].I3vPath.Equals(entry2SP)) &&
		(files[0].I3vPath != files[1].I3vPath)) {
		t.Log("files[0].I3vPath", files[0].I3vPath)
		t.Log("files[1].I3vPath", files[1].I3vPath)
		t.Log("file1.I3vPath()", rt.renter.staticFileSystem.FileI3vPath(entry1).String())
		t.Log("file2.I3vPath()", rt.renter.staticFileSystem.FileI3vPath(entry2).String())
		t.Error("FileList is returning wrong names for the files")
	}
}

// TestRenterRenameFile probes the rename method of the renter.
func TestRenterRenameFile(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()

	// Rename a file that doesn't exist.
	i3vPath1, err := modules.NewI3vPath("1")
	if err != nil {
		t.Fatal(err)
	}
	i3vPath1a, err := modules.NewI3vPath("1a")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(i3vPath1, i3vPath1a)
	if err.Error() != filesystem.ErrNotExist.Error() {
		t.Errorf("Expected '%v' got '%v'", filesystem.ErrNotExist, err)
	}

	// Get the filesystem.
	sfs := rt.renter.staticFileSystem

	// Rename a file that does exist.
	entry, _ := rt.renter.newRenterTestFile()
	var sp modules.I3vPath
	if err := sp.FromSysPath(entry.I3vFilePath(), sfs.DirPath(modules.RootI3vPath())); err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(sp, i3vPath1)
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(i3vPath1, i3vPath1a)
	if err != nil {
		t.Fatal(err)
	}
	files, err := rt.renter.FileList(modules.RootI3vPath(), true, false)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 1 {
		t.Fatal("FileList has unexpected number of files:", len(files))
	}
	if !files[0].I3vPath.Equals(i3vPath1a) {
		t.Errorf("RenameFile failed: expected %v, got %v", i3vPath1a.String(), files[0].I3vPath)
	}
	// Confirm I3vFileSet was updated
	_, err = rt.renter.staticFileSystem.OpenI3vFile(i3vPath1a)
	if err != nil {
		t.Fatal("renter staticFileSet not updated to new file name:", err)
	}
	_, err = rt.renter.staticFileSystem.OpenI3vFile(i3vPath1)
	if err == nil {
		t.Fatal("old name not removed from renter staticFileSet")
	}
	// Rename a file to an existing name.
	entry2, err := rt.renter.newRenterTestFile()
	if err != nil {
		t.Fatal(err)
	}
	var sp2 modules.I3vPath
	if err := sp2.FromSysPath(entry2.I3vFilePath(), sfs.DirPath(modules.RootI3vPath())); err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(sp2, i3vPath1) // Rename to "1"
	if err != nil {
		t.Fatal(err)
	}
	entry2.Close()
	err = rt.renter.RenameFile(i3vPath1, i3vPath1a)
	if err != filesystem.ErrExists {
		t.Fatal("Expecting ErrExists, got", err)
	}
	// Rename a file to the same name.
	err = rt.renter.RenameFile(i3vPath1, i3vPath1)
	if err != filesystem.ErrExists {
		t.Fatal("Expecting ErrExists, got", err)
	}

	// Confirm ability to rename file
	i3vPath1b, err := modules.NewI3vPath("1b")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(i3vPath1, i3vPath1b)
	if err != nil {
		t.Fatal(err)
	}
	// Rename file that would create a directory
	i3vPathWithDir, err := modules.NewI3vPath("new/name/with/dir/test")
	if err != nil {
		t.Fatal(err)
	}
	err = rt.renter.RenameFile(i3vPath1b, i3vPathWithDir)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm directory metadatas exist
	for !i3vPathWithDir.Equals(modules.RootI3vPath()) {
		i3vPathWithDir, err = i3vPathWithDir.Dir()
		if err != nil {
			t.Fatal(err)
		}
		_, err = rt.renter.staticFileSystem.OpenI3vDir(i3vPathWithDir)
		if err != nil {
			t.Fatal(err)
		}
	}
}

// TestRenterFileDir tests that the renter files are uploaded to the files
// directory and not the root directory of the renter.
func TestRenterFileDir(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer rt.Close()

	// Create local file to upload
	localDir := filepath.Join(rt.dir, "files")
	if err := os.MkdirAll(localDir, 0700); err != nil {
		t.Fatal(err)
	}
	size := 100
	fileName := fmt.Sprintf("%dbytes %s", size, hex.EncodeToString(fastrand.Bytes(4)))
	source := filepath.Join(localDir, fileName)
	bytes := fastrand.Bytes(size)
	if err := ioutil.WriteFile(source, bytes, 0600); err != nil {
		t.Fatal(err)
	}

	// Upload local file
	ec, err := i3vfile.NewRSCode(DefaultDataPieces, DefaultParityPieces)
	if err != nil {
		t.Fatal(err)
	}
	i3vPath, err := modules.NewI3vPath(fileName)
	if err != nil {
		t.Fatal(err)
	}
	params := modules.FileUploadParams{
		Source:      source,
		I3vPath:     i3vPath,
		ErasureCode: ec,
	}
	err = rt.renter.Upload(params)
	if err != nil {
		t.Fatal("failed to upload file:", err)
	}

	// Get file and check i3vpath
	f, err := rt.renter.File(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	if !f.I3vPath.Equals(i3vPath) {
		t.Fatalf("i3vpath not set as expected: got %v expected %v", f.I3vPath, fileName)
	}

	// Confirm .i3v file exists on disk in the I3vpathRoot directory
	renterDir := filepath.Join(rt.dir, modules.RenterDir)
	i3vpathRootDir := filepath.Join(renterDir, modules.FileSystemRoot)
	fullPath := i3vPath.I3vFileSysPath(i3vpathRootDir)
	if _, err := os.Stat(fullPath); os.IsNotExist(err) {
		t.Fatal("No .i3v file found on disk")
	}
}
