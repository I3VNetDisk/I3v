package renter

import (
	"strconv"
	"testing"

	"gitlab.com/I3VNetDisk/I3v/modules"
)

// TestStuckStack probes the implementation of the stuck stack
func TestStuckStack(t *testing.T) {
	stack := stuckStack{
		stack:    make([]modules.I3vPath, 0, maxSuccessfulStuckRepairFiles),
		i3vPaths: make(map[modules.I3vPath]struct{}),
	}

	// Check stack initialized as expected
	if stack.managedLen() != 0 {
		t.Fatal("Expected length of 0 got", stack.managedLen())
	}

	// Create some I3vPaths to add to the stack
	sp1, _ := modules.NewI3vPath("i3vPath1")
	sp2, _ := modules.NewI3vPath("i3vPath2")

	// Test pushing 1 i3vpath onto stack
	stack.managedPush(sp1)
	if stack.managedLen() != 1 {
		t.Fatal("Expected length of 1 got", stack.managedLen())
	}
	i3vPath := stack.managedPop()
	if !i3vPath.Equals(sp1) {
		t.Log("i3vPath:", i3vPath)
		t.Log("sp1:", sp1)
		t.Fatal("I3vPaths not equal")
	}
	if stack.managedLen() != 0 {
		t.Fatal("Expected length of 0 got", stack.managedLen())
	}

	// Test adding multiple i3vPaths to stack
	stack.managedPush(sp1)
	stack.managedPush(sp2)
	if stack.managedLen() != 2 {
		t.Fatal("Expected length of 2 got", stack.managedLen())
	}
	// Last i3vpath added should be returned
	i3vPath = stack.managedPop()
	if !i3vPath.Equals(sp2) {
		t.Log("i3vPath:", i3vPath)
		t.Log("sp2:", sp2)
		t.Fatal("I3vPaths not equal")
	}

	// Pushing first i3vpath again should result in moving it to the top
	stack.managedPush(sp2)
	stack.managedPush(sp1)
	if stack.managedLen() != 2 {
		t.Fatal("Expected length of 2 got", stack.managedLen())
	}
	i3vPath = stack.managedPop()
	if !i3vPath.Equals(sp1) {
		t.Log("i3vPath:", i3vPath)
		t.Log("sp1:", sp1)
		t.Fatal("I3vPaths not equal")
	}

	// Length should never exceed maxSuccessfulStuckRepairFiles
	for i := 0; i < 2*maxSuccessfulStuckRepairFiles; i++ {
		sp, _ := modules.NewI3vPath(strconv.Itoa(i))
		stack.managedPush(sp)
		if stack.managedLen() > maxSuccessfulStuckRepairFiles {
			t.Fatalf("Length exceeded %v, %v", maxSuccessfulStuckRepairFiles, stack.managedLen())
		}
	}
}
