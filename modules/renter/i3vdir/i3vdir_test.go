package i3vdir

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/errors"
)

// checkMetadataInit is a helper that verifies that the metadata was initialized
// properly
func checkMetadataInit(md Metadata) error {
	// Check Aggregate Fields
	if md.AggregateHealth != DefaultDirHealth {
		return fmt.Errorf("I3vDir AggregateHealth not set properly: got %v expected %v", md.AggregateHealth, DefaultDirHealth)
	}
	if !md.AggregateLastHealthCheckTime.IsZero() {
		return fmt.Errorf("AggregateLastHealthCheckTime should be zero but was %v", md.AggregateLastHealthCheckTime)
	}
	if md.AggregateMinRedundancy != DefaultDirRedundancy {
		return fmt.Errorf("I3vDir AggregateMinRedundancy not set properly: got %v expected %v", md.AggregateMinRedundancy, DefaultDirRedundancy)
	}
	if md.AggregateModTime.IsZero() {
		return errors.New("AggregateModTime not initialized")
	}
	if md.AggregateNumFiles != 0 {
		return fmt.Errorf("I3vDir AggregateNumFiles not set properly: got %v expected 0", md.AggregateNumFiles)
	}
	if md.AggregateNumStuckChunks != 0 {
		return fmt.Errorf("I3vDir AggregateNumStuckChunks not initialized properly, expected 0, got %v", md.AggregateNumStuckChunks)
	}
	if md.AggregateNumSubDirs != 0 {
		return fmt.Errorf("I3vDir AggregateNumSubDirs not initialized properly, expected 0, got %v", md.AggregateNumSubDirs)
	}
	if md.AggregateStuckHealth != DefaultDirHealth {
		return fmt.Errorf("I3vDir AggregateStuckHealth not set properly: got %v expected %v", md.AggregateStuckHealth, DefaultDirHealth)
	}
	if md.AggregateSize != 0 {
		return fmt.Errorf("I3vDir AggregateSize not set properly: got %v expected 0", md.AggregateSize)
	}

	// Check I3vDir Fields
	if md.Health != DefaultDirHealth {
		return fmt.Errorf("I3vDir Health not set properly: got %v expected %v", md.Health, DefaultDirHealth)
	}
	if !md.LastHealthCheckTime.IsZero() {
		return fmt.Errorf("LastHealthCheckTime should be zero but was %v", md.LastHealthCheckTime)
	}
	if md.MinRedundancy != DefaultDirRedundancy {
		return fmt.Errorf("I3vDir MinRedundancy not set properly: got %v expected %v", md.MinRedundancy, DefaultDirRedundancy)
	}
	if md.ModTime.IsZero() {
		return errors.New("ModTime not initialized")
	}
	if md.NumFiles != 0 {
		return fmt.Errorf("I3vDir NumFiles not initialized properly, expected 0, got %v", md.NumFiles)
	}
	if md.NumStuckChunks != 0 {
		return fmt.Errorf("I3vDir NumStuckChunks not initialized properly, expected 0, got %v", md.NumStuckChunks)
	}
	if md.NumSubDirs != 0 {
		return fmt.Errorf("I3vDir NumSubDirs not initialized properly, expected 0, got %v", md.NumSubDirs)
	}
	if md.StuckHealth != DefaultDirHealth {
		return fmt.Errorf("I3vDir stuck health not set properly: got %v expected %v", md.StuckHealth, DefaultDirHealth)
	}
	if md.Size != 0 {
		return fmt.Errorf("I3vDir Size not set properly: got %v expected 0", md.Size)
	}
	return nil
}

// newRootDir creates a root directory for the test and removes old test files
func newRootDir(t *testing.T) (string, error) {
	dir := filepath.Join(os.TempDir(), "i3vdirs", t.Name())
	err := os.RemoveAll(dir)
	if err != nil {
		return "", err
	}
	return dir, nil
}

// TestNewI3vDir tests that i3vdirs are created on disk properly. It uses
// LoadI3vDir to read the metadata from disk
//func TestNewI3vDir(t *testing.T) {
//	if testing.Short() {
//		t.SkipNow()
//	}
//	t.Parallel()
//
//	// Create New I3vDir that is two levels deep
//	rootDir, err := newRootDir(t)
//	if err != nil {
//		t.Fatal(err)
//	}
//	i3vPathDir, err := modules.NewI3vPath("TestDir")
//	if err != nil {
//		t.Fatal(err)
//	}
//	i3vPathSubDir, err := modules.NewI3vPath("SubDir")
//	if err != nil {
//		t.Fatal(err)
//	}
//	i3vPath, err := i3vPathDir.Join(i3vPathSubDir.String())
//	if err != nil {
//		t.Fatal(err)
//	}
//	wal, _ := newTestWAL()
//	i3vDir, err := New(i3vPath, rootDir, wal)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	// Check Sub Dir
//	//
//	// Check that the metadta was initialized properly
//	md := i3vDir.metadata
//	if err = checkMetadataInit(md); err != nil {
//		t.Fatal(err)
//	}
//	// Check that the I3vPath was initialized properly
//	if i3vDir.I3vPath() != i3vPath {
//		t.Fatalf("I3vDir I3vPath not set properly: got %v expected %v", i3vDir.I3vPath(), i3vPath)
//	}
//	// Check that the directory and .i3vdir file were created on disk
//	_, err = os.Stat(i3vPath.I3vDirSysPath(rootDir))
//	if err != nil {
//		t.Fatal(err)
//	}
//	_, err = os.Stat(i3vPath.I3vDirMetadataSysPath(rootDir))
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	// Check Top Directory
//	//
//	// Check that the directory and .i3vdir file were created on disk
//	_, err = os.Stat(i3vPath.I3vDirSysPath(rootDir))
//	if err != nil {
//		t.Fatal(err)
//	}
//	_, err = os.Stat(i3vPath.I3vDirMetadataSysPath(rootDir))
//	if err != nil {
//		t.Fatal(err)
//	}
//	// Get I3vDir
//	subDir, err := LoadI3vDir(rootDir, i3vPathDir, modules.ProdDependencies, wal)
//	// Check that the metadata was initialized properly
//	md = subDir.metadata
//	if err = checkMetadataInit(md); err != nil {
//		t.Fatal(err)
//	}
//	// Check that the I3vPath was initialized properly
//	if subDir.I3vPath() != i3vPathDir {
//		t.Fatalf("I3vDir I3vPath not set properly: got %v expected %v", subDir.I3vPath(), i3vPathDir)
//	}
//
//	// Check Root Directory
//	//
//	// Get I3vDir
//	rootI3vDir, err := LoadI3vDir(rootDir, modules.RootI3vPath(), modules.ProdDependencies, wal)
//	// Check that the metadata was initialized properly
//	md = rootI3vDir.metadata
//	if err = checkMetadataInit(md); err != nil {
//		t.Fatal(err)
//	}
//	// Check that the I3vPath was initialized properly
//	if !rootI3vDir.I3vPath().IsRoot() {
//		t.Fatalf("I3vDir I3vPath not set properly: got %v expected %v", rootI3vDir.I3vPath().String(), "")
//	}
//	// Check that the directory and .i3vdir file were created on disk
//	_, err = os.Stat(rootDir)
//	if err != nil {
//		t.Fatal(err)
//	}
//	_, err = os.Stat(modules.RootI3vPath().I3vDirMetadataSysPath(rootDir))
//	if err != nil {
//		t.Fatal(err)
//	}
//}

// Test UpdatedMetadata probes the UpdateMetadata method
func TestUpdateMetadata(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create new i3vDir
	rootDir, err := newRootDir(t)
	if err != nil {
		t.Fatal(err)
	}
	i3vPath, err := modules.NewI3vPath("TestDir")
	if err != nil {
		t.Fatal(err)
	}
	i3vDirSysPath := i3vPath.I3vDirSysPath(rootDir)
	wal, _ := newTestWAL()
	i3vDir, err := New(i3vDirSysPath, rootDir, modules.DefaultDirPerm, wal)
	if err != nil {
		t.Fatal(err)
	}

	// Check metadata was initialized properly in memory and on disk
	md := i3vDir.metadata
	if err = checkMetadataInit(md); err != nil {
		t.Fatal(err)
	}
	i3vDir, err = LoadI3vDir(i3vDirSysPath, modules.ProdDependencies, wal)
	if err != nil {
		t.Fatal(err)
	}
	md = i3vDir.metadata
	if err = checkMetadataInit(md); err != nil {
		t.Fatal(err)
	}

	// Set the metadata
	checkTime := time.Now()
	metadataUpdate := md
	// Aggregate fields
	metadataUpdate.AggregateHealth = 7
	metadataUpdate.AggregateLastHealthCheckTime = checkTime
	metadataUpdate.AggregateMinRedundancy = 2.2
	metadataUpdate.AggregateModTime = checkTime
	metadataUpdate.AggregateNumFiles = 11
	metadataUpdate.AggregateNumStuckChunks = 15
	metadataUpdate.AggregateNumSubDirs = 5
	metadataUpdate.AggregateSize = 2432
	metadataUpdate.AggregateStuckHealth = 5
	// I3vDir fields
	metadataUpdate.Health = 4
	metadataUpdate.LastHealthCheckTime = checkTime
	metadataUpdate.MinRedundancy = 2
	metadataUpdate.ModTime = checkTime
	metadataUpdate.NumFiles = 5
	metadataUpdate.NumStuckChunks = 6
	metadataUpdate.NumSubDirs = 4
	metadataUpdate.Size = 223
	metadataUpdate.StuckHealth = 2

	err = i3vDir.UpdateMetadata(metadataUpdate)
	if err != nil {
		t.Fatal(err)
	}

	// Check that the metadata was updated properly in memory and on disk
	md = i3vDir.metadata
	err = equalMetadatas(md, metadataUpdate)
	if err != nil {
		t.Fatal(err)
	}
	i3vDir, err = LoadI3vDir(i3vDirSysPath, modules.ProdDependencies, wal)
	if err != nil {
		t.Fatal(err)
	}
	md = i3vDir.metadata
	// Check Time separately due to how the time is persisted
	if !md.AggregateLastHealthCheckTime.Equal(metadataUpdate.AggregateLastHealthCheckTime) {
		t.Fatalf("AggregateLastHealthCheckTimes not equal, got %v expected %v", md.AggregateLastHealthCheckTime, metadataUpdate.AggregateLastHealthCheckTime)
	}
	metadataUpdate.AggregateLastHealthCheckTime = md.AggregateLastHealthCheckTime
	if !md.LastHealthCheckTime.Equal(metadataUpdate.LastHealthCheckTime) {
		t.Fatalf("LastHealthCheckTimes not equal, got %v expected %v", md.LastHealthCheckTime, metadataUpdate.LastHealthCheckTime)
	}
	metadataUpdate.LastHealthCheckTime = md.LastHealthCheckTime
	if !md.AggregateModTime.Equal(metadataUpdate.AggregateModTime) {
		t.Fatalf("AggregateModTimes not equal, got %v expected %v", md.AggregateModTime, metadataUpdate.AggregateModTime)
	}
	metadataUpdate.AggregateModTime = md.AggregateModTime
	if !md.ModTime.Equal(metadataUpdate.ModTime) {
		t.Fatalf("ModTimes not equal, got %v expected %v", md.ModTime, metadataUpdate.ModTime)
	}
	metadataUpdate.ModTime = md.ModTime
	// Check the rest of the metadata
	err = equalMetadatas(md, metadataUpdate)
	if err != nil {
		t.Fatal(err)
	}
}
