# I3vDir
The I3vDir module is responsible for creating and maintaining the directory
metadata information stored in the `.i3vdir` files on disk. This includes all
disk interaction and metadata definition. These i3vdirs represent directories on
the I3v network.

## Structure of the I3vDir
The I3vDir is a dir on the I3v network and the i3vdir metadata is a JSON
formatted metadata file that contains aggregate and non-aggregate fields. The
aggregate fields are the totals of the i3vdir and any sub i3vdirs, or are
calculated based on all the values in the subtree. The non-aggregate fields are
information specific to the i3vdir that is not an aggregate of the entire sub
directory tree

## Subsystems
The following subsystems help the I3vDir module execute its responsibilities:
 - [Persistence Subsystem](#persistence-subsystem)
 - [File Format Subsystem](#file-format-subsystem)
 - [I3vDirSet Subsystem](#i3vdirset-subsystem)
 - [DirReader Subsystem](#dirreader-subsystem)

 ### Persistence Subsystem
 **Key Files**
- [persist.go](./persist.go)
- [persistwal.go](./persistwal.go)

The Persistence subsystem is responsible for the disk interaction with the
`.i3vdir` files and ensuring safe and performant ACID operations by using the
[writeaheadlog](https://gitlab.com/I3VNetDisk/writeaheadlog) package. There
are two WAL updates that are used, deletion and metadata updates.

The WAL deletion update removes all the contents of the directory including the
directory itself.

The WAL metadata update re-writes the entire metadata, which is stored as JSON.
This is used whenever the metadata changes and needs to be saved as well as when
a new i3vdir is created.

**Exports**
 - `ApplyUpdates`
 - `CreateAndApplyTransaction`
 - `IsI3vDirUpdate`
 - `New`
 - `LoadI3vDir`
 - `UpdateMetadata`

**Inbound Complexities**
 - `callDelete` deletes a I3vDir from disk
    - `I3vDirSet.Delete` uses `callDelete`
 - `LoadI3vDir` loads a I3vDir from disk
    - `I3vDirSet.open` uses `LoadI3vDir`

### File Format Subsystem
 **Key Files**
- [i3vdir.go](./i3vdir.go)

The file format subsystem contains the type definitions for the I3vDir
format and methods that return information about the I3vDir.

**Exports**
 - `Deleted`
 - `Metatdata`
 - `I3vPath`

### I3vDirSet Subsystem
 **Key Files**
- [i3vdirset.go](./i3vdirset.go)

A I3vDir object is threadsafe by itself, and to ensure that when a I3vDir is
accessed by multiple threads that it is still threadsafe, I3vDirs should always
be accessed through the I3vDirSet. The I3vDirSet was created as a pool of
I3vDirs which is used by other packages to get access to I3vDirEntries which are
wrappers for I3vDirs containing some extra information about how many threads
are using it at a certain time. If a I3vDir was already loaded the I3vDirSet
will hand out the existing object, otherwise it will try to load it from disk.

**Exports**
 - `HealthPercentage`
 - `NewI3vDirSet`
 - `Close`
 - `Delete`
 - `DirInfo`
 - `DirList`
 - `Exists`
 - `InitRootDir`
 - `NewI3vDir`
 - `Open`
 - `Rename`

**Outbound Complexities**
 - `Delete` will use `callDelete` to delete the I3vDir once it has been acquired
   in the set
 - `open` calls `LoadI3vDir` to load the I3vDir from disk

### DirReader Subsystem
**Key Files**
 - [dirreader.go](./dirreader.go)

The DirReader Subsystem creates the DirReader which is used as a helper to read
raw .i3vdir from disk

**Exports**
 - `Close`
 - `Read`
 - `Stat`
 - `DirReader`
