package i3vdir

import (
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/I3VNetDisk/writeaheadlog"

	"gitlab.com/I3VNetDisk/I3v/modules"
)

type (
	// I3vDir contains the metadata information about a renter directory
	I3vDir struct {
		metadata Metadata

		// path is the path of the I3vDir folder.
		path string

		// Utility fields
		deleted bool
		deps    modules.Dependencies
		mu      sync.Mutex
		wal     *writeaheadlog.WAL
	}

	// Metadata is the metadata that is saved to disk as a .i3vdir file
	Metadata struct {
		// For each field in the metadata there is an aggregate value and a
		// i3vdir specific value. If a field has the aggregate prefix it means
		// that the value takes into account all the i3vfiles and i3vdirs in the
		// sub tree. The definition of aggregate and i3vdir specific values is
		// otherwise the same.
		//
		// Health is the health of the most in need i3vfile that is not stuck
		//
		// LastHealthCheckTime is the oldest LastHealthCheckTime of any of the
		// i3vfiles in the i3vdir and is the last time the health was calculated
		// by the health loop
		//
		// MinRedundancy is the minimum redundancy of any of the i3vfiles in the
		// i3vdir
		//
		// ModTime is the last time any of the i3vfiles in the i3vdir was
		// updated
		//
		// NumFiles is the total number of i3vfiles in a i3vdir
		//
		// NumStuckChunks is the sum of all the Stuck Chunks of any of the
		// i3vfiles in the i3vdir
		//
		// NumSubDirs is the number of sub-i3vdirs in a i3vdir
		//
		// Size is the total amount of data stored in the i3vfiles of the i3vdir
		//
		// StuckHealth is the health of the most in need i3vfile in the i3vdir,
		// stuck or not stuck

		// The following fields are aggregate values of the i3vdir. These values are
		// the totals of the i3vdir and any sub i3vdirs, or are calculated based on
		// all the values in the subtree
		AggregateHealth              float64   `json:"aggregatehealth"`
		AggregateLastHealthCheckTime time.Time `json:"aggregatelasthealthchecktime"`
		AggregateMinRedundancy       float64   `json:"aggregateminredundancy"`
		AggregateModTime             time.Time `json:"aggregatemodtime"`
		AggregateNumFiles            uint64    `json:"aggregatenumfiles"`
		AggregateNumStuckChunks      uint64    `json:"aggregatenumstuckchunks"`
		AggregateNumSubDirs          uint64    `json:"aggregatenumsubdirs"`
		AggregateSize                uint64    `json:"aggregatesize"`
		AggregateStuckHealth         float64   `json:"aggregatestuckhealth"`

		// The following fields are information specific to the i3vdir that is not
		// an aggregate of the entire sub directory tree
		Health              float64     `json:"health"`
		LastHealthCheckTime time.Time   `json:"lasthealthchecktime"`
		MinRedundancy       float64     `json:"minredundancy"`
		Mode                os.FileMode `json:"mode"`
		ModTime             time.Time   `json:"modtime"`
		NumFiles            uint64      `json:"numfiles"`
		NumStuckChunks      uint64      `json:"numstuckchunks"`
		NumSubDirs          uint64      `json:"numsubdirs"`
		Size                uint64      `json:"size"`
		StuckHealth         float64     `json:"stuckhealth"`

		// Version is the used version of the header file.
		Version string `json:"version"`
	}
)

// mdPath returns the path of the I3vDir's metadata on disk.
func (sd *I3vDir) mdPath() string {
	return filepath.Join(sd.path, modules.I3vDirExtension)
}

// Deleted returns the deleted field of the i3vDir
func (sd *I3vDir) Deleted() bool {
	sd.mu.Lock()
	defer sd.mu.Unlock()
	return sd.deleted
}

// Metadata returns the metadata of the I3vDir
func (sd *I3vDir) Metadata() Metadata {
	sd.mu.Lock()
	defer sd.mu.Unlock()
	return sd.metadata
}

// Path returns the path of the I3vDir on disk.
func (sd *I3vDir) Path() string {
	sd.mu.Lock()
	defer sd.mu.Unlock()
	return sd.path
}

// MDPath returns the path of the I3vDir's metadata on disk.
func (sd *I3vDir) MDPath() string {
	sd.mu.Lock()
	defer sd.mu.Unlock()
	return sd.mdPath()
}
