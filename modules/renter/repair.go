package renter

import (
	"fmt"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/I3v/modules"
)

// TODO - once bubbling metadata has been updated to be more I/O
// efficient this code should be removed and we should call bubble when
// we clean up the upload chunk after a successful repair.

var (
	// errNoStuckFiles is a helper to indicate that there are no stuck files in
	// the renter's directory
	errNoStuckFiles = errors.New("no stuck files")

	// errNoStuckChunks is a helper to indicate that there are no stuck chunks
	// in a i3vfile
	errNoStuckChunks = errors.New("no stuck chunks")
)

// managedAddRandomStuckChunks will try and add up to
// maxRandomStuckChunksAddToHeap random stuck chunks to the upload heap
func (r *Renter) managedAddRandomStuckChunks(hosts map[string]struct{}) ([]modules.I3vPath, error) {
	var dirI3vPaths []modules.I3vPath
	// Remember number of stuck chunks we are starting with
	prevNumStuckChunks, prevNumRandomStuckChunks := r.uploadHeap.managedNumStuckChunks()
	// Check if there is space in the heap. There is space if the number of
	// random stuck chunks has not exceeded maxRandomStuckChunksInHeap and the
	// total number of stuck chunks as not exceeded maxStuckChunksInHeap
	spaceInHeap := prevNumRandomStuckChunks < maxRandomStuckChunksInHeap && prevNumStuckChunks < maxStuckChunksInHeap
	for i := 0; i < maxRandomStuckChunksAddToHeap && spaceInHeap; i++ {
		// Randomly get directory with stuck files
		dirI3vPath, err := r.managedStuckDirectory()
		if err != nil {
			return dirI3vPaths, errors.AddContext(err, "unable to get random stuck directory")
		}

		// Get Random stuck file from directory
		i3vPath, err := r.managedStuckFile(dirI3vPath)
		if err != nil {
			return dirI3vPaths, errors.AddContext(err, "unable to get random stuck file in dir "+dirI3vPath.String())
		}

		// Add stuck chunk to upload heap and signal repair needed
		err = r.managedBuildAndPushRandomChunk(i3vPath, hosts, targetStuckChunks)
		if err != nil {
			return dirI3vPaths, errors.AddContext(err, "unable to push random stuck chunk from '"+i3vPath.String()+"' of '"+dirI3vPath.String()+"'")
		}

		// Sanity check that stuck chunks were added
		currentNumStuckChunks, currentNumRandomStuckChunks := r.uploadHeap.managedNumStuckChunks()
		if currentNumRandomStuckChunks <= prevNumRandomStuckChunks {
			// If the number of stuck chunks in the heap is not increasing
			// then break out of this loop in order to prevent getting stuck
			// in an infinite loop
			break
		}

		// Remember the directory so bubble can be called on it at the end of
		// the iteration
		dirI3vPaths = append(dirI3vPaths, dirI3vPath)
		r.repairLog.Printf("Added %v stuck chunks from %s", currentNumRandomStuckChunks-prevNumRandomStuckChunks, dirI3vPath.String())
		prevNumStuckChunks = currentNumStuckChunks
		prevNumRandomStuckChunks = currentNumRandomStuckChunks
		spaceInHeap = prevNumRandomStuckChunks < maxRandomStuckChunksInHeap && prevNumStuckChunks < maxStuckChunksInHeap
	}
	return dirI3vPaths, nil
}

// managedAddStuckChunksFromStuckStack will try and add up to
// maxStuckChunksInHeap stuck chunks to the upload heap from the files in the
// stuck stack.
func (r *Renter) managedAddStuckChunksFromStuckStack(hosts map[string]struct{}) ([]modules.I3vPath, error) {
	var dirI3vPaths []modules.I3vPath
	offline, goodForRenew, _ := r.managedContractUtilityMaps()
	numStuckChunks, _ := r.uploadHeap.managedNumStuckChunks()
	for r.stuckStack.managedLen() > 0 && numStuckChunks < maxStuckChunksInHeap {
		// Pop the first file I3vPath
		i3vPath := r.stuckStack.managedPop()

		// Add stuck chunks to uploadHeap
		err := r.managedAddStuckChunksToHeap(i3vPath, hosts, offline, goodForRenew)
		if err != nil && err != errNoStuckChunks {
			return dirI3vPaths, errors.AddContext(err, "unable to add stuck chunks to heap")
		}

		// Since we either added stuck chunks to the heap from this file,
		// there are no stuck chunks left in the file, or all the stuck
		// chunks for the file are already being worked on, remember the
		// directory so we can call bubble on it at the end of this
		// iteration of the stuck loop to update the filesystem
		dirI3vPath, err := i3vPath.Dir()
		if err != nil {
			return dirI3vPaths, errors.AddContext(err, "unable to get directory i3vpath")
		}
		dirI3vPaths = append(dirI3vPaths, dirI3vPath)
		numStuckChunks, _ = r.uploadHeap.managedNumStuckChunks()
	}
	return dirI3vPaths, nil
}

// managedAddStuckChunksToHeap tries to add as many stuck chunks from a i3vfile
// to the upload heap as possible
func (r *Renter) managedAddStuckChunksToHeap(i3vPath modules.I3vPath, hosts map[string]struct{}, offline, goodForRenew map[string]bool) error {
	// Open File
	sf, err := r.staticFileSystem.OpenI3vFile(i3vPath)
	if err != nil {
		return fmt.Errorf("unable to open i3vfile %v, error: %v", i3vPath, err)
	}
	defer sf.Close()

	// Check if there are still stuck chunks to repair
	if sf.NumStuckChunks() == 0 {
		return errNoStuckChunks
	}

	// Build unfinished stuck chunks
	var allErrors error
	unfinishedStuckChunks := r.managedBuildUnfinishedChunks(sf, hosts, targetStuckChunks, offline, goodForRenew)
	defer func() {
		// Close out remaining file entries
		for _, chunk := range unfinishedStuckChunks {
			chunk.fileEntry.Close()
		}
	}()

	// Add up to maxStuckChunksInHeap stuck chunks to the upload heap
	var chunk *unfinishedUploadChunk
	stuckChunksAdded := 0
	for len(unfinishedStuckChunks) > 0 && stuckChunksAdded < maxStuckChunksInHeap {
		chunk = unfinishedStuckChunks[0]
		unfinishedStuckChunks = unfinishedStuckChunks[1:]
		chunk.stuckRepair = true
		chunk.fileRecentlySuccessful = true
		if !r.uploadHeap.managedPush(chunk) {
			// Stuck chunk unable to be added. Close the file entry of that
			// chunk
			chunk.fileEntry.Close()
			continue
		}
		stuckChunksAdded++
	}
	if stuckChunksAdded > 0 {
		r.repairLog.Printf("Added %v stuck chunks from %s to the repair heap", stuckChunksAdded, i3vPath.String())
	}

	// check if there are more stuck chunks in the file
	if len(unfinishedStuckChunks) > 0 {
		r.stuckStack.managedPush(i3vPath)
	}
	return allErrors
}

// managedOldestHealthCheckTime finds the lowest level directory with the oldest
// LastHealthCheckTime
func (r *Renter) managedOldestHealthCheckTime() (modules.I3vPath, time.Time, error) {
	// Check the i3vdir metadata for the root files directory
	i3vPath := modules.RootI3vPath()
	metadata, err := r.managedDirectoryMetadata(i3vPath)
	if err != nil {
		return modules.I3vPath{}, time.Time{}, err
	}

	// Follow the path of oldest LastHealthCheckTime to the lowest level
	// directory
	for metadata.NumSubDirs > 0 {
		// Check to make sure renter hasn't been shutdown
		select {
		case <-r.tg.StopChan():
			return modules.I3vPath{}, time.Time{}, errors.New("Renter shutdown before oldestHealthCheckTime could be found")
		default:
		}

		// Check for sub directories
		subDirI3vPaths, err := r.managedSubDirectories(i3vPath)
		if err != nil {
			return modules.I3vPath{}, time.Time{}, err
		}

		// Find the oldest LastHealthCheckTime of the sub directories
		updated := false
		for _, subDirPath := range subDirI3vPaths {
			// Check to make sure renter hasn't been shutdown
			select {
			case <-r.tg.StopChan():
				return modules.I3vPath{}, time.Time{}, errors.New("Renter shutdown before oldestHealthCheckTime could be found")
			default:
			}

			// Check lastHealthCheckTime of sub directory
			subMetadata, err := r.managedDirectoryMetadata(subDirPath)
			if err != nil {
				return modules.I3vPath{}, time.Time{}, err
			}

			// If the LastHealthCheckTime is after current LastHealthCheckTime
			// continue since we are already in a directory with an older
			// timestamp
			if subMetadata.AggregateLastHealthCheckTime.After(metadata.AggregateLastHealthCheckTime) {
				continue
			}

			// Update LastHealthCheckTime and follow older path
			updated = true
			metadata = subMetadata
			i3vPath = subDirPath
		}

		// If the values were never updated with any of the sub directory values
		// then return as we are in the directory we are looking for
		if !updated {
			return i3vPath, metadata.AggregateLastHealthCheckTime, nil
		}
	}

	return i3vPath, metadata.AggregateLastHealthCheckTime, nil
}

// managedStuckDirectory randomly finds a directory that contains stuck chunks
func (r *Renter) managedStuckDirectory() (modules.I3vPath, error) {
	// Iterating of the renter directory until randomly ending up in a
	// directory, break and return that directory
	i3vPath := modules.RootI3vPath()
	for {
		select {
		// Check to make sure renter hasn't been shutdown
		case <-r.tg.StopChan():
			return modules.I3vPath{}, nil
		default:
		}

		_, directories, err := r.staticFileSystem.CachedList(i3vPath, false)
		if err != nil {
			return modules.I3vPath{}, err
		}
		// Sanity check that there is at least the current directory
		if len(directories) == 0 {
			build.Critical("No directories returned from DirList", i3vPath.String())
		}

		// Check if we are in an empty Directory. This will be the case before
		// any files have been uploaded so the root directory is empty. Also it
		// could happen if the only file in a directory was stuck and was very
		// recently deleted so the health of the directory has not yet been
		// updated.
		emptyDir := len(directories) == 1 && directories[0].NumFiles == 0
		if emptyDir {
			return i3vPath, errNoStuckFiles
		}
		// Check if there are stuck chunks in this directory
		if directories[0].AggregateNumStuckChunks == 0 {
			// Log error if we are not at the root directory
			if !i3vPath.IsRoot() {
				r.log.Debugln("WARN: ended up in directory with no stuck chunks that is not root directory:", i3vPath)
			}
			return i3vPath, errNoStuckFiles
		}
		// Check if we have reached a directory with only files
		if len(directories) == 1 {
			return i3vPath, nil
		}

		// Get random int
		rand := fastrand.Intn(int(directories[0].AggregateNumStuckChunks))
		// Use rand to decide which directory to go into. Work backwards over
		// the slice of directories. Since the first element is the current
		// directory that means that it is the sum of all the files and
		// directories.  We can chose a directory by subtracting the number of
		// stuck chunks a directory has from rand and if rand gets to 0 or less
		// we choose that directory
		for i := len(directories) - 1; i >= 0; i-- {
			// If we are on the last iteration and the directory does have files
			// then return the current directory
			if i == 0 {
				i3vPath = directories[0].I3vPath
				return i3vPath, nil
			}

			// Skip directories with no stuck chunks
			if directories[i].AggregateNumStuckChunks == uint64(0) {
				continue
			}

			rand = rand - int(directories[i].AggregateNumStuckChunks)
			i3vPath = directories[i].I3vPath
			// If rand is less than 0 break out of the loop and continue into
			// that directory
			if rand < 0 {
				break
			}
		}
	}
}

// managedStuckFile finds a weighted random stuck file from a directory based on
// the number of stuck chunks in the stuck files of the directory
func (r *Renter) managedStuckFile(dirI3vPath modules.I3vPath) (i3vpath modules.I3vPath, err error) {
	// Grab Aggregate number of stuck chunks from the directory
	//
	// NOTE: using the aggregate number of stuck chunks assumes that the
	// directory and the files within the directory are in sync. This is ok to
	// do as the risks associated with being out of sync are low.
	i3vDir, err := r.staticFileSystem.OpenI3vDir(dirI3vPath)
	if err != nil {
		return modules.I3vPath{}, errors.AddContext(err, "unable to open i3vDir "+dirI3vPath.String())
	}
	defer i3vDir.Close()
	metadata, err := i3vDir.Metadata()
	if err != nil {
		return modules.I3vPath{}, err
	}
	aggregateNumStuckChunks := metadata.AggregateNumStuckChunks
	numStuckChunks := metadata.NumStuckChunks
	numFiles := metadata.NumFiles
	if aggregateNumStuckChunks == 0 || numStuckChunks == 0 || numFiles == 0 {
		// If the number of stuck chunks or number of files is zero then this
		// directory should not have been used to find a stuck file. Call bubble
		// to make sure that the metadata is updated
		go r.callThreadedBubbleMetadata(dirI3vPath)
		err = fmt.Errorf("managedStuckFile should not have been called on %v, AggregateNumStuckChunks: %v, NumStuckChunks: %v, NumFiles: %v", dirI3vPath.String(), aggregateNumStuckChunks, numStuckChunks, numFiles)
		return modules.I3vPath{}, err
	}

	// Use rand to decide which file to select. We can chose a file by
	// subtracting the number of stuck chunks a file has from rand and if rand
	// gets to 0 or less we choose that file
	rand := fastrand.Intn(int(aggregateNumStuckChunks))

	// Read the directory, using ReadDir so we don't read all the i3vfiles
	// unless we need to
	fileinfos, err := r.staticFileSystem.ReadDir(dirI3vPath)
	if err != nil {
		return modules.I3vPath{}, errors.AddContext(err, "unable to open i3vdir: "+dirI3vPath.String())
	}
	// Iterate over the fileinfos
	for _, fi := range fileinfos {
		// Check for I3vFile
		if fi.IsDir() || filepath.Ext(fi.Name()) != modules.I3vFileExtension {
			continue
		}

		// Get I3vPath
		sp, err := dirI3vPath.Join(strings.TrimSuffix(fi.Name(), modules.I3vFileExtension))
		if err != nil {
			return modules.I3vPath{}, errors.AddContext(err, "unable to join the i3vpath with the file: "+fi.Name())
		}

		// Open I3vFile, grab the number of stuck chunks and close the file
		f, err := r.staticFileSystem.OpenI3vFile(sp)
		if err != nil {
			return modules.I3vPath{}, errors.AddContext(err, "could not open i3vfileset for "+sp.String())
		}
		numStuckChunks := int(f.NumStuckChunks())
		f.Close()

		// Check if stuck
		if numStuckChunks == 0 {
			continue
		}

		// Decrement rand and check if we have decremented fully
		rand = rand - numStuckChunks
		i3vpath = sp
		if rand < 0 {
			break
		}
	}
	if i3vpath.IsEmpty() {
		// If no files were selected from the directory than there is a mismatch
		// between the file metadata and the directory metadata. Call bubble to
		// update the directory metadata
		go r.callThreadedBubbleMetadata(dirI3vPath)
		return modules.I3vPath{}, errors.New("no files selected from directory " + dirI3vPath.String())
	}
	return i3vpath, nil
}

// managedSubDirectories reads a directory and returns a slice of all the sub
// directory I3vPaths
func (r *Renter) managedSubDirectories(i3vPath modules.I3vPath) ([]modules.I3vPath, error) {
	// Read directory
	fileinfos, err := r.staticFileSystem.ReadDir(i3vPath)
	if err != nil {
		return nil, err
	}
	// Find all sub directory I3vPaths
	folders := make([]modules.I3vPath, 0, len(fileinfos))
	for _, fi := range fileinfos {
		if fi.IsDir() {
			subDir, err := i3vPath.Join(fi.Name())
			if err != nil {
				return nil, err
			}
			folders = append(folders, subDir)
		}
	}
	return folders, nil
}

// threadedStuckFileLoop works through the renter directory and finds the stuck
// chunks and tries to repair them
func (r *Renter) threadedStuckFileLoop() {
	err := r.tg.Add()
	if err != nil {
		return
	}
	defer r.tg.Done()

	// Loop until the renter has shutdown or until there are no stuck chunks
	for {
		// Return if the renter has shut down.
		select {
		case <-r.tg.StopChan():
			return
		default:
		}

		// Wait until the renter is online to proceed.
		if !r.managedBlockUntilOnline() {
			// The renter shut down before the internet connection was restored.
			r.log.Debugln("renter shutdown before internet connection")
			return
		}

		// As we add stuck chunks to the upload heap we want to remember the
		// directories they came from so we can call bubble to update the
		// filesystem
		var dirI3vPaths []modules.I3vPath

		// Refresh the hosts and workers before adding stuck chunks to the
		// upload heap
		hosts := r.managedRefreshHostsAndWorkers()

		// Try and add stuck chunks from the stuck stack. We try and add these
		// first as they will be from files that previously had a successful
		// stuck chunk repair. The previous success gives us more confidence
		// that it is more likely additional stuck chunks from these files will
		// be successful compared to a random stuck chunk from the renter's
		// directory.
		stuckStackDirI3vPaths, err := r.managedAddStuckChunksFromStuckStack(hosts)
		if err != nil {
			r.repairLog.Println("WARN: error adding stuck chunks to repair heap from files with previously successful stuck repair jobs:", err)
		}
		dirI3vPaths = append(dirI3vPaths, stuckStackDirI3vPaths...)

		// Try add random stuck chunks to upload heap
		randomDirI3vPaths, err := r.managedAddRandomStuckChunks(hosts)
		if err != nil {
			r.repairLog.Println("WARN: error adding random stuck chunks to upload heap:", err)
		}
		dirI3vPaths = append(dirI3vPaths, randomDirI3vPaths...)

		// Check if any stuck chunks were added to the upload heap
		numStuckChunks, _ := r.uploadHeap.managedNumStuckChunks()
		if numStuckChunks == 0 {
			// Block until new work is required.
			select {
			case <-r.tg.StopChan():
				// The renter has shut down.
				return
			case <-r.uploadHeap.stuckChunkFound:
				// Health Loop found stuck chunk
			case <-r.uploadHeap.stuckChunkSuccess:
				// Stuck chunk was successfully repaired.
			}
			continue
		}

		// Signal that a repair is needed because stuck chunks were added to the
		// upload heap
		select {
		case r.uploadHeap.repairNeeded <- struct{}{}:
		default:
		}

		// Sleep until it is time to try and repair another stuck chunk
		rebuildStuckHeapSignal := time.After(repairStuckChunkInterval)
		select {
		case <-r.tg.StopChan():
			// Return if the return has been shutdown
			return
		case <-rebuildStuckHeapSignal:
			// Time to find another random chunk
		case <-r.uploadHeap.stuckChunkSuccess:
			// Stuck chunk was successfully repaired.
		}

		// Call bubble before continuing on next iteration to ensure filesystem
		// is updated.
		//
		// TODO - once bubbling metadata has been updated to be more I/O
		// efficient this code should be removed and we should call bubble when
		// we clean up the upload chunk after a successful repair.
		for _, dirI3vPath := range dirI3vPaths {
			err = r.managedBubbleMetadata(dirI3vPath)
			if err != nil {
				r.repairLog.Printf("Error propagating updated health of %s: %v", dirI3vPath.String(), err)
				select {
				case <-time.After(stuckLoopErrorSleepDuration):
				case <-r.tg.StopChan():
					return
				}
			}
		}
	}
}

// threadedUpdateRenterHealth reads all the i3vfiles in the renter, calculates
// the health of each file and updates the folder metadata
func (r *Renter) threadedUpdateRenterHealth() {
	err := r.tg.Add()
	if err != nil {
		return
	}
	defer r.tg.Done()

	// Loop until the renter has shutdown or until the renter's top level files
	// directory has a LasHealthCheckTime within the healthCheckInterval
	for {
		select {
		// Check to make sure renter hasn't been shutdown
		case <-r.tg.StopChan():
			return
		default:
		}

		// Follow path of oldest time, return directory and timestamp
		r.log.Debugln("Checking for oldest health check time")
		i3vPath, lastHealthCheckTime, err := r.managedOldestHealthCheckTime()
		if err != nil {
			// If there is an error getting the lastHealthCheckTime sleep for a
			// little bit before continuing
			r.log.Debug("WARN: Could not find oldest health check time:", err)
			select {
			case <-time.After(healthLoopErrorSleepDuration):
			case <-r.tg.StopChan():
				return
			}
			continue
		}

		// Check if the time since the last check on the least recently checked
		// folder is inside the health check interval. If so, the whole
		// filesystem has been checked recently, and we can sleep until the
		// least recent check is outside the check interval.
		timeSinceLastCheck := time.Since(lastHealthCheckTime)
		if timeSinceLastCheck < healthCheckInterval {
			// Sleep until the least recent check is outside the check interval.
			sleepDuration := healthCheckInterval - timeSinceLastCheck
			r.log.Debugln("Health loop sleeping for", sleepDuration)
			wakeSignal := time.After(sleepDuration)
			select {
			case <-r.tg.StopChan():
				return
			case <-wakeSignal:
			}
		}
		r.log.Debug("Health Loop calling bubble on '", i3vPath.String(), "'")
		err = r.managedBubbleMetadata(i3vPath)
		if err != nil {
			r.log.Println("Error calling managedBubbleMetadata on `", i3vPath.String(), "`:", err)
			select {
			case <-time.After(healthLoopErrorSleepDuration):
			case <-r.tg.StopChan():
				return
			}
		}
	}
}
