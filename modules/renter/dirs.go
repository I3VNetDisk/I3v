package renter

import (
	"os"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/errors"
)

// CreateDir creates a directory for the renter
func (r *Renter) CreateDir(i3vPath modules.I3vPath, mode os.FileMode) error {
	err := r.tg.Add()
	if err != nil {
		return err
	}
	defer r.tg.Done()
	return r.staticFileSystem.NewI3vDir(i3vPath, mode)
}

// DeleteDir removes a directory from the renter and deletes all its sub
// directories and files
func (r *Renter) DeleteDir(i3vPath modules.I3vPath) error {
	if err := r.tg.Add(); err != nil {
		return err
	}
	defer r.tg.Done()
	return r.staticFileSystem.DeleteDir(i3vPath)
}

// DirList lists the directories in a i3vdir
func (r *Renter) DirList(i3vPath modules.I3vPath) ([]modules.DirectoryInfo, error) {
	if err := r.tg.Add(); err != nil {
		return nil, err
	}
	defer r.tg.Done()
	_, dis, err := r.staticFileSystem.CachedList(i3vPath, false)
	return dis, err
}

// RenameDir takes an existing directory and changes the path. The original
// directory must exist, and there must not be any directory that already has
// the replacement path.  All i3v files within directory will also be renamed
func (r *Renter) RenameDir(oldPath, newPath modules.I3vPath) error {
	if err := r.tg.Add(); err != nil {
		return err
	}
	defer r.tg.Done()

	// Special case: do not allow a user to rename a dir to root.
	if newPath.IsRoot() {
		return errors.New("cannot rename a file to the root directory")
	}
	return r.staticFileSystem.RenameDir(oldPath, newPath)
}
