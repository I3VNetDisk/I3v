package i3vfile

import (
	"path/filepath"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/writeaheadlog"
)

// CombinedChunkIndex is a helper method which translates a chunk's index to the
// corresponding combined chunk index dependng on the number of combined chunks.
func CombinedChunkIndex(numChunks, chunkIndex uint64, numCombinedChunks int) int {
	if numCombinedChunks == 1 && chunkIndex == numChunks-1 {
		return 0
	}
	if numCombinedChunks == 2 && chunkIndex == numChunks-2 {
		return 0
	}
	if numCombinedChunks == 2 && chunkIndex == numChunks-1 {
		return 1
	}
	return -1
}

// Merge merges two PartialsI3vfiles into one, returning a map which translates
// chunk indices in newFile to indices in sf.
func (sf *I3vFile) Merge(newFile *I3vFile) (map[uint64]uint64, error) {
	sf.mu.Lock()
	defer sf.mu.Unlock()
	return sf.merge(newFile)
}

// addCombinedChunk adds a new combined chunk to a combined I3vfile. This can't
// be called on a regular I3vFile.
func (sf *I3vFile) addCombinedChunk() ([]writeaheadlog.Update, error) {
	if sf.deleted {
		return nil, errors.New("can't add combined chunk to deleted file")
	}
	if filepath.Ext(sf.i3vFilePath) != modules.PartialsI3vFileExtension {
		return nil, errors.New("can only call addCombinedChunk on combined I3vFiles")
	}
	// Create updates to add a chunk and return index of that new chunk.
	updates, err := sf.growNumChunks(uint64(sf.numChunks) + 1)
	return updates, err
}

// merge merges two PartialsI3vfiles into one, returning a map which translates
// chunk indices in newFile to indices in sf.
func (sf *I3vFile) merge(newFile *I3vFile) (map[uint64]uint64, error) {
	if sf.deleted {
		return nil, errors.New("can't merge into deleted file")
	}
	if filepath.Ext(sf.i3vFilePath) != modules.PartialsI3vFileExtension {
		return nil, errors.New("can only call merge on PartialsI3vFile")
	}
	if filepath.Ext(newFile.I3vFilePath()) != modules.PartialsI3vFileExtension {
		return nil, errors.New("can only merge PartialsI3vfiles into a PartialsI3vFile")
	}
	newFile.mu.Lock()
	defer newFile.mu.Unlock()
	if newFile.deleted {
		return nil, errors.New("can't merge deleted file")
	}
	var newChunks []chunk
	indexMap := make(map[uint64]uint64)
	ncb := sf.numChunks
	err := newFile.iterateChunksReadonly(func(chunk chunk) error {
		newIndex := sf.numChunks
		indexMap[uint64(chunk.Index)] = uint64(newIndex)
		chunk.Index = newIndex
		newChunks = append(newChunks, chunk)
		return nil
	})
	if err != nil {
		sf.numChunks = ncb
		return nil, err
	}
	return indexMap, sf.saveFile(newChunks)
}
