package i3vfile

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/writeaheadlog"
)

// createLinkedBlankI3vfile creates 2 I3vFiles which use the same I3vFile to
// store combined chunks. They reside within 'dir'.
//
//lint:file-ignore U1000 Ignore unused code, it's for future partial upload code
func createLinkedBlankI3vfiles(dir string) (*I3vFile, *I3vFile, error) {
	// Create a wal.
	walFilePath := filepath.Join(dir, "writeaheadlog.wal")
	_, wal, err := writeaheadlog.New(walFilePath)
	if err != nil {
		return nil, nil, err
	}
	// Get parameters for the files.
	_, _, source, rc, sk, fileSize, numChunks, fileMode := newTestFileParams(1, true)
	// Create a I3vFile for partial chunks.
	var partialsI3vFile *I3vFile
	partialsI3vPath := modules.CombinedI3vFilePath(rc)
	partialsI3vFilePath := partialsI3vPath.I3vPartialsFileSysPath(dir)
	if _, err = os.Stat(partialsI3vFilePath); os.IsNotExist(err) {
		partialsI3vFile, err = New(partialsI3vFilePath, "", wal, rc, sk, 0, fileMode, nil, false)
	} else {
		partialsI3vFile, err = LoadI3vFile(partialsI3vFilePath, wal)
	}
	if err != nil {
		return nil, nil, fmt.Errorf("failed to load partialsI3vFile: %v", err)
	}
	/*
		 PARTIAL TODO:
			partialsEntry := &I3vFileSetEntry{
				dummyEntry(partialsI3vFile),
				uint64(fastrand.Intn(math.MaxInt32)),
			}
	*/
	// Create the files.
	sf1Path := filepath.Join(dir, "sf1"+modules.I3vFileExtension)
	sf2Path := filepath.Join(dir, "sf2"+modules.I3vFileExtension)
	sf1, err := New(sf1Path, source, wal, rc, sk, fileSize, fileMode, nil, false)
	if err != nil {
		return nil, nil, err
	}
	sf2, err := New(sf2Path, source, wal, rc, sk, fileSize, fileMode, nil, false)
	if err != nil {
		return nil, nil, err
	}
	// Check that the number of chunks in the files are correct.
	if numChunks >= 0 && sf1.numChunks != numChunks {
		return nil, nil, errors.New("createLinkedBlankI3vfiles didn't create the expected number of chunks")
	}
	if numChunks >= 0 && sf2.numChunks != numChunks {
		return nil, nil, errors.New("createLinkedBlankI3vfiles didn't create the expected number of chunks")
	}
	if partialsI3vFile.numChunks != 0 {
		return nil, nil, errors.New("createLinkedBlankI3vfiles didn't create an empty partialsI3vFile")
	}
	return sf1, sf2, nil
}
