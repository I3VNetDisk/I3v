package filesystem

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vdir"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vfile"
	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/writeaheadlog"

	"gitlab.com/I3VNetDisk/I3v/build"
)

// newTestFileSystemWithFile creates a new FileSystem and I3vFile and makes sure
// that they are linked
func newTestFileSystemWithFile(name string) (*FileNode, *FileSystem, error) {
	dir := testDir(name)
	fs := newTestFileSystem(dir)
	sp := modules.RandomI3vPath()
	fs.AddTestI3vFile(sp)
	sf, err := fs.OpenI3vFile(sp)
	return sf, fs, err
}

// testDir creates a testing directory for a filesystem test.
func testDir(name string) string {
	dir := build.TempDir(name, filepath.Join("filesystem"))
	if err := os.MkdirAll(dir, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return dir
}

// newI3vPath creates a new i3vpath from the specified string.
func newI3vPath(path string) modules.I3vPath {
	sp, err := modules.NewI3vPath(path)
	if err != nil {
		panic(err)
	}
	return sp
}

// newTestFileSystem creates a new filesystem for testing.
func newTestFileSystem(root string) *FileSystem {
	wal, _ := newTestWAL()
	fs, err := New(root, persist.NewLogger(ioutil.Discard), wal)
	if err != nil {
		panic(err.Error())
	}
	return fs
}

// newTestWal is a helper method to create a WAL for testing.
func newTestWAL() (*writeaheadlog.WAL, string) {
	// Create the wal.
	walsDir := filepath.Join(os.TempDir(), "wals")
	if err := os.MkdirAll(walsDir, 0700); err != nil {
		panic(err)
	}
	walFilePath := filepath.Join(walsDir, hex.EncodeToString(fastrand.Bytes(8)))
	_, wal, err := writeaheadlog.New(walFilePath)
	if err != nil {
		panic(err)
	}
	return wal, walFilePath
}

// AddTestI3vFile is a convenience method to add a I3vFile for testing to a
// FileSystem.
func (fs *FileSystem) AddTestI3vFile(i3vPath modules.I3vPath) {
	if err := fs.AddTestI3vFileWithErr(i3vPath); err != nil {
		panic(err)
	}
}

// AddTestI3vFileWithErr is a convenience method to add a I3vFile for testing to
// a FileSystem.
func (fs *FileSystem) AddTestI3vFileWithErr(i3vPath modules.I3vPath) error {
	ec, err := i3vfile.NewRSSubCode(10, 20, crypto.SegmentSize)
	if err != nil {
		return err
	}
	err = fs.NewI3vFile(i3vPath, "", ec, crypto.GenerateI3vKey(crypto.TypeDefaultRenter), uint64(fastrand.Intn(100)), persist.DefaultDiskPermissionsTest, false)
	if err != nil {
		return err
	}
	return nil
}

// TestNew tests creating a new FileSystem.
func TestNew(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Check fields.
	if fs.parent != nil {
		t.Fatalf("fs.parent shoud be 'nil' but wasn't")
	}
	if *fs.name != "" {
		t.Fatalf("fs.staticName should be %v but was %v", "", *fs.name)
	}
	if *fs.path != root {
		t.Fatalf("fs.path should be %v but was %v", root, *fs.path)
	}
	if fs.threads == nil || len(fs.threads) != 0 {
		t.Fatal("fs.threads is not an empty initialized map")
	}
	if fs.threadUID != 0 {
		t.Fatalf("fs.threadUID should be 0 but was %v", fs.threadUID)
	}
	if fs.directories == nil || len(fs.directories) != 0 {
		t.Fatal("fs.directories is not an empty initialized map")
	}
	if fs.files == nil || len(fs.files) != 0 {
		t.Fatal("fs.files is not an empty initialized map")
	}
	// Create the filesystem again at the same location.
	_ = newTestFileSystem(*fs.path)
}

// TestNewI3vDir tests if creating a new directory using NewI3vDir creates the
// correct folder structure.
func TestNewI3vDir(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create dir /sub/foo
	sp := newI3vPath("sub/foo")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// The whole path should exist.
	if _, err := os.Stat(filepath.Join(root, sp.String())); err != nil {
		t.Fatal(err)
	}
}

// TestNewI3vFile tests if creating a new file using NewI3vFiles creates the
// correct folder structure and file.
func TestNewI3vFile(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create file /sub/foo/file
	sp := newI3vPath("sub/foo/file")
	fs.AddTestI3vFile(sp)
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != ErrExists {
		t.Fatal("err should be ErrExists but was", err)
	}
	if _, err := os.Stat(filepath.Join(root, sp.String())); !os.IsNotExist(err) {
		t.Fatal("there should be no dir on disk")
	}
	if _, err := os.Stat(filepath.Join(root, sp.String()+modules.I3vFileExtension)); err != nil {
		t.Fatal(err)
	}
	// Create a file in the root dir.
	sp = newI3vPath("file")
	fs.AddTestI3vFile(sp)
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != ErrExists {
		t.Fatal("err should be ErrExists but was", err)
	}
	if _, err := os.Stat(filepath.Join(root, sp.String())); !os.IsNotExist(err) {
		t.Fatal("there should be no dir on disk")
	}
	if _, err := os.Stat(filepath.Join(root, sp.String()+modules.I3vFileExtension)); err != nil {
		t.Fatal(err)
	}
}

func (d *DirNode) checkNode(numThreads, numDirs, numFiles int) error {
	if len(d.threads) != numThreads {
		return fmt.Errorf("Expected d.threads to have length %v but was %v", numThreads, len(d.threads))
	}
	if len(d.directories) != numDirs {
		return fmt.Errorf("Expected %v subdirectories in the root but got %v", numDirs, len(d.directories))
	}
	if len(d.files) != numFiles {
		return fmt.Errorf("Expected %v files in the root but got %v", numFiles, len(d.files))
	}
	return nil
}

// TestOpenI3vDir confirms that a previoiusly created I3vDir can be opened and
// that the filesystem tree is extended accordingly in the process.
func TestOpenI3vDir(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create dir /foo
	sp := newI3vPath("foo")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// Open the newly created dir.
	foo, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer foo.Close()
	// Create dir /sub/foo
	sp = newI3vPath("sub/foo")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// Open the newly created dir.
	sd, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer sd.Close()
	// Confirm the integrity of the root node.
	if err := fs.checkNode(0, 2, 0); err != nil {
		t.Fatal(err)
	}
	// Open the root node manually and confirm that they are the same.
	rootSD, err := fs.OpenI3vDir(modules.RootI3vPath())
	if err != nil {
		t.Fatal(err)
	}
	if err := fs.checkNode(len(rootSD.threads), len(rootSD.directories), len(rootSD.files)); err != nil {
		t.Fatal(err)
	}
	// Confirm the integrity of the /sub node.
	subNode, exists := fs.directories["sub"]
	if !exists {
		t.Fatal("expected root to contain the 'sub' node")
	}
	if *subNode.name != "sub" {
		t.Fatalf("subNode name should be 'sub' but was %v", *subNode.name)
	}
	if path := filepath.Join(*subNode.parent.path, *subNode.name); path != *subNode.path {
		t.Fatalf("subNode path should be %v but was %v", path, *subNode.path)
	}
	if err := subNode.checkNode(0, 1, 0); err != nil {
		t.Fatal(err)
	}
	// Confirm the integrity of the /sub/foo node.
	fooNode, exists := subNode.directories["foo"]
	if !exists {
		t.Fatal("expected /sub to contain /sub/foo")
	}
	if *fooNode.name != "foo" {
		t.Fatalf("fooNode name should be 'foo' but was %v", *fooNode.name)
	}
	if path := filepath.Join(*fooNode.parent.path, *fooNode.name); path != *fooNode.path {
		t.Fatalf("fooNode path should be %v but was %v", path, *fooNode.path)
	}
	if err := fooNode.checkNode(1, 0, 0); err != nil {
		t.Fatal(err)
	}
	// Open the newly created dir again.
	sd2, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer sd2.Close()
	// They should have different UIDs.
	if sd.threadUID == 0 {
		t.Fatal("threaduid shouldn't be 0")
	}
	if sd2.threadUID == 0 {
		t.Fatal("threaduid shouldn't be 0")
	}
	if sd.threadUID == sd2.threadUID {
		t.Fatal("sd and sd2 should have different threaduids")
	}
	if len(sd.threads) != 2 || len(sd2.threads) != 2 {
		t.Fatal("sd and sd2 should both have 2 threads registered")
	}
	_, exists1 := sd.threads[sd.threadUID]
	_, exists2 := sd.threads[sd2.threadUID]
	_, exists3 := sd2.threads[sd.threadUID]
	_, exists4 := sd2.threads[sd2.threadUID]
	if exists := exists1 && exists2 && exists3 && exists4; !exists {
		t.Fatal("sd and sd1's threads don't contain the right uids")
	}
	// Open /sub manually and make sure that subDir and sdSub are consistent.
	sdSub, err := fs.OpenI3vDir(newI3vPath("sub"))
	if err != nil {
		t.Fatal(err)
	}
	defer sdSub.Close()
	if err := subNode.checkNode(1, 1, 0); err != nil {
		t.Fatal(err)
	}
	if err := sdSub.checkNode(1, 1, 0); err != nil {
		t.Fatal(err)
	}
}

// TestOpenI3vFile confirms that a previously created I3vFile can be opened and
// that the filesystem tree is extended accordingly in the process.
func TestOpenI3vFile(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create file /file
	sp := newI3vPath("file")
	fs.AddTestI3vFile(sp)
	// Open the newly created file.
	sf, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer sf.Close()
	// Confirm the integrity of the file.
	if *sf.name != "file" {
		t.Fatalf("name of file should be file but was %v", *sf.name)
	}
	if *sf.path != filepath.Join(root, (*sf.name)+modules.I3vFileExtension) {
		t.Fatal("file has wrong path", *sf.path)
	}
	if sf.parent != &fs.DirNode {
		t.Fatalf("parent of file should be %v but was %v", &fs.node, sf.parent)
	}
	if sf.threadUID == 0 {
		t.Fatal("threaduid wasn't set")
	}
	if len(sf.threads) != 1 {
		t.Fatalf("len(threads) should be 1 but was %v", len(sf.threads))
	}
	if _, exists := sf.threads[sf.threadUID]; !exists {
		t.Fatal("threaduid doesn't exist in threads map")
	}
	// Confirm the integrity of the root node.
	if len(fs.threads) != 0 {
		t.Fatalf("Expected fs.threads to have length 0 but was %v", len(fs.threads))
	}
	if len(fs.directories) != 0 {
		t.Fatalf("Expected 0 subdirectories in the root but got %v", len(fs.directories))
	}
	if len(fs.files) != 1 {
		t.Fatalf("Expected 1 file in the root but got %v", len(fs.files))
	}
	// Create file /sub/file
	sp = newI3vPath("/sub1/sub2/file")
	fs.AddTestI3vFile(sp)
	// Open the newly created file.
	sf2, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer sf2.Close()
	// Confirm the integrity of the file.
	if *sf2.name != "file" {
		t.Fatalf("name of file should be file but was %v", *sf2.name)
	}
	if *sf2.parent.name != "sub2" {
		t.Fatalf("parent of file should be %v but was %v", "sub", *sf2.parent.name)
	}
	if sf2.threadUID == 0 {
		t.Fatal("threaduid wasn't set")
	}
	if len(sf2.threads) != 1 {
		t.Fatalf("len(threads) should be 1 but was %v", len(sf2.threads))
	}
	// Confirm the integrity of the "sub2" folder.
	sub2 := sf2.parent
	if err := sub2.checkNode(0, 0, 1); err != nil {
		t.Fatal(err)
	}
	if _, exists := sf2.threads[sf2.threadUID]; !exists {
		t.Fatal("threaduid doesn't exist in threads map")
	}
	// Confirm the integrity of the "sub1" folder.
	sub1 := sub2.parent
	if err := sub1.checkNode(0, 1, 0); err != nil {
		t.Fatal(err)
	}
	if _, exists := sf2.threads[sf2.threadUID]; !exists {
		t.Fatal("threaduid doesn't exist in threads map")
	}
}

// TestCloseI3vDir tests that closing an opened directory shrinks the tree
// accordingly.
func TestCloseI3vDir(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create dir /sub/foo
	sp := newI3vPath("sub1/foo")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// Open the newly created dir.
	sd, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	if len(sd.threads) != 1 {
		t.Fatalf("There should be 1 thread in sd.threads but got %v", len(sd.threads))
	}
	if len(sd.parent.threads) != 0 {
		t.Fatalf("The parent shouldn't have any threads but had %v", len(sd.parent.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 directory in fs.directories but got %v", len(fs.directories))
	}
	if len(sd.parent.directories) != 1 {
		t.Fatalf("The parent should have 1 directory but got %v", len(sd.parent.directories))
	}
	// After closing it the thread should be gone.
	sd.Close()
	if err := fs.checkNode(0, 0, 0); err != nil {
		t.Fatal(err)
	}
	// Open the dir again. This time twice.
	sd1, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	sd2, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	if len(sd1.threads) != 2 || len(sd2.threads) != 2 {
		t.Fatalf("There should be 2 threads in sd.threads but got %v", len(sd1.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 directory in fs.directories but got %v", len(fs.directories))
	}
	if len(sd1.parent.directories) != 1 || len(sd2.parent.directories) != 1 {
		t.Fatalf("The parent should have 1 directory but got %v", len(sd.parent.directories))
	}
	// Close one instance.
	sd1.Close()
	if len(sd1.threads) != 1 || len(sd2.threads) != 1 {
		t.Fatalf("There should be 1 thread in sd.threads but got %v", len(sd1.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 directory in fs.directories but got %v", len(fs.directories))
	}
	if len(sd1.parent.directories) != 1 || len(sd2.parent.directories) != 1 {
		t.Fatalf("The parent should have 1 directory but got %v", len(sd.parent.directories))
	}
	// Close the second one.
	sd2.Close()
	if len(fs.threads) != 0 {
		t.Fatalf("There should be 0 threads in fs.threads but got %v", len(fs.threads))
	}
	if len(sd1.threads) != 0 || len(sd2.threads) != 0 {
		t.Fatalf("There should be 0 threads in sd.threads but got %v", len(sd1.threads))
	}
	if len(fs.directories) != 0 {
		t.Fatalf("There should be 0 directories in fs.directories but got %v", len(fs.directories))
	}
}

// TestCloseI3vFile tests that closing an opened file shrinks the tree
// accordingly.
func TestCloseI3vFile(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create file /sub/file
	sp := newI3vPath("sub/file")
	fs.AddTestI3vFile(sp)
	// Open the newly created file.
	sf, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	if len(sf.threads) != 1 {
		t.Fatalf("There should be 1 thread in sf.threads but got %v", len(sf.threads))
	}
	if len(sf.parent.threads) != 0 {
		t.Fatalf("The parent shouldn't have any threads but had %v", len(sf.parent.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 directory in fs.directories but got %v", len(fs.directories))
	}
	if len(sf.parent.files) != 1 {
		t.Fatalf("The parent should have 1 file but got %v", len(sf.parent.files))
	}
	// After closing it the thread should be gone.
	sf.Close()
	if len(fs.threads) != 0 {
		t.Fatalf("There should be 0 threads in fs.threads but got %v", len(fs.threads))
	}
	if len(sf.threads) != 0 {
		t.Fatalf("There should be 0 threads in sd.threads but got %v", len(sf.threads))
	}
	if len(fs.files) != 0 {
		t.Fatalf("There should be 0 files in fs.files but got %v", len(fs.files))
	}
	// Open the file again. This time twice.
	sf1, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	sf2, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	if len(sf1.threads) != 2 || len(sf2.threads) != 2 {
		t.Fatalf("There should be 2 threads in sf1.threads but got %v", len(sf1.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 directory in fs.directories but got %v", len(fs.directories))
	}
	if len(sf1.parent.files) != 1 || len(sf2.parent.files) != 1 {
		t.Fatalf("The parent should have 1 file but got %v", len(sf1.parent.files))
	}
	// Close one instance.
	sf1.Close()
	if len(sf1.threads) != 1 || len(sf2.threads) != 1 {
		t.Fatalf("There should be 1 thread in sf1.threads but got %v", len(sf1.threads))
	}
	if len(fs.directories) != 1 {
		t.Fatalf("There should be 1 dir in fs.directories but got %v", len(fs.directories))
	}
	if len(sf1.parent.files) != 1 || len(sf2.parent.files) != 1 {
		t.Fatalf("The parent should have 1 file but got %v", len(sf1.parent.files))
	}
	if len(sf1.parent.parent.directories) != 1 {
		t.Fatalf("The root should have 1 directory but had %v", len(sf1.parent.parent.directories))
	}
	// Close the second one.
	sf2.Close()
	if len(fs.threads) != 0 {
		t.Fatalf("There should be 0 threads in fs.threads but got %v", len(fs.threads))
	}
	if len(sf1.threads) != 0 || len(sf2.threads) != 0 {
		t.Fatalf("There should be 0 threads in sd.threads but got %v", len(sf1.threads))
	}
	if len(fs.directories) != 0 {
		t.Fatalf("There should be 0 directories in fs.directories but got %v", len(fs.directories))
	}
	if len(sf1.parent.files) != 0 || len(sf2.parent.files) != 0 {
		t.Fatalf("The parent should have 0 files but got %v", len(sf1.parent.files))
	}
	if len(sf1.parent.parent.directories) != 0 {
		t.Fatalf("The root should have 0 directories but had %v", len(sf1.parent.parent.directories))
	}
}

// TestDeleteFile tests that deleting a file works as expected and that certain
// edge cases are covered.
func TestDeleteFile(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Add a file to the root dir.
	sp := newI3vPath("foo")
	fs.AddTestI3vFile(sp)
	// Open the file.
	sf, err := fs.OpenI3vFile(sp)
	if err != nil {
		t.Fatal(err)
	}
	// File shouldn't be deleted yet.
	if sf.Deleted() {
		t.Fatal("foo is deleted before calling delete")
	}
	// Delete it using the filesystem.
	if err := fs.DeleteFile(sp); err != nil {
		t.Fatal(err)
	}
	// Check that the open instance is marked as deleted.
	if !sf.Deleted() {
		t.Fatal("foo shuld be marked as deleted but wasn't")
	}
	// Check that we can't open another instance of foo and that we can't create
	// an new file at the same path.
	if _, err := fs.OpenI3vFile(sp); err != ErrNotExist {
		t.Fatal("err should be ErrNotExist but was:", err)
	}
	if err := fs.AddTestI3vFileWithErr(sp); err != nil {
		t.Fatal("err should be nil but was:", err)
	}
}

// TestDeleteDirectory tests if deleting a directory correctly and recursively
// removes the dir.
func TestDeleteDirectory(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Add some files.
	fs.AddTestI3vFile(newI3vPath("dir/foo/bar/file1"))
	fs.AddTestI3vFile(newI3vPath("dir/foo/bar/file2"))
	fs.AddTestI3vFile(newI3vPath("dir/foo/bar/file3"))
	// Delete "foo"
	if err := fs.DeleteDir(newI3vPath("/dir/foo")); err != nil {
		t.Fatal(err)
	}
	// Check that /dir still exists.
	if _, err := os.Stat(filepath.Join(root, "dir")); err != nil {
		t.Fatal(err)
	}
	// Check that /dir is empty.
	if fis, err := ioutil.ReadDir(filepath.Join(root, "dir")); err != nil {
		t.Fatal(err)
	} else if len(fis) != 1 {
		for i, fi := range fis {
			t.Logf("fi%v: %v", i, fi.Name())
		}
		t.Fatalf("expected 1 file in 'dir' but contains %v files", len(fis))
	}
}

// TestRenameFile tests if renaming a single file works as expected.
func TestRenameFile(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Add a file to the root dir.
	foo := newI3vPath("foo")
	foobar := newI3vPath("foobar")
	barfoo := newI3vPath("bar/foo")
	fs.AddTestI3vFile(foo)
	// Rename the file.
	if err := fs.RenameFile(foo, foobar); err != nil {
		t.Fatal(err)
	}
	// Check if the file was renamed.
	if _, err := fs.OpenI3vFile(foo); err != ErrNotExist {
		t.Fatal("expected ErrNotExist but got:", err)
	}
	sf, err := fs.OpenI3vFile(foobar)
	if err != nil {
		t.Fatal("expected ErrNotExist but got:", err)
	}
	sf.Close()
	// Rename the file again. This time it changes to a non-existent folder.
	if err := fs.RenameFile(foobar, barfoo); err != nil {
		t.Fatal(err)
	}
	sf, err = fs.OpenI3vFile(barfoo)
	if err != nil {
		t.Fatal("expected ErrNotExist but got:", err)
	}
	sf.Close()
}

// TestThreadedAccess tests rapidly opening and closing files and directories
// from multiple threads to check the locking conventions.
func TestThreadedAccess(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Specify the file structure for the test.
	filePaths := []string{
		"f0",
		"f1",
		"f2",

		"d0/f0", "d0/f1", "d0/f2",
		"d1/f0", "d1/f1", "d1/f2",
		"d2/f0", "d2/f1", "d2/f2",

		"d0/d0/f0", "d0/d0/f1", "d0/d0/f2",
		"d0/d1/f0", "d0/d1/f1", "d0/d1/f2",
		"d0/d2/f0", "d0/d2/f1", "d0/d2/f2",

		"d1/d0/f0", "d1/d0/f1", "d1/d0/f2",
		"d1/d1/f0", "d1/d1/f1", "d1/d1/f2",
		"d1/d2/f0", "d1/d2/f1", "d1/d2/f2",

		"d2/d0/f0", "d2/d0/f1", "d2/d0/f2",
		"d2/d1/f0", "d2/d1/f1", "d2/d1/f2",
		"d2/d2/f0", "d2/d2/f1", "d2/d2/f2",
	}
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	for _, fp := range filePaths {
		fs.AddTestI3vFile(newI3vPath(fp))
	}
	// Create a few threads which open files
	var wg sync.WaitGroup
	numThreads := 5
	maxNumActions := uint64(50000)
	numActions := uint64(0)
	for i := 0; i < numThreads; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				if atomic.LoadUint64(&numActions) >= maxNumActions {
					break
				}
				atomic.AddUint64(&numActions, 1)
				sp := newI3vPath(filePaths[fastrand.Intn(len(filePaths))])
				sf, err := fs.OpenI3vFile(sp)
				if err != nil {
					t.Fatal(err)
				}
				sf.Close()
			}
		}()
	}
	// Create a few threads which open dirs
	for i := 0; i < numThreads; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				if atomic.LoadUint64(&numActions) >= maxNumActions {
					break
				}
				atomic.AddUint64(&numActions, 1)
				sp := newI3vPath(filePaths[fastrand.Intn(len(filePaths))])
				sp, err := sp.Dir()
				if err != nil {
					t.Fatal(err)
				}
				sd, err := fs.OpenI3vDir(sp)
				if err != nil {
					t.Fatal(err)
				}
				sd.Close()
			}
		}()
	}
	wg.Wait()

	// Check the root's integrity. Since all files and dirs were closed, the
	// node's maps should reflect that.
	if len(fs.threads) != 0 {
		t.Fatalf("fs should have 0 threads but had %v", len(fs.threads))
	}
	if len(fs.directories) != 0 {
		t.Fatalf("fs should have 0 directories but had %v", len(fs.directories))
	}
	if len(fs.files) != 0 {
		t.Fatalf("fs should have 0 files but had %v", len(fs.files))
	}
}

// TestI3vDirRename tests the Rename method of the i3vdirset.
func TestI3vDirRename(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	// Prepare a filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	os.RemoveAll(root)
	fs := newTestFileSystem(root)

	// Specify a directory structure for this test.
	var dirStructure = []string{
		"dir1",
		"dir1/subdir1",
		"dir1/subdir1/subsubdir1",
		"dir1/subdir1/subsubdir2",
		"dir1/subdir1/subsubdir3",
		"dir1/subdir2",
		"dir1/subdir2/subsubdir1",
		"dir1/subdir2/subsubdir2",
		"dir1/subdir2/subsubdir3",
		"dir1/subdir3",
		"dir1/subdir3/subsubdir1",
		"dir1/subdir3/subsubdir2",
		"dir1/subdir3/subsubdir3",
	}
	// Specify a function that's executed in parallel which continuously saves dirs
	// to disk.
	stop := make(chan struct{})
	wg := new(sync.WaitGroup)
	f := func(entry *DirNode) {
		defer wg.Done()
		defer entry.Close()
		for {
			select {
			case <-stop:
				return
			default:
			}
			err := entry.UpdateMetadata(i3vdir.Metadata{})
			if err != nil {
				t.Error(err)
				return
			}
			time.Sleep(50 * time.Millisecond)
		}
	}
	// Create the structure and spawn a goroutine that keeps saving the structure
	// to disk for each directory.
	for _, dir := range dirStructure {
		sp, err := modules.NewI3vPath(dir)
		if err != nil {
			t.Fatal(err)
		}
		err = fs.NewI3vDir(sp, modules.DefaultDirPerm)
		if err != nil {
			t.Fatal(err)
		}
		entry, err := fs.OpenI3vDir(sp)
		if err != nil {
			t.Fatal(err)
		}
		// 50% chance to spawn goroutine. It's not realistic to assume that all dirs
		// are loaded.
		if fastrand.Intn(2) == 0 {
			wg.Add(1)
			go f(entry)
		} else {
			entry.Close()
		}
	}
	// Wait a second for the goroutines to write to disk a few times.
	time.Sleep(time.Second)
	// Rename dir1 to dir2.
	oldPath, err1 := modules.NewI3vPath(dirStructure[0])
	newPath, err2 := modules.NewI3vPath("dir2")
	if err := errors.Compose(err1, err2); err != nil {
		t.Fatal(err)
	}
	if err := fs.RenameDir(oldPath, newPath); err != nil {
		t.Fatal(err)
	}
	// Wait another second for more writes to disk after renaming the dir before
	// killing the goroutines.
	time.Sleep(time.Second)
	close(stop)
	wg.Wait()
	time.Sleep(time.Second)
	// Make sure we can't open any of the old folders on disk but we can open the
	// new ones.
	for _, dir := range dirStructure {
		oldDir, err1 := modules.NewI3vPath(dir)
		newDir, err2 := oldDir.Rebase(oldPath, newPath)
		if err := errors.Compose(err1, err2); err != nil {
			t.Fatal(err)
		}
		// Open entry with old dir. Shouldn't work.
		_, err := fs.OpenI3vDir(oldDir)
		if err != ErrNotExist {
			t.Fatal("shouldn't be able to open old path", oldDir.String(), err)
		}
		// Open entry with new dir. Should succeed.
		entry, err := fs.OpenI3vDir(newDir)
		if err != nil {
			t.Fatal(err)
		}
		defer entry.Close()
		// Check path of entry.
		if expectedPath := fs.DirPath(newDir); *entry.path != expectedPath {
			t.Fatalf("entry should have path '%v' but was '%v'", expectedPath, entry.path)
		}
	}
}

// TestAddI3vFileFromReader tests the AddI3vFileFromReader method's behavior.
func TestAddI3vFileFromReader(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()
	// Create a fileset with file.
	sf, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	// Add the existing file to the set again this shouldn't do anything.
	sr, err := sf.SnapshotReader()
	if err != nil {
		t.Fatal(err)
	}
	d, err := ioutil.ReadAll(sr)
	sr.Close()
	if err != nil {
		t.Fatal(err)
	}
	if err := sfs.AddI3vFileFromReader(bytes.NewReader(d), sfs.FileI3vPath(sf)); err != nil {
		t.Fatal(err)
	}
	numI3vFiles := 0
	err = sfs.Walk(modules.RootI3vPath(), func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == modules.I3vFileExtension {
			numI3vFiles++
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// There should be 1 i3vfile.
	if numI3vFiles != 1 {
		t.Fatalf("Found %v i3vfiles but expected %v", numI3vFiles, 1)
	}
	// Load the same i3vfile again, but change the UID.
	b, err := ioutil.ReadFile(sf.I3vFilePath())
	if err != nil {
		t.Fatal(err)
	}
	reader := bytes.NewReader(b)
	newSF, newChunks, err := i3vfile.LoadI3vFileFromReaderWithChunks(reader, sf.I3vFilePath(), sfs.staticWal)
	if err != nil {
		t.Fatal(err)
	}
	// Save the file to a temporary location with the new uid.
	newSF.UpdateUniqueID()
	newSF.SetI3vFilePath(sf.I3vFilePath() + "_tmp")
	if err := newSF.SaveWithChunks(newChunks); err != nil {
		t.Fatal(err)
	}
	// Grab the pre-import UID after changing it.
	preImportUID := newSF.UID()
	// Import the file. This should work because the files no longer share the same
	// UID.
	b, err = ioutil.ReadFile(newSF.I3vFilePath())
	if err != nil {
		t.Fatal(err)
	}
	// Remove file at temporary location after reading it.
	if err := os.Remove(newSF.I3vFilePath()); err != nil {
		t.Fatal(err)
	}
	reader = bytes.NewReader(b)
	var newSFI3vPath modules.I3vPath
	if err := newSFI3vPath.FromSysPath(sf.I3vFilePath(), sfs.Root()); err != nil {
		t.Fatal(err)
	}
	if err := sfs.AddI3vFileFromReader(reader, newSFI3vPath); err != nil {
		t.Fatal(err)
	}
	// Reload newSF with the new expected path.
	newSFPath := filepath.Join(filepath.Dir(sf.I3vFilePath()), newSFI3vPath.String()+"_1"+modules.I3vFileExtension)
	newSF, err = i3vfile.LoadI3vFile(newSFPath, sfs.staticWal)
	if err != nil {
		t.Fatal(err)
	}
	// sf and newSF should have the same pieces.
	for chunkIndex := uint64(0); chunkIndex < sf.NumChunks(); chunkIndex++ {
		piecesOld, err1 := sf.Pieces(chunkIndex)
		piecesNew, err2 := newSF.Pieces(chunkIndex)
		if err := errors.Compose(err1, err2); err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(piecesOld, piecesNew) {
			t.Log("piecesOld: ", piecesOld)
			t.Log("piecesNew: ", piecesNew)
			t.Fatal("old pieces don't match new pieces")
		}
	}
	numI3vFiles = 0
	err = sfs.Walk(modules.RootI3vPath(), func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == modules.I3vFileExtension {
			numI3vFiles++
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// There should be 2 i3vfiles.
	if numI3vFiles != 2 {
		t.Fatalf("Found %v i3vfiles but expected %v", numI3vFiles, 2)
	}
	// The UID should have changed.
	if newSF.UID() == preImportUID {
		t.Fatal("newSF UID should have changed after importing the file")
	}
	if !strings.HasSuffix(newSF.I3vFilePath(), "_1"+modules.I3vFileExtension) {
		t.Fatal("I3vFile should have a suffix but didn't")
	}
	// Should be able to open the new file from disk.
	if _, err := os.Stat(newSF.I3vFilePath()); err != nil {
		t.Fatal(err)
	}
}

// TestI3vFileSetDeleteOpen checks that deleting an entry from the set followed
// by creating a I3vfile with the same name without closing the deleted entry
// works as expected.
func TestI3vFileSetDeleteOpen(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create filesystem.
	sfs := newTestFileSystem(testDir(t.Name()))
	i3vPath := modules.RandomI3vPath()
	rc, _ := i3vfile.NewRSSubCode(10, 20, crypto.SegmentSize)
	fileSize := uint64(100)
	source := ""
	sk := crypto.GenerateI3vKey(crypto.TypeDefaultRenter)
	fileMode := os.FileMode(persist.DefaultDiskPermissionsTest)

	// Repeatedly create a I3vFile and delete it while still keeping the entry
	// around. That should only be possible without errors if the correctly
	// delete the entry from the set.
	var entries []*FileNode
	for i := 0; i < 10; i++ {
		// Create I3vFile
		up := modules.FileUploadParams{
			Source:              source,
			I3vPath:             i3vPath,
			ErasureCode:         rc,
			DisablePartialChunk: true,
		}
		err := sfs.NewI3vFile(up.I3vPath, up.Source, up.ErasureCode, sk, fileSize, fileMode, up.DisablePartialChunk)
		if err != nil {
			t.Fatal(err)
		}
		entry, err := sfs.OpenI3vFile(up.I3vPath)
		if err != nil {
			t.Fatal(err)
		}
		// Delete I3vFile
		if err := sfs.DeleteFile(sfs.FileI3vPath(entry)); err != nil {
			t.Fatal(err)
		}
		// The map should be empty.
		if len(sfs.files) != 0 {
			t.Fatal("I3vFileMap should have 1 file")
		}
		// Append the entry to make sure we can close it later.
		entries = append(entries, entry)
	}
	// The I3vFile shouldn't exist anymore.
	_, err := sfs.OpenI3vFile(i3vPath)
	if err != ErrNotExist {
		t.Fatal("I3vFile shouldn't exist anymore")
	}
	// Close the entries.
	for _, entry := range entries {
		entry.Close()
	}
}

// TestI3vFileSetOpenClose tests that the threadCount of the i3vfile is
// incremented and decremented properly when Open() and Close() are called
func TestI3vFileSetOpenClose(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create I3vFileSet with I3vFile
	entry, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	i3vPath := sfs.FileI3vPath(entry)
	exists, _ := sfs.FileExists(i3vPath)
	if !exists {
		t.Fatal("No I3vFileSetEntry found")
	}
	if err != nil {
		t.Fatal(err)
	}

	// Confirm 1 file is in memory
	if len(sfs.files) != 1 {
		t.Fatalf("Expected I3vFileSet map to be of length 1, instead is length %v", len(sfs.files))
	}

	// Confirm threadCount is incremented properly
	if len(entry.threads) != 1 {
		t.Fatalf("Expected threadMap to be of length 1, got %v", len(entry.threads))
	}

	// Close I3vFileSetEntry
	entry.Close()

	// Confirm that threadCount was decremented
	if len(entry.threads) != 0 {
		t.Fatalf("Expected threadCount to be 0, got %v", len(entry.threads))
	}

	// Confirm file and partialsI3vFile were removed from memory
	if len(sfs.files) != 0 {
		t.Fatalf("Expected I3vFileSet map to contain 0 files, instead is length %v", len(sfs.files))
	}

	// Open i3vfile again and confirm threadCount was incremented
	entry, err = sfs.OpenI3vFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	if len(entry.threads) != 1 {
		t.Fatalf("Expected threadCount to be 1, got %v", len(entry.threads))
	}
}

// TestFilesInMemory confirms that files are added and removed from memory
// as expected when files are in use and not in use
func TestFilesInMemory(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create I3vFileSet with I3vFile
	entry, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	i3vPath := sfs.FileI3vPath(entry)
	exists, _ := sfs.FileExists(i3vPath)
	if !exists {
		t.Fatal("No I3vFileSetEntry found")
	}
	if err != nil {
		t.Fatal(err)
	}
	// Confirm there is 1 file in memory.
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 files in memory, got:", len(sfs.files))
	}
	// Close File
	entry.Close()
	// Confirm there are no files in memory
	if len(sfs.files) != 0 {
		t.Fatal("Expected 0 files in memory, got:", len(sfs.files))
	}

	// Test accessing the same file from two separate threads
	//
	// Open file
	entry1, err := sfs.OpenI3vFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	// Confirm there is 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	// Access the file again
	entry2, err := sfs.OpenI3vFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	// Confirm there is still only has 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	// Close one of the file instances
	entry1.Close()
	// Confirm there is still only has 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}

	// Confirm closing out remaining files removes all files from memory
	//
	// Close last instance of the first file
	entry2.Close()
	// Confirm there is no file in memory
	if len(sfs.files) != 0 {
		t.Fatal("Expected 0 files in memory, got:", len(sfs.files))
	}
}

// TestRenameFileInMemory confirms that threads that have access to a file
// will continue to have access to the file even it another thread renames it
func TestRenameFileInMemory(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create I3vFileSet with I3vFile and corresponding combined i3vfile.
	entry, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	i3vPath := sfs.FileI3vPath(entry)
	exists, _ := sfs.FileExists(i3vPath)
	if !exists {
		t.Fatal("No I3vFileSetEntry found")
	}
	if err != nil {
		t.Fatal(err)
	}

	// Confirm there are 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}

	// Test renaming an instance of a file
	//
	// Access file with another instance
	entry2, err := sfs.OpenI3vFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	// Confirm that renter still only has 2 files in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	_, err = os.Stat(entry.I3vFilePath())
	if err != nil {
		println("err2", err.Error())
	}
	// Rename second instance
	newI3vPath := modules.RandomI3vPath()
	err = sfs.RenameFile(i3vPath, newI3vPath)
	if err != nil {
		t.Fatal(err)
	}
	// Confirm there are still only 1 file in memory as renaming doesn't add
	// the new name to memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	// Close instance of renamed file
	entry2.Close()
	// Confirm there are still only 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	// Close other instance of second file
	entry.Close()
	// Confirm there is no file in memory
	if len(sfs.files) != 0 {
		t.Fatal("Expected 0 files in memory, got:", len(sfs.files))
	}
}

// TestDeleteFileInMemory confirms that threads that have access to a file
// will continue to have access to the file even it another thread deletes it
func TestDeleteFileInMemory(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create I3vFileSet with I3vFile
	entry, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	i3vPath := sfs.FileI3vPath(entry)
	exists, _ := sfs.FileExists(i3vPath)
	if !exists {
		t.Fatal("No I3vFileSetEntry found")
	}
	if err != nil {
		t.Fatal(err)
	}

	// Confirm there is 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}

	// Test deleting an instance of a file
	//
	// Access the file again
	entry2, err := sfs.OpenI3vFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}
	// Confirm there is still only has 1 file in memory
	if len(sfs.files) != 1 {
		t.Fatal("Expected 1 file in memory, got:", len(sfs.files))
	}
	// delete and close instance of file
	if err := sfs.DeleteFile(i3vPath); err != nil {
		t.Fatal(err)
	}
	entry2.Close()
	// There should be no file in the set after deleting it.
	if len(sfs.files) != 0 {
		t.Fatal("Expected 0 files in memory, got:", len(sfs.files))
	}
	// confirm other instance is still in memory by calling methods on it
	if !entry.Deleted() {
		t.Fatal("Expected file to be deleted")
	}

	// Confirm closing out remaining files removes all files from memory
	//
	// Close last instance of the first file
	entry.Close()
	// Confirm renter has one file in memory
	if len(sfs.files) != 0 {
		t.Fatal("Expected 0 file in memory, got:", len(sfs.files))
	}
}

// TestDeleteCorruptI3vFile confirms that the i3vfileset will delete a i3vfile
// even if it cannot be opened
func TestDeleteCorruptI3vFile(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create i3vfileset
	_, sfs, err := newTestFileSystemWithFile(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Create i3vfile on disk with random bytes
	i3vPath, err := modules.NewI3vPath("badFile")
	if err != nil {
		t.Fatal(err)
	}
	i3vFilePath := i3vPath.I3vFileSysPath(sfs.Root())
	err = ioutil.WriteFile(i3vFilePath, fastrand.Bytes(100), 0666)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the i3vfile cannot be opened
	_, err = sfs.OpenI3vFile(i3vPath)
	if err == nil || err == ErrNotExist {
		t.Fatal("expected open to fail for read error but instead got:", err)
	}

	// Delete the i3vfile
	err = sfs.DeleteFile(i3vPath)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the file is no longer on disk
	_, err = os.Stat(i3vFilePath)
	if !os.IsNotExist(err) {
		t.Fatal("Expected err to be that file does not exists but was:", err)
	}
}

// TestI3vDirDelete tests the DeleteDir method of the i3vfileset.
func TestI3vDirDelete(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	// Prepare a i3vdirset
	root := filepath.Join(testDir(t.Name()), "fs-root")
	os.RemoveAll(root)
	fs := newTestFileSystem(root)

	// Specify a directory structure for this test.
	var dirStructure = []string{
		"dir1",
		"dir1/subdir1",
		"dir1/subdir1/subsubdir1",
		"dir1/subdir1/subsubdir2",
		"dir1/subdir1/subsubdir3",
		"dir1/subdir2",
		"dir1/subdir2/subsubdir1",
		"dir1/subdir2/subsubdir2",
		"dir1/subdir2/subsubdir3",
		"dir1/subdir3",
		"dir1/subdir3/subsubdir1",
		"dir1/subdir3/subsubdir2",
		"dir1/subdir3/subsubdir3",
	}
	// Specify a function that's executed in parallel which continuously saves a
	// file to disk.
	stop := make(chan struct{})
	wg := new(sync.WaitGroup)
	f := func(entry *FileNode) {
		defer wg.Done()
		defer entry.Close()
		for {
			select {
			case <-stop:
				return
			default:
			}
			err := entry.SaveHeader()
			if err != nil && !strings.Contains(err.Error(), "can't call createAndApplyTransaction on deleted file") {
				t.Fatal(err)
			}
			time.Sleep(50 * time.Millisecond)
		}
	}
	// Create the structure and spawn a goroutine that keeps saving the structure
	// to disk for each directory.
	for _, dir := range dirStructure {
		sp, err := modules.NewI3vPath(dir)
		if err != nil {
			t.Fatal(err)
		}
		err = fs.NewI3vDir(sp, modules.DefaultDirPerm)
		if err != nil {
			t.Fatal(err)
		}
		entry, err := fs.OpenI3vDir(sp)
		if err != nil {
			t.Fatal(err)
		}
		// 50% chance to close the dir.
		if fastrand.Intn(2) == 0 {
			entry.Close()
		}
		// Create a file in the dir.
		fileSP, err := sp.Join(hex.EncodeToString(fastrand.Bytes(16)))
		if err != nil {
			t.Fatal(err)
		}
		ec, _ := i3vfile.NewRSSubCode(10, 20, crypto.SegmentSize)
		up := modules.FileUploadParams{Source: "", I3vPath: fileSP, ErasureCode: ec}
		err = fs.NewI3vFile(up.I3vPath, up.Source, up.ErasureCode, crypto.GenerateI3vKey(crypto.TypeDefaultRenter), 100, persist.DefaultDiskPermissionsTest, up.DisablePartialChunk)
		if err != nil {
			t.Fatal(err)
		}
		sf, err := fs.OpenI3vFile(up.I3vPath)
		if err != nil {
			t.Fatal(err)
		}
		// 50% chance to spawn goroutine. It's not realistic to assume that all dirs
		// are loaded.
		if fastrand.Intn(2) == 0 {
			wg.Add(1)
			go f(sf)
		} else {
			sf.Close()
		}
	}
	// Wait a second for the goroutines to write to disk a few times.
	time.Sleep(time.Second)
	// Delete dir1.
	sp, err := modules.NewI3vPath("dir1")
	if err != nil {
		t.Fatal(err)
	}
	if err := fs.DeleteDir(sp); err != nil {
		t.Fatal(err)
	}

	// Wait another second for more writes to disk after renaming the dir before
	// killing the goroutines.
	time.Sleep(time.Second)
	close(stop)
	wg.Wait()
	time.Sleep(time.Second)
	// The root i3vfile dir should be empty except for 1 .i3vdir file.
	files, err := fs.ReadDir(modules.RootI3vPath())
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 1 {
		for _, file := range files {
			t.Log("Found ", file.Name())
		}
		t.Fatalf("There should be %v files/folders in the root dir but found %v\n", 1, len(files))
	}
	for _, file := range files {
		if filepath.Ext(file.Name()) != modules.I3vDirExtension &&
			filepath.Ext(file.Name()) != modules.PartialsI3vFileExtension {
			t.Fatal("Encountered unexpected file:", file.Name())
		}
	}
}

// TestI3vDirRenameWithFiles tests the RenameDir method of the filesystem with
// files.
func TestI3vDirRenameWithFiles(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	// Prepare a filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	os.RemoveAll(root)
	fs := newTestFileSystem(root)

	// Prepare parameters for i3vfiles.
	rc, _ := i3vfile.NewRSSubCode(10, 20, crypto.SegmentSize)
	fileSize := uint64(100)
	source := ""
	sk := crypto.GenerateI3vKey(crypto.TypeDefaultRenter)
	fileMode := os.FileMode(persist.DefaultDiskPermissionsTest)

	// Specify a directory structure for this test.
	var dirStructure = []string{
		"dir1",
		"dir1/subdir1",
		"dir1/subdir1/subsubdir1",
		"dir1/subdir1/subsubdir2",
		"dir1/subdir1/subsubdir3",
		"dir1/subdir2",
		"dir1/subdir2/subsubdir1",
		"dir1/subdir2/subsubdir2",
		"dir1/subdir2/subsubdir3",
		"dir1/subdir3",
		"dir1/subdir3/subsubdir1",
		"dir1/subdir3/subsubdir2",
		"dir1/subdir3/subsubdir3",
	}
	// Specify a function that's executed in parallel which continuously saves a
	// file to disk.
	stop := make(chan struct{})
	wg := new(sync.WaitGroup)
	f := func(entry *FileNode) {
		defer wg.Done()
		defer entry.Close()
		for {
			select {
			case <-stop:
				return
			default:
			}
			err := entry.SaveHeader()
			if err != nil {
				t.Fatal(err)
			}
			time.Sleep(50 * time.Millisecond)
		}
	}
	// Create the structure and spawn a goroutine that keeps saving the structure
	// to disk for each directory.
	for _, dir := range dirStructure {
		sp, err := modules.NewI3vPath(dir)
		if err != nil {
			t.Fatal(err)
		}
		err = fs.NewI3vDir(sp, modules.DefaultDirPerm)
		if err != nil {
			t.Fatal(err)
		}
		entry, err := fs.OpenI3vDir(sp)
		// 50% chance to close the dir.
		if fastrand.Intn(2) == 0 {
			entry.Close()
		}
		// Create a file in the dir.
		fileSP, err := sp.Join(hex.EncodeToString(fastrand.Bytes(16)))
		if err != nil {
			t.Fatal(err)
		}
		err = fs.NewI3vFile(fileSP, source, rc, sk, fileSize, fileMode, true)
		if err != nil {
			t.Fatal(err)
		}
		sf, err := fs.OpenI3vFile(fileSP)
		if err != nil {
			t.Fatal(err)
		}
		// 50% chance to spawn goroutine. It's not realistic to assume that all dirs
		// are loaded.
		if fastrand.Intn(2) == 0 {
			wg.Add(1)
			go f(sf)
		} else {
			sf.Close()
		}
	}
	// Wait a second for the goroutines to write to disk a few times.
	time.Sleep(time.Second)
	// Rename dir1 to dir2.
	oldPath, err1 := modules.NewI3vPath(dirStructure[0])
	newPath, err2 := modules.NewI3vPath("dir2")
	if err := errors.Compose(err1, err2); err != nil {
		t.Fatal(err)
	}
	if err := fs.RenameDir(oldPath, newPath); err != nil {
		t.Fatal(err)
	}
	// Wait another second for more writes to disk after renaming the dir before
	// killing the goroutines.
	time.Sleep(time.Second)
	close(stop)
	wg.Wait()
	time.Sleep(time.Second)
	// Make sure we can't open any of the old folders/files on disk but we can open
	// the new ones.
	for _, dir := range dirStructure {
		oldDir, err1 := modules.NewI3vPath(dir)
		newDir, err2 := oldDir.Rebase(oldPath, newPath)
		if err := errors.Compose(err1, err2); err != nil {
			t.Fatal(err)
		}
		// Open entry with old dir. Shouldn't work.
		_, err := fs.OpenI3vDir(oldDir)
		if err != ErrNotExist {
			t.Fatal("shouldn't be able to open old path", oldDir.String(), err)
		}
		// Old dir shouldn't exist.
		if _, err = fs.Stat(oldDir); !os.IsNotExist(err) {
			t.Fatal(err)
		}
		// Open entry with new dir. Should succeed.
		entry, err := fs.OpenI3vDir(newDir)
		if err != nil {
			t.Fatal(err)
		}
		defer entry.Close()
		// New dir should contain 1 i3vfile.
		fis, err := fs.ReadDir(newDir)
		if err != nil {
			t.Fatal(err)
		}
		numFiles := 0
		for _, fi := range fis {
			if !fi.IsDir() && filepath.Ext(fi.Name()) == modules.I3vFileExtension {
				numFiles++
			}
		}
		if numFiles != 1 {
			t.Fatalf("there should be 1 file in the new dir not %v", numFiles)
		}
		// Check i3vpath of entry.
		if entry.managedAbsPath() != fs.DirPath(newDir) {
			t.Fatalf("entry should have path '%v' but was '%v'", fs.DirPath(newDir), entry.managedAbsPath())
		}
	}
}

// TestLazyI3vDir tests that i3vDir correctly reads and sets the lazyI3vDir
// field.
func TestLazyI3vDir(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create dir /foo
	sp := newI3vPath("foo")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// Open the newly created dir.
	foo, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer foo.Close()
	// Get the i3vdir.
	sd, err := foo.i3vDir()
	if err != nil {
		t.Fatal(err)
	}
	// Lazydir should be set.
	if *foo.lazyI3vDir != sd {
		t.Fatal(err)
	}
	// Fetching foo from root should also have lazydir set.
	fooRoot := fs.directories["foo"]
	if *fooRoot.lazyI3vDir != sd {
		t.Fatal("fooRoot doesn't have lazydir set")
	}
	// Open foo again.
	foo2, err := fs.OpenI3vDir(sp)
	if err != nil {
		t.Fatal(err)
	}
	defer foo2.Close()
	// Lazydir should already be loaded.
	if *foo2.lazyI3vDir != sd {
		t.Fatal("foo2.lazyI3vDir isn't set correctly", foo2.lazyI3vDir)
	}
}

// TestLazyI3vDir tests that i3vDir correctly reads and sets the lazyI3vDir
// field.
func TestOpenCloseRoot(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)

	rootNode, err := fs.OpenI3vDir(modules.RootI3vPath())
	if err != nil {
		t.Fatal(err)
	}
	rootNode.Close()
}

// TestFailedOpenFileFolder makes sure that a failed call to OpenI3vFile or
// Openi3vDir doesn't leave any nodes dangling in memory.
func TestFailedOpenFileFolder(t *testing.T) {
	if testing.Short() && !build.VLONG {
		t.SkipNow()
	}
	t.Parallel()
	// Create filesystem.
	root := filepath.Join(testDir(t.Name()), "fs-root")
	fs := newTestFileSystem(root)
	// Create dir /sub1/sub2
	sp := newI3vPath("sub1/sub2")
	if err := fs.NewI3vDir(sp, modules.DefaultDirPerm); err != nil {
		t.Fatal(err)
	}
	// Prepare a path to "foo"
	foo, err := sp.Join("foo")
	if err != nil {
		t.Fatal(err)
	}
	// Open "foo" as a dir.
	_, err = fs.OpenI3vDir(foo)
	if err != ErrNotExist {
		t.Fatal("err should be ErrNotExist but was", err)
	}
	if len(fs.files) != 0 || len(fs.directories) != 0 {
		t.Fatal("Expected 0 files and folders but got", len(fs.files), len(fs.directories))
	}
	// Open "foo" as a file.
	_, err = fs.OpenI3vDir(foo)
	if err != ErrNotExist {
		t.Fatal("err should be ErrNotExist but was", err)
	}
	if len(fs.files) != 0 || len(fs.directories) != 0 {
		t.Fatal("Expected 0 files and folders but got", len(fs.files), len(fs.directories))
	}
}
