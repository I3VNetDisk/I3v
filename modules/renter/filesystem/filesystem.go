package filesystem

import (
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vdir"
	"gitlab.com/I3VNetDisk/I3v/modules/renter/i3vfile"
	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/writeaheadlog"
)

var (
	// ErrNotExist is returned when a file or folder can't be found on disk.
	ErrNotExist = errors.New("path does not exist")

	// ErrExists is returned when a file or folder already exists at a given
	// location.
	ErrExists = errors.New("a file or folder already exists at the specified path")

	// ErrDeleteFileIsDir is returned when the file delete method is used but
	// the filename corresponds to a directory
	ErrDeleteFileIsDir = errors.New("cannot delete file, file is a directory")
)

type (
	// FileSystem implements a thread-safe filesystem for I3v for loading
	// I3vFiles, I3vDirs and potentially other supported I3v types in the
	// future.
	FileSystem struct {
		DirNode
	}

	// node is a struct that contains the commmon fields of every node.
	node struct {
		// fields that all copies of a node share.
		path      *string
		parent    *DirNode
		name      *string
		staticWal *writeaheadlog.WAL
		threads   map[threadUID]struct{} // tracks all the threadUIDs of evey copy of the node
		staticLog *persist.Logger
		staticUID uint64
		mu        *sync.Mutex

		// fields that differ between copies of the same node.
		threadUID threadUID // unique ID of a copy of a node
	}
	threadUID uint64
)

// newNode is a convenience function to initialize a node.
func newNode(parent *DirNode, path, name string, uid threadUID, wal *writeaheadlog.WAL, log *persist.Logger) node {
	return node{
		path:      &path,
		parent:    parent,
		name:      &name,
		staticLog: log,
		staticUID: newInode(),
		staticWal: wal,
		threads:   make(map[threadUID]struct{}),
		threadUID: uid,
		mu:        new(sync.Mutex),
	}
}

// managedLockWithParent is a helper method which correctly acquires the lock of
// a node and it's parent. If no parent it available it will return 'nil'. In
// either case the node and potential parent will be locked after the call.
func (n *node) managedLockWithParent() *DirNode {
	var parent *DirNode
	for {
		// If a parent exists, we need to lock it while closing a child.
		n.mu.Lock()
		parent = n.parent
		n.mu.Unlock()
		if parent != nil {
			parent.mu.Lock()
		}
		n.mu.Lock()
		if n.parent != parent {
			n.mu.Unlock()
			parent.mu.Unlock()
			continue // try again
		}
		break
	}
	return parent
}

// NID returns the node's unique identifier.
func (n *node) Inode() uint64 {
	return n.staticUID
}

// newThreadUID returns a random threadUID to be used as the threadUID in the
// threads map of the node.
func newThreadUID() threadUID {
	return threadUID(fastrand.Uint64n(math.MaxUint64))
}

// newInode will create a unique identifier for a filesystem node.
//
// TODO: replace this with a function that doesn't repeat itself.
func newInode() uint64 {
	return fastrand.Uint64n(math.MaxUint64)
}

// nodeI3vPath returns the I3vPath of a node relative to a root path.
func nodeI3vPath(rootPath string, n *node) (sp modules.I3vPath) {
	if err := sp.FromSysPath(n.managedAbsPath(), rootPath); err != nil {
		build.Critical("FileSystem.managedI3vPath: should never fail", err)
	}
	return sp
}

// closeNode removes a thread from the node's threads map. This should only be
// called from within other 'close' methods.
func (n *node) closeNode() {
	if _, exists := n.threads[n.threadUID]; !exists {
		build.Critical("threaduid doesn't exist in threads map: ", n.threadUID, len(n.threads))
	}
	delete(n.threads, n.threadUID)
}

// absPath returns the absolute path of the node.
func (n *node) absPath() string {
	return *n.path
}

// managedAbsPath returns the absolute path of the node.
func (n *node) managedAbsPath() string {
	n.mu.Lock()
	defer n.mu.Unlock()
	return n.absPath()
}

// New creates a new FileSystem at the specified root path. The folder will be
// created if it doesn't exist already.
func New(root string, log *persist.Logger, wal *writeaheadlog.WAL) (*FileSystem, error) {
	fs := &FileSystem{
		DirNode: DirNode{
			// The root doesn't require a parent, a name or uid.
			node:        newNode(nil, root, "", 0, wal, log),
			directories: make(map[string]*DirNode),
			files:       make(map[string]*FileNode),
			lazyI3vDir:  new(*i3vdir.I3vDir),
		},
	}
	// Prepare root folder.
	err := fs.NewI3vDir(modules.RootI3vPath(), modules.DefaultDirPerm)
	if err != nil && !errors.Contains(err, ErrExists) {
		return nil, err
	}
	return fs, nil
}

// AddI3vFileFromReader adds an existing I3vFile to the set and stores it on
// disk. If the exact same file already exists, this is a no-op. If a file
// already exists with a different UID, the UID will be updated and a unique
// path will be chosen. If no file exists, the UID will be updated but the path
// remains the same.
func (fs *FileSystem) AddI3vFileFromReader(rs io.ReadSeeker, i3vPath modules.I3vPath) error {
	// Load the file.
	path := fs.FilePath(i3vPath)
	sf, chunks, err := i3vfile.LoadI3vFileFromReaderWithChunks(rs, path, fs.staticWal)
	if err != nil {
		return err
	}
	// Create dir with same Mode as file if it doesn't exist already and open
	// it.
	dirI3vPath, err := i3vPath.Dir()
	if err != nil {
		return err
	}
	if err := fs.managedNewI3vDir(dirI3vPath, sf.Mode()); err != nil {
		return err
	}
	dir, err := fs.managedOpenDir(dirI3vPath.String())
	if err != nil {
		return err
	}
	defer dir.Close()
	// Add the file to the dir.
	return dir.managedNewI3vFileFromExisting(sf, chunks)
}

// CachedFileInfo returns the cached File Information of the i3vfile
func (fs *FileSystem) CachedFileInfo(i3vPath modules.I3vPath) (modules.FileInfo, error) {
	return fs.managedFileInfo(i3vPath, true, nil, nil, nil)
}

// CachedList lists the files and directories within a I3vDir.
func (fs *FileSystem) CachedList(i3vPath modules.I3vPath, recursive bool) ([]modules.FileInfo, []modules.DirectoryInfo, error) {
	return fs.managedList(i3vPath, recursive, true, nil, nil, nil)
}

// CachedListOnNode will return the files and directories within a given i3vdir
// node.
func (fs *FileSystem) CachedListOnNode(d *DirNode, recursive bool) ([]modules.FileInfo, []modules.DirectoryInfo, error) {
	return d.managedList(fs.managedAbsPath(), recursive, true, nil, nil, nil)
}

// DeleteDir deletes a dir from the filesystem. The dir will be marked as
// 'deleted' which should cause all remaining instances of the dir to be close
// shortly. Only when all instances of the dir are closed it will be removed
// from the tree. This means that as long as the deletion is in progress, no new
// file of the same path can be created and the existing file can't be opened
// until all instances of it are closed.
func (fs *FileSystem) DeleteDir(i3vPath modules.I3vPath) error {
	return fs.managedDeleteDir(i3vPath.String())
}

// DeleteFile deletes a file from the filesystem. The file will be marked as
// 'deleted' which should cause all remaining instances of the file to be closed
// shortly. Only when all instances of the file are closed it will be removed
// from the tree. This means that as long as the deletion is in progress, no new
// file of the same path can be created and the existing file can't be opened
// until all instances of it are closed.
func (fs *FileSystem) DeleteFile(i3vPath modules.I3vPath) error {
	return fs.managedDeleteFile(i3vPath.String())
}

// DirInfo returns the Directory Information of the i3vdir
func (fs *FileSystem) DirInfo(i3vPath modules.I3vPath) (modules.DirectoryInfo, error) {
	dir, err := fs.managedOpenDir(i3vPath.String())
	if err != nil {
		return modules.DirectoryInfo{}, nil
	}
	defer dir.Close()
	di, err := dir.managedInfo(i3vPath)
	if err != nil {
		return modules.DirectoryInfo{}, err
	}
	di.I3vPath = i3vPath
	return di, nil
}

// DirNodeInfo will return the DirectoryInfo of a i3vdir given the node. This is
// more efficient than calling fs.DirInfo.
func (fs *FileSystem) DirNodeInfo(n *DirNode) (modules.DirectoryInfo, error) {
	sp := fs.DirI3vPath(n)
	return n.managedInfo(sp)
}

// FileInfo returns the File Information of the i3vfile
func (fs *FileSystem) FileInfo(i3vPath modules.I3vPath, offline map[string]bool, goodForRenew map[string]bool, contracts map[string]modules.RenterContract) (modules.FileInfo, error) {
	return fs.managedFileInfo(i3vPath, false, offline, goodForRenew, contracts)
}

// FileNodeInfo returns the FileInfo of a i3vfile given the node for the
// i3vfile. This is faster than calling fs.FileInfo.
func (fs *FileSystem) FileNodeInfo(n *FileNode) (modules.FileInfo, error) {
	sp := fs.FileI3vPath(n)
	return n.staticCachedInfo(sp)
}

// List lists the files and directories within a I3vDir.
func (fs *FileSystem) List(i3vPath modules.I3vPath, recursive bool, offlineMap, goodForRenewMap map[string]bool, contractsMap map[string]modules.RenterContract) ([]modules.FileInfo, []modules.DirectoryInfo, error) {
	return fs.managedList(i3vPath, recursive, false, offlineMap, goodForRenewMap, contractsMap)
}

// FileExists checks to see if a file with the provided i3vPath already exists
// in the renter.
func (fs *FileSystem) FileExists(i3vPath modules.I3vPath) (bool, error) {
	path := fs.FilePath(i3vPath)
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// FilePath converts a I3vPath into a file's system path.
func (fs *FileSystem) FilePath(i3vPath modules.I3vPath) string {
	return i3vPath.I3vFileSysPath(fs.managedAbsPath())
}

// NewI3vDir creates the folder for the specified i3vPath.
func (fs *FileSystem) NewI3vDir(i3vPath modules.I3vPath, mode os.FileMode) error {
	return fs.managedNewI3vDir(i3vPath, mode)
}

// NewI3vFile creates a I3vFile at the specified i3vPath.
func (fs *FileSystem) NewI3vFile(i3vPath modules.I3vPath, source string, ec modules.ErasureCoder, mk crypto.CipherKey, fileSize uint64, fileMode os.FileMode, disablePartialUpload bool) error {
	// Create I3vDir for file.
	dirI3vPath, err := i3vPath.Dir()
	if err != nil {
		return err
	}
	if err := fs.NewI3vDir(dirI3vPath, fileMode); err != nil {
		return errors.AddContext(err, fmt.Sprintf("failed to create I3vDir %v for I3vFile %v", dirI3vPath.String(), i3vPath.String()))
	}
	return fs.managedNewI3vFile(i3vPath.String(), source, ec, mk, fileSize, fileMode, disablePartialUpload)
}

// ReadDir is a wrapper of ioutil.ReadDir which takes a I3vPath as an argument
// instead of a system path.
func (fs *FileSystem) ReadDir(i3vPath modules.I3vPath) ([]os.FileInfo, error) {
	dirPath := i3vPath.I3vDirSysPath(fs.managedAbsPath())
	return ioutil.ReadDir(dirPath)
}

// DirPath converts a I3vPath into a dir's system path.
func (fs *FileSystem) DirPath(i3vPath modules.I3vPath) string {
	return i3vPath.I3vDirSysPath(fs.managedAbsPath())
}

// Root returns the root system path of the FileSystem.
func (fs *FileSystem) Root() string {
	return fs.DirPath(modules.RootI3vPath())
}

// FileI3vPath returns the I3vPath of a file node.
func (fs *FileSystem) FileI3vPath(n *FileNode) (sp modules.I3vPath) {
	return fs.managedI3vPath(&n.node)
}

// DirI3vPath returns the I3vPath of a dir node.
func (fs *FileSystem) DirI3vPath(n *DirNode) (sp modules.I3vPath) {
	return fs.managedI3vPath(&n.node)
}

// UpdateDirMetadata updates the metadata of a I3vDir.
func (fs *FileSystem) UpdateDirMetadata(i3vPath modules.I3vPath, metadata i3vdir.Metadata) error {
	dir, err := fs.OpenI3vDir(i3vPath)
	if err != nil {
		return err
	}
	defer dir.Close()
	return dir.UpdateMetadata(metadata)
}

// managedI3vPath returns the I3vPath of a node.
func (fs *FileSystem) managedI3vPath(n *node) modules.I3vPath {
	return nodeI3vPath(fs.managedAbsPath(), n)
}

// Stat is a wrapper for os.Stat which takes a I3vPath as an argument instead of
// a system path.
func (fs *FileSystem) Stat(i3vPath modules.I3vPath) (os.FileInfo, error) {
	path := i3vPath.I3vDirSysPath(fs.managedAbsPath())
	return os.Stat(path)
}

// Walk is a wrapper for filepath.Walk which takes a I3vPath as an argument
// instead of a system path.
func (fs *FileSystem) Walk(i3vPath modules.I3vPath, walkFn filepath.WalkFunc) error {
	dirPath := i3vPath.I3vDirSysPath(fs.managedAbsPath())
	return filepath.Walk(dirPath, walkFn)
}

// WriteFile is a wrapper for ioutil.WriteFile which takes a I3vPath as an
// argument instead of a system path.
func (fs *FileSystem) WriteFile(i3vPath modules.I3vPath, data []byte, perm os.FileMode) error {
	path := i3vPath.I3vFileSysPath(fs.managedAbsPath())
	return ioutil.WriteFile(path, data, perm)
}

// NewI3vFileFromLegacyData creates a new I3vFile from data that was previously loaded
// from a legacy file.
func (fs *FileSystem) NewI3vFileFromLegacyData(fd i3vfile.FileData) (*FileNode, error) {
	// Get file's I3vPath.
	sp, err := modules.UserI3vPath().Join(fd.Name)
	if err != nil {
		return nil, err
	}
	// Get i3vpath of dir.
	dirI3vPath, err := sp.Dir()
	if err != nil {
		return nil, err
	}
	// Create the dir if it doesn't exist.
	if err := fs.NewI3vDir(dirI3vPath, 0755); err != nil {
		return nil, err
	}
	// Open dir.
	dir, err := fs.managedOpenDir(dirI3vPath.String())
	if err != nil {
		return nil, err
	}
	defer dir.Close()
	// Add the file to the dir.
	return dir.managedNewI3vFileFromLegacyData(sp.Name(), fd)
}

// OpenI3vDir opens a I3vDir and adds it and all of its parents to the
// filesystem tree.
func (fs *FileSystem) OpenI3vDir(i3vPath modules.I3vPath) (*DirNode, error) {
	return fs.managedOpenI3vDir(i3vPath)
}

// OpenI3vFile opens a I3vFile and adds it and all of its parents to the
// filesystem tree.
func (fs *FileSystem) OpenI3vFile(i3vPath modules.I3vPath) (*FileNode, error) {
	sf, err := fs.managedOpenFile(i3vPath.String())
	if err != nil {
		return nil, err
	}
	return sf, nil
}

// RenameFile renames the file with oldI3vPath to newI3vPath.
func (fs *FileSystem) RenameFile(oldI3vPath, newI3vPath modules.I3vPath) error {
	// Open I3vDir for file at old location.
	oldDirI3vPath, err := oldI3vPath.Dir()
	if err != nil {
		return err
	}
	oldDir, err := fs.managedOpenI3vDir(oldDirI3vPath)
	if err != nil {
		return err
	}
	defer oldDir.Close()
	// Open the file.
	sf, err := oldDir.managedOpenFile(oldI3vPath.Name())
	if err == ErrNotExist {
		return ErrNotExist
	}
	if err != nil {
		return errors.AddContext(err, "failed to open file for renaming")
	}
	defer sf.Close()

	// Create and Open I3vDir for file at new location.
	newDirI3vPath, err := newI3vPath.Dir()
	if err != nil {
		return err
	}
	if err := fs.NewI3vDir(newDirI3vPath, sf.managedMode()); err != nil {
		return errors.AddContext(err, fmt.Sprintf("failed to create I3vDir %v for I3vFile %v", newDirI3vPath.String(), oldI3vPath.String()))
	}
	newDir, err := fs.managedOpenI3vDir(newDirI3vPath)
	if err != nil {
		return err
	}
	defer newDir.Close()
	// Rename the file.
	return sf.managedRename(newI3vPath.Name(), oldDir, newDir)
}

// RenameDir takes an existing directory and changes the path. The original
// directory must exist, and there must not be any directory that already has
// the replacement path.  All i3v files within directory will also be renamed
func (fs *FileSystem) RenameDir(oldI3vPath, newI3vPath modules.I3vPath) error {
	// Open I3vDir for parent dir at old location.
	oldDirI3vPath, err := oldI3vPath.Dir()
	if err != nil {
		return err
	}
	oldDir, err := fs.managedOpenI3vDir(oldDirI3vPath)
	if err != nil {
		return err
	}
	defer func() {
		oldDir.Close()
	}()
	// Open the dir to rename.
	sd, err := oldDir.managedOpenDir(oldI3vPath.Name())
	if err == ErrNotExist {
		return ErrNotExist
	}
	if err != nil {
		return errors.AddContext(err, "failed to open file for renaming")
	}
	defer func() {
		sd.Close()
	}()

	// Create and Open parent I3vDir for dir at new location.
	newDirI3vPath, err := newI3vPath.Dir()
	if err != nil {
		return err
	}
	md, err := sd.Metadata()
	if err != nil {
		return err
	}
	if err := fs.NewI3vDir(newDirI3vPath, md.Mode); err != nil {
		return errors.AddContext(err, fmt.Sprintf("failed to create I3vDir %v for I3vFile %v", newDirI3vPath.String(), oldI3vPath.String()))
	}
	newDir, err := fs.managedOpenI3vDir(newDirI3vPath)
	if err != nil {
		return err
	}
	defer func() {
		newDir.Close()
	}()
	// Rename the dir.
	err = sd.managedRename(newI3vPath.Name(), oldDir, newDir)
	return err
}

// managedDeleteFile opens the parent folder of the file to delete and calls
// managedDeleteFile on it.
func (fs *FileSystem) managedDeleteFile(relPath string) error {
	// Open the folder that contains the file.
	dirPath, fileName := filepath.Split(relPath)
	var dir *DirNode
	if dirPath == string(filepath.Separator) || dirPath == "." || dirPath == "" {
		dir = &fs.DirNode // file is in the root dir
	} else {
		var err error
		dir, err = fs.managedOpenDir(filepath.Dir(relPath))
		if err != nil {
			return errors.AddContext(err, "failed to open parent dir of file")
		}
		// Close the dir since we are not returning it. The open file keeps it
		// loaded in memory.
		defer dir.Close()
	}
	return dir.managedDeleteFile(fileName)
}

// managedDeleteDir opens the parent folder of the dir to delete and calls
// managedDelete on it.
func (fs *FileSystem) managedDeleteDir(path string) error {
	// Open the dir.
	dir, err := fs.managedOpenDir(path)
	if err != nil {
		return errors.AddContext(err, "failed to open parent dir of file")
	}
	// Close the dir since we are not returning it. The open file keeps it
	// loaded in memory.
	defer dir.Close()
	return dir.managedDelete()
}

// managedFileInfo returns the FileInfo of the i3vfile.
func (fs *FileSystem) managedFileInfo(i3vPath modules.I3vPath, cached bool, offline map[string]bool, goodForRenew map[string]bool, contracts map[string]modules.RenterContract) (modules.FileInfo, error) {
	// Open the file.
	file, err := fs.managedOpenFile(i3vPath.String())
	if err != nil {
		return modules.FileInfo{}, err
	}
	defer file.Close()
	if cached {
		return file.staticCachedInfo(i3vPath)
	}
	return file.managedFileInfo(i3vPath, offline, goodForRenew, contracts)
}

// managedList returns the files and dirs within the I3vDir specified by i3vPath.
// offlineMap, goodForRenewMap and contractMap don't need to be provided if
// 'cached' is set to 'true'.
func (fs *FileSystem) managedList(i3vPath modules.I3vPath, recursive, cached bool, offlineMap map[string]bool, goodForRenewMap map[string]bool, contractsMap map[string]modules.RenterContract) (fis []modules.FileInfo, dis []modules.DirectoryInfo, _ error) {
	// Open the folder.
	dir, err := fs.managedOpenDir(i3vPath.String())
	if err != nil {
		return nil, nil, errors.AddContext(err, "failed to open folder specified by FileList")
	}
	defer dir.Close()
	return dir.managedList(fs.managedAbsPath(), recursive, cached, offlineMap, goodForRenewMap, contractsMap)
}

// managedNewI3vDir creates the folder at the specified i3vPath.
func (fs *FileSystem) managedNewI3vDir(i3vPath modules.I3vPath, mode os.FileMode) error {
	// If i3vPath is the root dir we just create the metadata for it.
	if i3vPath.IsRoot() {
		fs.mu.Lock()
		defer fs.mu.Unlock()
		dirPath := i3vPath.I3vDirSysPath(fs.absPath())
		_, err := i3vdir.New(dirPath, fs.absPath(), mode, fs.staticWal)
		if os.IsExist(err) {
			return nil // nothing to do
		}
		return err
	}
	// If i3vPath isn't the root dir we need to grab the parent.
	parentPath, err := i3vPath.Dir()
	if err != nil {
		return err
	}
	parent, err := fs.managedOpenDir(parentPath.String())
	if err == ErrNotExist {
		// If the parent doesn't exist yet we create it.
		err = fs.managedNewI3vDir(parentPath, mode)
		if err == nil {
			parent, err = fs.managedOpenDir(parentPath.String())
		}
	}
	if err != nil {
		return err
	}
	defer parent.Close()
	// Create the dir within the parent.
	return parent.managedNewI3vDir(i3vPath.Name(), fs.managedAbsPath(), mode)
}

// managedOpenFile opens a I3vFile and adds it and all of its parents to the
// filesystem tree.
func (fs *FileSystem) managedOpenFile(relPath string) (*FileNode, error) {
	// Open the folder that contains the file.
	dirPath, fileName := filepath.Split(relPath)
	var dir *DirNode
	if dirPath == string(filepath.Separator) || dirPath == "." || dirPath == "" {
		dir = &fs.DirNode // file is in the root dir
	} else {
		var err error
		dir, err = fs.managedOpenDir(filepath.Dir(relPath))
		if err != nil {
			return nil, errors.AddContext(err, "failed to open parent dir of file")
		}
		// Close the dir since we are not returning it. The open file keeps it
		// loaded in memory.
		defer dir.Close()
	}
	return dir.managedOpenFile(fileName)
}

// managedNewI3vFile opens the parent folder of the new I3vFile and calls
// managedNewI3vFile on it.
func (fs *FileSystem) managedNewI3vFile(relPath string, source string, ec modules.ErasureCoder, mk crypto.CipherKey, fileSize uint64, fileMode os.FileMode, disablePartialUpload bool) error {
	// Open the folder that contains the file.
	dirPath, fileName := filepath.Split(relPath)
	var dir *DirNode
	if dirPath == string(filepath.Separator) || dirPath == "." || dirPath == "" {
		dir = &fs.DirNode // file is in the root dir
	} else {
		var err error
		dir, err = fs.managedOpenDir(filepath.Dir(relPath))
		if err != nil {
			return errors.AddContext(err, "failed to open parent dir of new file")
		}
		defer dir.Close()
	}
	return dir.managedNewI3vFile(fileName, source, ec, mk, fileSize, fileMode, disablePartialUpload)
}

// managedOpenI3vDir opens a I3vDir and adds it and all of its parents to the
// filesystem tree.
func (fs *FileSystem) managedOpenI3vDir(i3vPath modules.I3vPath) (*DirNode, error) {
	if i3vPath.IsRoot() {
		return fs.DirNode.managedCopy(), nil
	}
	dir, err := fs.DirNode.managedOpenDir(i3vPath.String())
	if err != nil {
		return nil, err
	}
	return dir, nil
}
