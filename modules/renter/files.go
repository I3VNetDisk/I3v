package renter

import (
	"gitlab.com/I3VNetDisk/I3v/modules"

	"gitlab.com/I3VNetDisk/errors"
)

// DeleteFile removes a file entry from the renter and deletes its data from
// the hosts it is stored on.
func (r *Renter) DeleteFile(i3vPath modules.I3vPath) error {
	err := r.tg.Add()
	if err != nil {
		return err
	}
	defer r.tg.Done()

	// Perform the delete operation.
	err = r.staticFileSystem.DeleteFile(i3vPath)
	if err != nil {
		return errors.AddContext(err, "unable to delete i3vfile from filesystem")
	}

	// Update the filesystem metadata.
	//
	// TODO: This is incorrect, should be running the metadata update call on a
	// node, not on a i3vPath. The node should be returned by the delete call.
	// Need a metadata update func that operates on a node to do that.
	dirI3vPath, err := i3vPath.Dir()
	if err != nil {
		r.log.Printf("Unable to fetch the directory from a i3vPath %v for deleted i3vfile: %v", i3vPath, err)
		// Return 'nil' because the delete operation succeeded, it was only the
		// metadata update operation that failed.
		return nil
	}
	go r.callThreadedBubbleMetadata(dirI3vPath)
	return nil
}

// FileList returns all of the files that the renter has.
func (r *Renter) FileList(i3vPath modules.I3vPath, recursive, cached bool) ([]modules.FileInfo, error) {
	if err := r.tg.Add(); err != nil {
		return []modules.FileInfo{}, err
	}
	defer r.tg.Done()
	var fis []modules.FileInfo
	var err error
	if cached {
		fis, _, err = r.staticFileSystem.CachedList(i3vPath, recursive)
	} else {
		offlineMap, goodForRenewMap, contractsMap := r.managedContractUtilityMaps()
		fis, _, err = r.staticFileSystem.List(i3vPath, recursive, offlineMap, goodForRenewMap, contractsMap)
	}
	if err != nil {
		return nil, err
	}
	return fis, err
}

// File returns file from i3vPath queried by user.
// Update based on FileList
func (r *Renter) File(i3vPath modules.I3vPath) (modules.FileInfo, error) {
	if err := r.tg.Add(); err != nil {
		return modules.FileInfo{}, err
	}
	defer r.tg.Done()
	offline, goodForRenew, contracts := r.managedContractUtilityMaps()
	fi, err := r.staticFileSystem.FileInfo(i3vPath, offline, goodForRenew, contracts)
	if err != nil {
		return modules.FileInfo{}, errors.AddContext(err, "unable to get the fileinfo from the filesystem")
	}
	return fi, nil
}

// FileCached returns file from i3vPath queried by user, using cached values for
// health and redundancy.
func (r *Renter) FileCached(i3vPath modules.I3vPath) (modules.FileInfo, error) {
	if err := r.tg.Add(); err != nil {
		return modules.FileInfo{}, err
	}
	defer r.tg.Done()
	return r.staticFileSystem.CachedFileInfo(i3vPath)
}

// RenameFile takes an existing file and changes the nickname. The original
// file must exist, and there must not be any file that already has the
// replacement nickname.
func (r *Renter) RenameFile(currentName, newName modules.I3vPath) error {
	if err := r.tg.Add(); err != nil {
		return err
	}
	defer r.tg.Done()

	// Rename file.
	err := r.staticFileSystem.RenameFile(currentName, newName)
	if err != nil {
		return err
	}

	// Call callThreadedBubbleMetadata on the old directory to make sure the
	// system metadata is updated to reflect the move.
	oldDirI3vPath, err := currentName.Dir()
	if err != nil {
		return err
	}
	go r.callThreadedBubbleMetadata(oldDirI3vPath)
	// Call callThreadedBubbleMetadata on the new directory to make sure the
	// system metadata is updated to reflect the move.
	newDirI3vPath, err := newName.Dir()
	if err != nil {
		return err
	}
	go r.callThreadedBubbleMetadata(newDirI3vPath)

	return nil
}

// SetFileStuck sets the Stuck field of the whole i3vfile to stuck.
func (r *Renter) SetFileStuck(i3vPath modules.I3vPath, stuck bool) error {
	if err := r.tg.Add(); err != nil {
		return err
	}
	defer r.tg.Done()
	// Open the file.
	entry, err := r.staticFileSystem.OpenI3vFile(i3vPath)
	if err != nil {
		return err
	}
	defer entry.Close()
	// Update the file.
	return entry.SetAllStuck(stuck)
}
