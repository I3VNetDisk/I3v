package modules

import (
	"encoding/base32"
	"encoding/json"
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/fastrand"
)

// i3vpath.go contains the types and methods for creating and manipulating
// i3vpaths. Any methods such as filepath.Join should be implemented here for
// the I3vPath type to ensure consistent handling across OS.

var (
	// ErrEmptyI3vPath is an error when I3vPath is empty
	ErrEmptyI3vPath = errors.New("I3vPath must be a nonempty string")

	// I3vDirExtension is the extension for i3vdir metadata files on disk
	I3vDirExtension = ".i3vdir"

	// I3vFileExtension is the extension for i3vfiles on disk
	I3vFileExtension = ".i3v"

	// PartialsI3vFileExtension is the extension for i3vfiles which contain
	// combined chunks.
	PartialsI3vFileExtension = ".ci3v"

	// CombinedChunkExtension is the extension for a combined chunk on disk.
	CombinedChunkExtension = ".cc"
	// UnfinishedChunkExtension is the extension for an unfinished combined chunk
	// and is appended to the file in addition to CombinedChunkExtension.
	UnfinishedChunkExtension = ".unfinished"
	// ChunkMetadataExtension is the extension of a metadata file for a combined
	// chunk.
	ChunkMetadataExtension = ".ccmd"
)

var (
	// SkynetFolder is the I3v folder where all of the skyfiles are stored by
	// default.
	SkynetFolder = NewGlobalI3vPath("/var/skynet")
)

type (
	// I3vPath is the struct used to uniquely identify i3vfiles and i3vdirs across
	// I3v
	I3vPath struct {
		Path string `json:"path"`
	}
)

// NewI3vPath returns a new I3vPath with the path set
func NewI3vPath(s string) (I3vPath, error) {
	return newI3vPath(s)
}

// NewGlobalI3vPath can be used to create a global var which is a I3vPath. If
// there is an error creating the I3vPath, the function will panic, making this
// function unsuitable for typical use.
func NewGlobalI3vPath(s string) I3vPath {
	sp, err := NewI3vPath(s)
	if err != nil {
		panic("error creating global i3vpath: " + err.Error())
	}
	return sp
}

// RandomI3vPath returns a random I3vPath created from 20 bytes of base32
// encoded entropy.
func RandomI3vPath() (sp I3vPath) {
	sp.Path = base32.StdEncoding.EncodeToString(fastrand.Bytes(20))
	sp.Path = sp.Path[:20]
	return
}

// RootI3vPath returns a I3vPath for the root i3vdir which has a blank path
func RootI3vPath() I3vPath {
	return I3vPath{}
}

// HomeI3vPath returns a i3vpath to /home
func HomeI3vPath() I3vPath {
	sp, err := RootI3vPath().Join(HomeFolderRoot)
	if err != nil {
		build.Critical(err)
	}
	return sp
}

// UserI3vPath returns a i3vpath to /home/user
func UserI3vPath() I3vPath {
	sp := HomeI3vPath()
	sp, err := sp.Join(UserRoot)
	if err != nil {
		build.Critical(err)
	}
	return sp
}

// SnapshotsI3vPath returns a i3vpath to /snapshots
func SnapshotsI3vPath() I3vPath {
	sp, err := RootI3vPath().Join(BackupRoot)
	if err != nil {
		build.Critical(err)
	}
	return sp
}

// CombinedI3vFilePath returns the I3vPath to a hidden i3vfile which is used to
// store chunks that contain pieces of multiple i3vfiles.
func CombinedI3vFilePath(ec ErasureCoder) I3vPath {
	return I3vPath{Path: fmt.Sprintf(".%v", ec.Identifier())}
}

// clean cleans up the string by converting an OS separators to forward slashes
// and trims leading and trailing slashes
func clean(s string) string {
	s = filepath.ToSlash(s)
	s = strings.TrimPrefix(s, "/")
	s = strings.TrimSuffix(s, "/")
	return s
}

// newI3vPath returns a new I3vPath with the path set
func newI3vPath(s string) (I3vPath, error) {
	sp := I3vPath{
		Path: clean(s),
	}
	return sp, sp.Validate(false)
}

// AddSuffix adds a numeric suffix to the end of the I3vPath.
func (sp I3vPath) AddSuffix(suffix uint) I3vPath {
	return I3vPath{
		Path: sp.Path + fmt.Sprintf("_%v", suffix),
	}
}

// Dir returns the directory of the I3vPath
func (sp I3vPath) Dir() (I3vPath, error) {
	pathElements := strings.Split(sp.Path, "/")
	// If there is only one path element, then the I3vpath was just a filename
	// and did not have a directory, return the root I3vpath
	if len(pathElements) <= 1 {
		return RootI3vPath(), nil
	}
	dir := strings.Join(pathElements[:len(pathElements)-1], "/")
	// If dir is empty or a dot, return the root I3vpath
	if dir == "" || dir == "." {
		return RootI3vPath(), nil
	}
	return newI3vPath(dir)
}

// Equals compares two I3vPath types for equality
func (sp I3vPath) Equals(i3vPath I3vPath) bool {
	return sp.Path == i3vPath.Path
}

// IsEmpty returns true if the i3vpath is equal to the nil value
func (sp I3vPath) IsEmpty() bool {
	return sp.Equals(I3vPath{})
}

// IsRoot indicates whether or not the I3vPath path is a root directory i3vpath
func (sp I3vPath) IsRoot() bool {
	return sp.Path == ""
}

// Join joins the string to the end of the I3vPath with a "/" and returns the
// new I3vPath.
func (sp I3vPath) Join(s string) (I3vPath, error) {
	if s == "" {
		return I3vPath{}, errors.New("cannot join an empty string to a i3vpath")
	}
	return newI3vPath(sp.Path + "/" + clean(s))
}

// LoadString sets the path of the I3vPath to the provided string
func (sp *I3vPath) LoadString(s string) error {
	sp.Path = clean(s)
	return sp.Validate(false)
}

// LoadSysPath loads a I3vPath from a given system path by trimming the dir at
// the front of the path, the extension at the back and returning the remaining
// path as a I3vPath.
func (sp *I3vPath) LoadSysPath(dir, path string) error {
	if !strings.HasPrefix(path, dir) {
		return fmt.Errorf("%v is not a prefix of %v", dir, path)
	}
	path = strings.TrimSuffix(strings.TrimPrefix(path, dir), I3vFileExtension)
	return sp.LoadString(path)
}

// MarshalJSON marshales a I3vPath as a string.
func (sp I3vPath) MarshalJSON() ([]byte, error) {
	return json.Marshal(sp.String())
}

// Name returns the name of the file.
func (sp I3vPath) Name() string {
	pathElements := strings.Split(sp.Path, "/")
	name := pathElements[len(pathElements)-1]
	// If name is a dot, return the root I3vpath name
	if name == "." {
		name = ""
	}
	return name
}

// Rebase changes the base of a i3vpath from oldBase to newBase and returns a new I3vPath.
// e.g. rebasing 'a/b/myfile' from oldBase 'a/b/' to 'a/' would result in 'a/myfile'
func (sp I3vPath) Rebase(oldBase, newBase I3vPath) (I3vPath, error) {
	if !strings.HasPrefix(sp.Path, oldBase.Path) {
		return I3vPath{}, fmt.Errorf("'%v' isn't the base of '%v'", oldBase.Path, sp.Path)
	}
	relPath := strings.TrimPrefix(sp.Path, oldBase.Path)
	if relPath == "" {
		return newBase, nil
	}
	return newBase.Join(relPath)
}

// UnmarshalJSON unmarshals a i3vpath into a I3vPath object.
func (sp *I3vPath) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, &sp.Path); err != nil {
		return err
	}
	sp.Path = clean(sp.Path)
	return sp.Validate(true)
}

// I3vDirSysPath returns the system path needed to read a directory on disk, the
// input dir is the root i3vdir directory on disk
func (sp I3vPath) I3vDirSysPath(dir string) string {
	return filepath.Join(dir, filepath.FromSlash(sp.Path), "")
}

// I3vDirMetadataSysPath returns the system path needed to read the I3vDir
// metadata file from disk, the input dir is the root i3vdir directory on disk
func (sp I3vPath) I3vDirMetadataSysPath(dir string) string {
	return filepath.Join(dir, filepath.FromSlash(sp.Path), I3vDirExtension)
}

// I3vFileSysPath returns the system path needed to read the I3vFile from disk,
// the input dir is the root i3vfile directory on disk
func (sp I3vPath) I3vFileSysPath(dir string) string {
	return filepath.Join(dir, filepath.FromSlash(sp.Path)+I3vFileExtension)
}

// I3vPartialsFileSysPath returns the system path needed to read the
// PartialsI3vFile from disk, the input dir is the root i3vfile directory on
// disk
func (sp I3vPath) I3vPartialsFileSysPath(dir string) string {
	return filepath.Join(dir, filepath.FromSlash(sp.Path)+PartialsI3vFileExtension)
}

// String returns the I3vPath's path
func (sp I3vPath) String() string {
	return sp.Path
}

// FromSysPath creates a I3vPath from a i3vFilePath and corresponding root files
// dir.
func (sp *I3vPath) FromSysPath(i3vFilePath, dir string) (err error) {
	if !strings.HasPrefix(i3vFilePath, dir) {
		return fmt.Errorf("I3vFilePath %v is not within dir %v", i3vFilePath, dir)
	}
	relPath := strings.TrimPrefix(i3vFilePath, dir)
	relPath = strings.TrimSuffix(relPath, I3vFileExtension)
	relPath = strings.TrimSuffix(relPath, PartialsI3vFileExtension)
	*sp, err = newI3vPath(relPath)
	return
}

// Validate checks that a I3vpath is a legal filename. ../ is disallowed to
// prevent directory traversal, and paths must not begin with / or be empty.
func (sp I3vPath) Validate(isRoot bool) error {
	if sp.Path == "" && !isRoot {
		return ErrEmptyI3vPath
	}
	if sp.Path == ".." {
		return errors.New("i3vpath cannot be '..'")
	}
	if sp.Path == "." {
		return errors.New("i3vpath cannot be '.'")
	}
	// check prefix
	if strings.HasPrefix(sp.Path, "/") {
		return errors.New("i3vpath cannot begin with /")
	}
	if strings.HasPrefix(sp.Path, "../") {
		return errors.New("i3vpath cannot begin with ../")
	}
	if strings.HasPrefix(sp.Path, "./") {
		return errors.New("i3vpath connot begin with ./")
	}
	var prevElem string
	for _, pathElem := range strings.Split(sp.Path, "/") {
		if pathElem == "." || pathElem == ".." {
			return errors.New("i3vpath cannot contain . or .. elements")
		}
		if prevElem != "" && pathElem == "" {
			return ErrEmptyI3vPath
		}
		if prevElem == "/" || pathElem == "/" {
			return errors.New("i3vpath cannot contain //")
		}
		prevElem = pathElem
	}
	return nil
}
