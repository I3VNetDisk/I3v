package modules

import (
	"errors"
	"os"
	"path/filepath"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/I3v/types"
	"gitlab.com/I3VNetDisk/i3vmux"
	"gitlab.com/I3VNetDisk/i3vmux/mux"
)

const (
	// logfile is the filename of the i3vmux log file
	logfile = "i3vmux.log"

	// I3vMuxDir is the name of the i3vmux dir
	I3vMuxDir = "i3vmux"
)

// NewI3vMux returns a new I3vMux object
func NewI3vMux(i3vMuxDir, i3vDir, address string) (*i3vmux.I3vMux, error) {
	// can't use relative path
	if !filepath.IsAbs(i3vMuxDir) || !filepath.IsAbs(i3vDir) {
		err := errors.New("paths need to be absolute")
		build.Critical(err)
		return nil, err
	}

	// ensure the persist directory exists
	err := os.MkdirAll(i3vMuxDir, 0700)
	if err != nil {
		return nil, err
	}

	// CompatV143 migrate existing mux in i3vDir root to i3vMuxDir.
	if err := compatV143MigrateI3vMux(i3vMuxDir, i3vDir); err != nil {
		return nil, err
	}

	// create a logger
	file, err := os.OpenFile(filepath.Join(i3vMuxDir, logfile), os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return nil, err
	}
	logger := persist.NewLogger(file)

	// create a i3vmux, if the host's persistence file is at v120 we want to
	// recycle the host's key pair to use in the i3vmux
	pubKey, privKey, compat := compatLoadKeysFromHost(i3vDir)
	if compat {
		return i3vmux.CompatV1421NewWithKeyPair(address, logger, i3vMuxDir, privKey, pubKey)
	}
	return i3vmux.New(address, logger, i3vMuxDir)
}

// compatLoadKeysFromHost will try and load the host's keypair from its
// persistence file. It tries all host metadata versions before v143. From that
// point on, the i3vmux was introduced and will already have a correct set of
// keys persisted in its persistence file. Only for hosts upgrading to v143 we
// want to recycle the host keys in the i3vmux.
func compatLoadKeysFromHost(persistDir string) (pubKey mux.ED25519PublicKey, privKey mux.ED25519SecretKey, compat bool) {
	persistPath := filepath.Join(persistDir, HostDir, HostSettingsFile)

	historicMetadata := []persist.Metadata{
		Hostv120PersistMetadata,
		Hostv112PersistMetadata,
	}

	// Try to load the host's key pair from its persistence file, we try all
	// metadata version up until v143
	hk := struct {
		PublicKey types.I3vPublicKey `json:"publickey"`
		SecretKey crypto.SecretKey   `json:"secretkey"`
	}{}
	for _, metadata := range historicMetadata {
		err := persist.LoadJSON(metadata, &hk, persistPath)
		if err == nil {
			copy(pubKey[:], hk.PublicKey.Key[:])
			copy(privKey[:], hk.SecretKey[:])
			compat = true
			return
		}
	}

	compat = false
	return
}

// compatV143MigrateI3vMux migrates the I3vMux from the root dir of the i3v data
// dir to the i3vmux subdir.
func compatV143MigrateI3vMux(i3vMuxDir, i3vDir string) error {
	oldPath := filepath.Join(i3vDir, "i3vmux.json")
	newPath := filepath.Join(i3vMuxDir, "i3vmux.json")
	oldPathTmp := filepath.Join(i3vDir, "i3vmux.json_temp")
	newPathTmp := filepath.Join(i3vMuxDir, "i3vmux.json_temp")
	oldPathLog := filepath.Join(i3vDir, logfile)
	newPathLog := filepath.Join(i3vMuxDir, logfile)
	_, errOld := os.Stat(oldPath)
	_, errNew := os.Stat(newPath)

	// Migrate if old file exists but no file at new location exists yet.
	migrated := false
	if errOld == nil && os.IsNotExist(errNew) {
		if err := os.Rename(oldPath, newPath); err != nil {
			return err
		}
		migrated = true
	}
	// If no migration is necessary we are done.
	if !migrated {
		return nil
	}
	// If we migrated the main files, also migrate the tmp files if available.
	if err := os.Rename(oldPathTmp, newPathTmp); err != nil && !os.IsNotExist(err) {
		return err
	}
	// Also migrate the log file.
	if err := os.Rename(oldPathLog, newPathLog); err != nil && !os.IsNotExist(err) {
		return err
	}
	return nil
}
