package modules

import (
	"bytes"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/I3v/types"
)

// TestI3vMuxCompat verifies the I3vMux is initialized in compatibility mode
// when the host's persitence metadata version is v1.4.2
func TestI3vMuxCompat(t *testing.T) {
	// ensure the host's persistence file does not exist
	i3vDataDir := filepath.Join(os.TempDir(), t.Name())
	i3vMuxDir := filepath.Join(i3vDataDir, I3vMuxDir)
	persistPath := filepath.Join(i3vDataDir, HostDir, HostSettingsFile)
	os.Remove(persistPath)

	// create a new i3vmux, seeing as there won't be a host persistence file, it
	// will act as if this is a fresh new node and create a new key pair
	mux, err := NewI3vMux(i3vMuxDir, i3vDataDir, "localhost:0")
	if err != nil {
		t.Fatal(err)
	}
	expectedPK := mux.PublicKey()
	expectedSK := mux.PrivateKey()
	mux.Close()

	// re-open the mux and verify it uses the same keys
	mux, err = NewI3vMux(i3vMuxDir, i3vDataDir, "localhost:0")
	if err != nil {
		t.Fatal(err)
	}

	actualPK := mux.PublicKey()
	actualSK := mux.PrivateKey()
	if !bytes.Equal(actualPK[:], expectedPK[:]) {
		t.Log(actualPK)
		t.Log(expectedPK)
		t.Fatal("I3vMux's public key was different after reloading the mux")
	}
	if !bytes.Equal(actualSK[:], expectedSK[:]) {
		t.Log(actualSK)
		t.Log(expectedSK)
		t.Fatal("I3vMux's private key was different after reloading the mux")
	}
	mux.Close()

	// prepare a host's persistence file with v1.4.2 and verify the mux is now
	// initialised using the host's key pair

	// create the host directory if it doesn't exist.
	err = os.MkdirAll(filepath.Join(i3vDataDir, HostDir), 0700)
	if err != nil {
		t.Fatal(err)
	}

	sk, pk := crypto.GenerateKeyPair()
	spk := types.I3vPublicKey{
		Algorithm: types.SignatureEd25519,
		Key:       pk[:],
	}
	persistence := struct {
		PublicKey types.I3vPublicKey `json:"publickey"`
		SecretKey crypto.SecretKey   `json:"secretkey"`
	}{
		PublicKey: spk,
		SecretKey: sk,
	}
	err = persist.SaveJSON(Hostv120PersistMetadata, persistence, persistPath)
	if err != nil {
		t.Fatal(err)
	}

	// create a new i3vmux
	mux, err = NewI3vMux(i3vMuxDir, i3vDataDir, "localhost:0")
	if err != nil {
		t.Fatal(err)
	}

	actualPK = mux.PublicKey()
	actualSK = mux.PrivateKey()
	if !bytes.Equal(actualPK[:], spk.Key) {
		t.Log(actualPK)
		t.Log(spk.Key)
		t.Fatal("I3vMux's public key was not equal to the host's pubkey")
	}
	if !bytes.Equal(actualSK[:], sk[:]) {
		t.Log(mux.PrivateKey())
		t.Log(spk.Key)
		t.Fatal("I3vMux's public key was not equal to the host's pubkey")
	}
}
