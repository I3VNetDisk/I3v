package consensus

import (
	"testing"

	"gitlab.com/I3VNetDisk/bolt"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/types"
)

// TestCommitDelayedI3vcoinOutputDiffBadMaturity commits a delayed i3vcoin
// output that has a bad maturity height and triggers a panic.
func TestCommitDelayedI3vcoinOutputDiffBadMaturity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting error after corrupting database")
		}
	}()

	// Commit a delayed i3vcoin output with maturity height = cs.height()+1
	maturityHeight := cst.cs.dbBlockHeight() - 1
	id := types.I3vcoinOutputID{'1'}
	dsco := types.I3vcoinOutput{Value: types.NewCurrency64(1)}
	dscod := modules.DelayedI3vcoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             id,
		I3vcoinOutput:  dsco,
		MaturityHeight: maturityHeight,
	}
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitDelayedI3vcoinOutputDiff(tx, dscod, modules.DiffApply)
		return nil
	})
}

// TestCommitNodeDiffs probes the commitNodeDiffs method of the consensus set.
/*
func TestCommitNodeDiffs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()
	pb := cst.cs.dbCurrentProcessedBlock()
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitDiffSet(tx, pb, modules.DiffRevert) // pull the block node out of the consensus set.
		return nil
	})

	// For diffs that can be destroyed in the same block they are created,
	// create diffs that do just that. This has in the past caused issues upon
	// rewinding.
	scoid := types.I3vcoinOutputID{'1'}
	scod0 := modules.I3vcoinOutputDiff{
		Direction: modules.DiffApply,
		ID:        scoid,
	}
	scod1 := modules.I3vcoinOutputDiff{
		Direction: modules.DiffRevert,
		ID:        scoid,
	}
	fcid := types.FileContractID{'2'}
	fcd0 := modules.FileContractDiff{
		Direction: modules.DiffApply,
		ID:        fcid,
	}
	fcd1 := modules.FileContractDiff{
		Direction: modules.DiffRevert,
		ID:        fcid,
	}
	sfoid := types.I3vfundOutputID{'3'}
	sfod0 := modules.I3vfundOutputDiff{
		Direction: modules.DiffApply,
		ID:        sfoid,
	}
	sfod1 := modules.I3vfundOutputDiff{
		Direction: modules.DiffRevert,
		ID:        sfoid,
	}
	dscoid := types.I3vcoinOutputID{'4'}
	dscod := modules.DelayedI3vcoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             dscoid,
		MaturityHeight: cst.cs.dbBlockHeight() + types.MaturityDelay,
	}
	var i3vfundPool types.Currency
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		i3vfundPool = getI3vfundPool(tx)
		return nil
	})
	if err != nil {
		panic(err)
	}
	sfpd := modules.I3vfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  i3vfundPool,
		Adjusted:  i3vfundPool.Add(types.NewCurrency64(1)),
	}
	pb.I3vcoinOutputDiffs = append(pb.I3vcoinOutputDiffs, scod0)
	pb.I3vcoinOutputDiffs = append(pb.I3vcoinOutputDiffs, scod1)
	pb.FileContractDiffs = append(pb.FileContractDiffs, fcd0)
	pb.FileContractDiffs = append(pb.FileContractDiffs, fcd1)
	pb.I3vfundOutputDiffs = append(pb.I3vfundOutputDiffs, sfod0)
	pb.I3vfundOutputDiffs = append(pb.I3vfundOutputDiffs, sfod1)
	pb.DelayedI3vcoinOutputDiffs = append(pb.DelayedI3vcoinOutputDiffs, dscod)
	pb.I3vfundPoolDiffs = append(pb.I3vfundPoolDiffs, sfpd)
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		createUpcomingDelayedOutputMaps(tx, pb, modules.DiffApply)
		return nil
	})
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitNodeDiffs(tx, pb, modules.DiffApply)
		return nil
	})
	exists := cst.cs.db.inI3vcoinOutputs(scoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inFileContracts(fcid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inI3vfundOutputs(sfoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	_ = cst.cs.db.Update(func(tx *bolt.Tx) error {
		commitNodeDiffs(tx, pb, modules.DiffRevert)
		return nil
	})
	exists = cst.cs.db.inI3vcoinOutputs(scoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inFileContracts(fcid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
	exists = cst.cs.db.inI3vfundOutputs(sfoid)
	if exists {
		t.Error("intradependent outputs not treated correctly")
	}
}
*/

/*
// TestI3vcoinOutputDiff applies and reverts a i3vcoin output diff, then
// triggers an inconsistency panic.
func TestCommitI3vcoinOutputDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.Close()

	// Commit a i3vcoin output diff.
	initialScosLen := cst.cs.db.lenI3vcoinOutputs()
	id := types.I3vcoinOutputID{'1'}
	sco := types.I3vcoinOutput{Value: types.NewCurrency64(1)}
	scod := modules.I3vcoinOutputDiff{
		Direction:     modules.DiffApply,
		ID:            id,
		I3vcoinOutput: sco,
	}
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffApply)
	if cst.cs.db.lenI3vcoinOutputs() != initialScosLen+1 {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	if cst.cs.db.getI3vcoinOutputs(id).Value.Cmp(sco.Value) != 0 {
		t.Error("wrong i3vcoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffRevert)
	if cst.cs.db.lenI3vcoinOutputs() != initialScosLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inI3vcoinOutputs(id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffApply)
	scod.Direction = modules.DiffRevert
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffApply)
	if cst.cs.db.lenI3vcoinOutputs() != initialScosLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inI3vcoinOutputs(id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffRevert)
	if cst.cs.db.lenI3vcoinOutputs() != initialScosLen+1 {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	if cst.cs.db.getI3vcoinOutputs(id).Value.Cmp(sco.Value) != 0 {
		t.Error("wrong i3vcoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitI3vcoinOutputDiff {
			t.Error("expecting errBadCommitI3vcoinOutputDiff, got", r)
		}
	}()
	// Try reverting a revert diff that was already reverted. (add an object
	// that already exists)
	cst.cs.commitI3vcoinOutputDiff(scod, modules.DiffRevert)
}
*/

/*
// TestCommitFileContracttDiff applies and reverts a file contract diff, then
// triggers an inconsistency panic.
func TestCommitFileContractDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a file contract diff.
	initialFcsLen := cst.cs.db.lenFileContracts()
	id := types.FileContractID{'1'}
	fc := types.FileContract{Payout: types.NewCurrency64(1)}
	fcd := modules.FileContractDiff{
		Direction:    modules.DiffApply,
		ID:           id,
		FileContract: fc,
	}
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	if cst.cs.db.lenFileContracts() != initialFcsLen+1 {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	if cst.cs.db.getFileContracts(id).Payout.Cmp(fc.Payout) != 0 {
		t.Error("wrong i3vcoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert)
	if cst.cs.db.lenFileContracts() != initialFcsLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inFileContracts(id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	fcd.Direction = modules.DiffRevert
	cst.cs.commitFileContractDiff(fcd, modules.DiffApply)
	if cst.cs.db.lenFileContracts() != initialFcsLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inFileContracts(id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert)
	if cst.cs.db.lenFileContracts() != initialFcsLen+1 {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	if cst.cs.db.getFileContracts(id).Payout.Cmp(fc.Payout) != 0 {
		t.Error("wrong i3vcoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitFileContractDiff {
			t.Error("expecting errBadCommitFileContractDiff, got", r)
		}
	}()
	// Try reverting an apply diff that was already reverted. (remove an object
	// that was already removed)
	fcd.Direction = modules.DiffApply                      // Object currently exists, but make the direction 'apply'.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert) // revert the application.
	cst.cs.commitFileContractDiff(fcd, modules.DiffRevert) // revert the application again, in error.
}
*/

// TestI3vfundOutputDiff applies and reverts a i3vfund output diff, then
// triggers an inconsistency panic.
/*
func TestCommitI3vfundOutputDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a i3vfund output diff.
	initialScosLen := cst.cs.db.lenI3vfundOutputs()
	id := types.I3vfundOutputID{'1'}
	sfo := types.I3vfundOutput{Value: types.NewCurrency64(1)}
	sfod := modules.I3vfundOutputDiff{
		Direction:     modules.DiffApply,
		ID:            id,
		I3vfundOutput: sfo,
	}
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffApply)
	if cst.cs.db.lenI3vfundOutputs() != initialScosLen+1 {
		t.Error("i3vfund output diff set did not increase in size")
	}
	sfo1 := cst.cs.db.getI3vfundOutputs(id)
	if sfo1.Value.Cmp(sfo.Value) != 0 {
		t.Error("wrong i3vfund output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffRevert)
	if cst.cs.db.lenI3vfundOutputs() != initialScosLen {
		t.Error("i3vfund output diff set did not increase in size")
	}
	exists := cst.cs.db.inI3vfundOutputs(id)
	if exists {
		t.Error("i3vfund output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffApply)
	sfod.Direction = modules.DiffRevert
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffApply)
	if cst.cs.db.lenI3vfundOutputs() != initialScosLen {
		t.Error("i3vfund output diff set did not increase in size")
	}
	exists = cst.cs.db.inI3vfundOutputs(id)
	if exists {
		t.Error("i3vfund output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffRevert)
	if cst.cs.db.lenI3vfundOutputs() != initialScosLen+1 {
		t.Error("i3vfund output diff set did not increase in size")
	}
	sfo2 := cst.cs.db.getI3vfundOutputs(id)
	if sfo2.Value.Cmp(sfo.Value) != 0 {
		t.Error("wrong i3vfund output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitI3vfundOutputDiff {
			t.Error("expecting errBadCommitI3vfundOutputDiff, got", r)
		}
	}()
	// Try applying a revert diff that was already applied. (remove an object
	// that was already removed)
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffApply) // Remove the object.
	cst.cs.commitI3vfundOutputDiff(sfod, modules.DiffApply) // Remove the object again.
}
*/

// TestCommitDelayedI3vcoinOutputDiff probes the commitDelayedI3vcoinOutputDiff
// method of the consensus set.
/*
func TestCommitDelayedI3vcoinOutputDiff(t *testing.T) {
	t.Skip("test isn't working, but checks the wrong code anyway")
	if testing.Short() {
		t.Skip()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Commit a delayed i3vcoin output with maturity height = cs.height()+1
	maturityHeight := cst.cs.height() + 1
	initialDscosLen := cst.cs.db.lenDelayedI3vcoinOutputsHeight(maturityHeight)
	id := types.I3vcoinOutputID{'1'}
	dsco := types.I3vcoinOutput{Value: types.NewCurrency64(1)}
	dscod := modules.DelayedI3vcoinOutputDiff{
		Direction:      modules.DiffApply,
		ID:             id,
		I3vcoinOutput:  dsco,
		MaturityHeight: maturityHeight,
	}
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffApply)
	if cst.cs.db.lenDelayedI3vcoinOutputsHeight(maturityHeight) != initialDscosLen+1 {
		t.Fatal("delayed output diff set did not increase in size")
	}
	if cst.cs.db.getDelayedI3vcoinOutputs(maturityHeight, id).Value.Cmp(dsco.Value) != 0 {
		t.Error("wrong delayed i3vcoin output value after committing a diff")
	}

	// Rewind the diff.
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffRevert)
	if cst.cs.db.lenDelayedI3vcoinOutputsHeight(maturityHeight) != initialDscosLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists := cst.cs.db.inDelayedI3vcoinOutputsHeight(maturityHeight, id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Restore the diff and then apply the inverse diff.
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffApply)
	dscod.Direction = modules.DiffRevert
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffApply)
	if cst.cs.db.lenDelayedI3vcoinOutputsHeight(maturityHeight) != initialDscosLen {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	exists = cst.cs.db.inDelayedI3vcoinOutputsHeight(maturityHeight, id)
	if exists {
		t.Error("i3vcoin output was not reverted")
	}

	// Revert the inverse diff.
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffRevert)
	if cst.cs.db.lenDelayedI3vcoinOutputsHeight(maturityHeight) != initialDscosLen+1 {
		t.Error("i3vcoin output diff set did not increase in size")
	}
	if cst.cs.db.getDelayedI3vcoinOutputs(maturityHeight, id).Value.Cmp(dsco.Value) != 0 {
		t.Error("wrong i3vcoin output value after committing a diff")
	}

	// Trigger an inconsistency check.
	defer func() {
		r := recover()
		if r != errBadCommitDelayedI3vcoinOutputDiff {
			t.Error("expecting errBadCommitDelayedI3vcoinOutputDiff, got", r)
		}
	}()
	// Try applying an apply diff that was already applied. (add an object
	// that already exists)
	dscod.Direction = modules.DiffApply                             // set the direction to apply
	cst.cs.commitDelayedI3vcoinOutputDiff(dscod, modules.DiffApply) // apply an already existing delayed output.
}
*/

/*
// TestCommitI3vfundPoolDiff probes the commitI3vfundPoolDiff method of the
// consensus set.
func TestCommitI3vfundPoolDiff(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	// Apply two i3vfund pool diffs, and then a diff with 0 change. Then revert
	// them all.
	initial := cst.cs.i3vfundPool
	adjusted1 := initial.Add(types.NewCurrency64(200))
	adjusted2 := adjusted1.Add(types.NewCurrency64(500))
	adjusted3 := adjusted2.Add(types.NewCurrency64(0))
	sfpd1 := modules.I3vfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  initial,
		Adjusted:  adjusted1,
	}
	sfpd2 := modules.I3vfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  adjusted1,
		Adjusted:  adjusted2,
	}
	sfpd3 := modules.I3vfundPoolDiff{
		Direction: modules.DiffApply,
		Previous:  adjusted2,
		Adjusted:  adjusted3,
	}
	cst.cs.commitI3vfundPoolDiff(sfpd1, modules.DiffApply)
	if cst.cs.i3vfundPool.Cmp(adjusted1) != 0 {
		t.Error("i3vfund pool was not adjusted correctly")
	}
	cst.cs.commitI3vfundPoolDiff(sfpd2, modules.DiffApply)
	if cst.cs.i3vfundPool.Cmp(adjusted2) != 0 {
		t.Error("second i3vfund pool adjustment was flawed")
	}
	cst.cs.commitI3vfundPoolDiff(sfpd3, modules.DiffApply)
	if cst.cs.i3vfundPool.Cmp(adjusted3) != 0 {
		t.Error("second i3vfund pool adjustment was flawed")
	}
	cst.cs.commitI3vfundPoolDiff(sfpd3, modules.DiffRevert)
	if cst.cs.i3vfundPool.Cmp(adjusted2) != 0 {
		t.Error("reverting second adjustment was flawed")
	}
	cst.cs.commitI3vfundPoolDiff(sfpd2, modules.DiffRevert)
	if cst.cs.i3vfundPool.Cmp(adjusted1) != 0 {
		t.Error("reverting second adjustment was flawed")
	}
	cst.cs.commitI3vfundPoolDiff(sfpd1, modules.DiffRevert)
	if cst.cs.i3vfundPool.Cmp(initial) != 0 {
		t.Error("reverting first adjustment was flawed")
	}

	// Do a chaining set of panics. First apply a negative pool adjustment,
	// then revert the pool diffs in the wrong order, than apply the pool diffs
	// in the wrong order.
	defer func() {
		r := recover()
		if r != errApplyI3vfundPoolDiffMismatch {
			t.Error("expecting errApplyI3vfundPoolDiffMismatch, got", r)
		}
	}()
	defer func() {
		r := recover()
		if r != errRevertI3vfundPoolDiffMismatch {
			t.Error("expecting errRevertI3vfundPoolDiffMismatch, got", r)
		}
		cst.cs.commitI3vfundPoolDiff(sfpd1, modules.DiffApply)
	}()
	defer func() {
		r := recover()
		if r != errNonApplyI3vfundPoolDiff {
			t.Error(r)
		}
		cst.cs.commitI3vfundPoolDiff(sfpd1, modules.DiffRevert)
	}()
	defer func() {
		r := recover()
		if r != errNegativePoolAdjustment {
			t.Error("expecting errNegativePoolAdjustment, got", r)
		}
		sfpd2.Direction = modules.DiffRevert
		cst.cs.commitI3vfundPoolDiff(sfpd2, modules.DiffApply)
	}()
	cst.cs.commitI3vfundPoolDiff(sfpd1, modules.DiffApply)
	cst.cs.commitI3vfundPoolDiff(sfpd2, modules.DiffApply)
	negativeAdjustment := adjusted2.Sub(types.NewCurrency64(100))
	negativeSfpd := modules.I3vfundPoolDiff{
		Previous: adjusted3,
		Adjusted: negativeAdjustment,
	}
	cst.cs.commitI3vfundPoolDiff(negativeSfpd, modules.DiffApply)
}
*/

/*
// TestDeleteObsoleteDelayedOutputMapsSanity probes the sanity checks of the
// deleteObsoleteDelayedOutputMaps method of the consensus set.
func TestDeleteObsoleteDelayedOutputMapsSanity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	pb := cst.cs.currentProcessedBlock()
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		return commitDiffSet(tx, pb, modules.DiffRevert)
	})
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting an error after corrupting the database")
		}
	}()
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting an error after corrupting the database")
		}

		// Trigger a panic by deleting a map with outputs in it during revert.
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return createUpcomingDelayedOutputMaps(tx, pb, modules.DiffApply)
		})
		if err != nil {
			t.Fatal(err)
		}
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return commitNodeDiffs(tx, pb, modules.DiffApply)
		})
		if err != nil {
			t.Fatal(err)
		}
		err = cst.cs.db.Update(func(tx *bolt.Tx) error {
			return deleteObsoleteDelayedOutputMaps(tx, pb, modules.DiffRevert)
		})
		if err != nil {
			t.Fatal(err)
		}
	}()

	// Trigger a panic by deleting a map with outputs in it during apply.
	err = cst.cs.db.Update(func(tx *bolt.Tx) error {
		return deleteObsoleteDelayedOutputMaps(tx, pb, modules.DiffApply)
	})
	if err != nil {
		t.Fatal(err)
	}
}
*/

/*
// TestGenerateAndApplyDiffSanity triggers the sanity checks in the
// generateAndApplyDiff method of the consensus set.
func TestGenerateAndApplyDiffSanity(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	pb := cst.cs.currentProcessedBlock()
	cst.cs.commitDiffSet(pb, modules.DiffRevert)

	defer func() {
		r := recover()
		if r != errRegenerateDiffs {
			t.Error("expected errRegenerateDiffs, got", r)
		}
	}()
	defer func() {
		r := recover()
		if r != errInvalidSuccessor {
			t.Error("expected errInvalidSuccessor, got", r)
		}

		// Trigger errRegenerteDiffs
		_ = cst.cs.generateAndApplyDiff(pb)
	}()

	// Trigger errInvalidSuccessor
	parent := cst.cs.db.getBlockMap(pb.Parent)
	parent.DiffsGenerated = false
	_ = cst.cs.generateAndApplyDiff(parent)
}
*/
