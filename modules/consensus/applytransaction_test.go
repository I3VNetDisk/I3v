package consensus

/*
import (
	"testing"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/types"
)

// TestApplyI3vcoinInputs probes the applyI3vcoinInputs method of the consensus
// set.
func TestApplyI3vcoinInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	// Create a consensus set and get it to 3 i3vcoin outputs. The consensus
	// set starts with 2 i3vcoin outputs, mining a block will add another.
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()
	b, _ := cst.miner.FindBlock()
	err = cst.cs.AcceptBlock(b)
	if err != nil {
		t.Fatal(err)
	}

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Fetch the output id's of each i3vcoin output in the consensus set.
	var ids []types.I3vcoinOutputID
	cst.cs.db.forEachI3vcoinOutputs(func(id types.I3vcoinOutputID, sco types.I3vcoinOutput) {
		ids = append(ids, id)
	})

	// Apply a transaction with a single i3vcoin input.
	txn := types.Transaction{
		I3vcoinInputs: []types.I3vcoinInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applyI3vcoinInputs(pb, txn)
	exists := cst.cs.db.inI3vcoinOutputs(ids[0])
	if exists {
		t.Error("Failed to conusme a i3vcoin output")
	}
	if cst.cs.db.lenI3vcoinOutputs() != 2 {
		t.Error("i3vcoin outputs not correctly updated")
	}
	if len(pb.I3vcoinOutputDiffs) != 1 {
		t.Error("block node was not updated for single transaction")
	}
	if pb.I3vcoinOutputDiffs[0].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when consuming a i3vcoin output")
	}
	if pb.I3vcoinOutputDiffs[0].ID != ids[0] {
		t.Error("wrong id used when consuming a i3vcoin output")
	}

	// Apply a transaction with two i3vcoin inputs.
	txn = types.Transaction{
		I3vcoinInputs: []types.I3vcoinInput{
			{ParentID: ids[1]},
			{ParentID: ids[2]},
		},
	}
	cst.cs.applyI3vcoinInputs(pb, txn)
	if cst.cs.db.lenI3vcoinOutputs() != 0 {
		t.Error("failed to consume all i3vcoin outputs in the consensus set")
	}
	if len(pb.I3vcoinOutputDiffs) != 3 {
		t.Error("processed block was not updated for single transaction")
	}
}

// TestMisuseApplyI3vcoinInputs misuses applyI3vcoinInput and checks that a
// panic was triggered.
func TestMisuseApplyI3vcoinInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Fetch the output id's of each i3vcoin output in the consensus set.
	var ids []types.I3vcoinOutputID
	cst.cs.db.forEachI3vcoinOutputs(func(id types.I3vcoinOutputID, sco types.I3vcoinOutput) {
		ids = append(ids, id)
	})

	// Apply a transaction with a single i3vcoin input.
	txn := types.Transaction{
		I3vcoinInputs: []types.I3vcoinInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applyI3vcoinInputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("expecting error after corrupting database")
		}
	}()
	cst.cs.applyI3vcoinInputs(pb, txn)
}

// TestApplyI3vcoinOutputs probes the applyI3vcoinOutput method of the
// consensus set.
func TestApplyI3vcoinOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single i3vcoin output.
	txn := types.Transaction{
		I3vcoinOutputs: []types.I3vcoinOutput{{}},
	}
	cst.cs.applyI3vcoinOutputs(pb, txn)
	scoid := txn.I3vcoinOutputID(0)
	exists := cst.cs.db.inI3vcoinOutputs(scoid)
	if !exists {
		t.Error("Failed to create i3vcoin output")
	}
	if cst.cs.db.lenI3vcoinOutputs() != 3 { // 3 because createConsensusSetTester has 2 initially.
		t.Error("i3vcoin outputs not correctly updated")
	}
	if len(pb.I3vcoinOutputDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.I3vcoinOutputDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a i3vcoin output")
	}
	if pb.I3vcoinOutputDiffs[0].ID != scoid {
		t.Error("wrong id used when creating a i3vcoin output")
	}

	// Apply a transaction with 2 i3vcoin outputs.
	txn = types.Transaction{
		I3vcoinOutputs: []types.I3vcoinOutput{
			{Value: types.NewCurrency64(1)},
			{Value: types.NewCurrency64(2)},
		},
	}
	cst.cs.applyI3vcoinOutputs(pb, txn)
	scoid0 := txn.I3vcoinOutputID(0)
	scoid1 := txn.I3vcoinOutputID(1)
	exists = cst.cs.db.inI3vcoinOutputs(scoid0)
	if !exists {
		t.Error("Failed to create i3vcoin output")
	}
	exists = cst.cs.db.inI3vcoinOutputs(scoid1)
	if !exists {
		t.Error("Failed to create i3vcoin output")
	}
	if cst.cs.db.lenI3vcoinOutputs() != 5 { // 5 because createConsensusSetTester has 2 initially.
		t.Error("i3vcoin outputs not correctly updated")
	}
	if len(pb.I3vcoinOutputDiffs) != 3 {
		t.Error("block node was not updated correctly")
	}
}

// TestMisuseApplyI3vcoinOutputs misuses applyI3vcoinOutputs and checks that a
// panic was triggered.
func TestMisuseApplyI3vcoinOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single i3vcoin output.
	txn := types.Transaction{
		I3vcoinOutputs: []types.I3vcoinOutput{{}},
	}
	cst.cs.applyI3vcoinOutputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("no panic occurred when misusing applyI3vcoinInput")
		}
	}()
	cst.cs.applyI3vcoinOutputs(pb, txn)
}

// TestApplyFileContracts probes the applyFileContracts method of the
// consensus set.
func TestApplyFileContracts(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single file contract.
	txn := types.Transaction{
		FileContracts: []types.FileContract{{}},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid := txn.FileContractID(0)
	exists := cst.cs.db.inFileContracts(fcid)
	if !exists {
		t.Error("Failed to create file contract")
	}
	if cst.cs.db.lenFileContracts() != 1 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a file contract")
	}
	if pb.FileContractDiffs[0].ID != fcid {
		t.Error("wrong id used when creating a file contract")
	}

	// Apply a transaction with 2 file contracts.
	txn = types.Transaction{
		FileContracts: []types.FileContract{
			{Payout: types.NewCurrency64(1)},
			{Payout: types.NewCurrency64(300e3)},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)
	exists = cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Failed to create file contract")
	}
	exists = cst.cs.db.inFileContracts(fcid1)
	if !exists {
		t.Error("Failed to create file contract")
	}
	if cst.cs.db.lenFileContracts() != 3 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 3 {
		t.Error("block node was not updated correctly")
	}
	if cst.cs.i3vfundPool.Cmp64(10e3) != 0 {
		t.Error("i3vfund pool did not update correctly upon creation of a file contract")
	}
}

// TestMisuseApplyFileContracts misuses applyFileContracts and checks that a
// panic was triggered.
func TestMisuseApplyFileContracts(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single file contract.
	txn := types.Transaction{
		FileContracts: []types.FileContract{{}},
	}
	cst.cs.applyFileContracts(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r == nil {
			t.Error("no panic occurred when misusing applyI3vcoinInput")
		}
	}()
	cst.cs.applyFileContracts(pb, txn)
}

// TestApplyFileContractRevisions probes the applyFileContractRevisions method
// of the consensus set.
func TestApplyFileContractRevisions(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with two file contracts - that way there is
	// something to revise.
	txn := types.Transaction{
		FileContracts: []types.FileContract{
			{},
			{Payout: types.NewCurrency64(1)},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)

	// Apply a single file contract revision.
	txn = types.Transaction{
		FileContractRevisions: []types.FileContractRevision{
			{
				ParentID:    fcid0,
				NewFileSize: 1,
			},
		},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
	exists := cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Revision killed a file contract")
	}
	fc := cst.cs.db.getFileContracts(fcid0)
	if fc.FileSize != 1 {
		t.Error("file contract filesize not properly updated")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 4 { // 2 creating the initial contracts, 1 to remove the old, 1 to add the revision.
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[2].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[3].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[2].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}
	if pb.FileContractDiffs[3].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}

	// Apply a transaction with 2 file contract revisions.
	txn = types.Transaction{
		FileContractRevisions: []types.FileContractRevision{
			{
				ParentID:    fcid0,
				NewFileSize: 2,
			},
			{
				ParentID:    fcid1,
				NewFileSize: 3,
			},
		},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
	exists = cst.cs.db.inFileContracts(fcid0)
	if !exists {
		t.Error("Revision ate file contract")
	}
	fc0 := cst.cs.db.getFileContracts(fcid0)
	exists = cst.cs.db.inFileContracts(fcid1)
	if !exists {
		t.Error("Revision ate file contract")
	}
	fc1 := cst.cs.db.getFileContracts(fcid1)
	if fc0.FileSize != 2 {
		t.Error("Revision not correctly applied")
	}
	if fc1.FileSize != 3 {
		t.Error("Revision not correctly applied")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 8 {
		t.Error("block node was not updated correctly")
	}
}

// TestMisuseApplyFileContractRevisions misuses applyFileContractRevisions and
// checks that a panic was triggered.
func TestMisuseApplyFileContractRevisions(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Trigger a panic from revising a nonexistent file contract.
	defer func() {
		r := recover()
		if r != errNilItem {
			t.Error("no panic occurred when misusing applyI3vcoinInput")
		}
	}()
	txn := types.Transaction{
		FileContractRevisions: []types.FileContractRevision{{}},
	}
	cst.cs.applyFileContractRevisions(pb, txn)
}

// TestApplyStorageProofs probes the applyStorageProofs method of the consensus
// set.
func TestApplyStorageProofs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Apply a transaction with two file contracts - there is a reason to
	// create a storage proof.
	txn := types.Transaction{
		FileContracts: []types.FileContract{
			{
				Payout: types.NewCurrency64(300e3),
				ValidProofOutputs: []types.I3vcoinOutput{
					{Value: types.NewCurrency64(290e3)},
				},
			},
			{},
			{
				Payout: types.NewCurrency64(600e3),
				ValidProofOutputs: []types.I3vcoinOutput{
					{Value: types.NewCurrency64(280e3)},
					{Value: types.NewCurrency64(300e3)},
				},
			},
		},
	}
	cst.cs.applyFileContracts(pb, txn)
	fcid0 := txn.FileContractID(0)
	fcid1 := txn.FileContractID(1)
	fcid2 := txn.FileContractID(2)

	// Apply a single storage proof.
	txn = types.Transaction{
		StorageProofs: []types.StorageProof{{ParentID: fcid0}},
	}
	cst.cs.applyStorageProofs(pb, txn)
	exists := cst.cs.db.inFileContracts(fcid0)
	if exists {
		t.Error("Storage proof did not disable a file contract.")
	}
	if cst.cs.db.lenFileContracts() != 2 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 4 { // 3 creating the initial contracts, 1 for the storage proof.
		t.Error("block node was not updated for single element transaction")
	}
	if pb.FileContractDiffs[3].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when revising a file contract")
	}
	if pb.FileContractDiffs[3].ID != fcid0 {
		t.Error("wrong id used when revising a file contract")
	}
	spoid0 := fcid0.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inDelayedI3vcoinOutputsHeight(pb.Height+types.MaturityDelay, spoid0)
	if !exists {
		t.Error("storage proof output not created after applying a storage proof")
	}
	sco := cst.cs.db.getDelayedI3vcoinOutputs(pb.Height+types.MaturityDelay, spoid0)
	if sco.Value.Cmp64(290e3) != 0 {
		t.Error("storage proof output was created with the wrong value")
	}

	// Apply a transaction with 2 storage proofs.
	txn = types.Transaction{
		StorageProofs: []types.StorageProof{
			{ParentID: fcid1},
			{ParentID: fcid2},
		},
	}
	cst.cs.applyStorageProofs(pb, txn)
	exists = cst.cs.db.inFileContracts(fcid1)
	if exists {
		t.Error("Storage proof failed to consume file contract.")
	}
	exists = cst.cs.db.inFileContracts(fcid2)
	if exists {
		t.Error("storage proof did not consume file contract")
	}
	if cst.cs.db.lenFileContracts() != 0 {
		t.Error("file contracts not correctly updated")
	}
	if len(pb.FileContractDiffs) != 6 {
		t.Error("block node was not updated correctly")
	}
	spoid1 := fcid1.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inI3vcoinOutputs(spoid1)
	if exists {
		t.Error("output created when file contract had no corresponding output")
	}
	spoid2 := fcid2.StorageProofOutputID(types.ProofValid, 0)
	exists = cst.cs.db.inDelayedI3vcoinOutputsHeight(pb.Height+types.MaturityDelay, spoid2)
	if !exists {
		t.Error("no output created by first output of file contract")
	}
	sco = cst.cs.db.getDelayedI3vcoinOutputs(pb.Height+types.MaturityDelay, spoid2)
	if sco.Value.Cmp64(280e3) != 0 {
		t.Error("first i3vcoin output created has wrong value")
	}
	spoid3 := fcid2.StorageProofOutputID(types.ProofValid, 1)
	exists = cst.cs.db.inDelayedI3vcoinOutputsHeight(pb.Height+types.MaturityDelay, spoid3)
	if !exists {
		t.Error("second output not created for storage proof")
	}
	sco = cst.cs.db.getDelayedI3vcoinOutputs(pb.Height+types.MaturityDelay, spoid3)
	if sco.Value.Cmp64(300e3) != 0 {
		t.Error("second i3vcoin output has wrong value")
	}
	if cst.cs.i3vfundPool.Cmp64(30e3) != 0 {
		t.Error("i3vfund pool not being added up correctly")
	}
}

// TestNonexistentStorageProof applies a storage proof which points to a
// nonextentent parent.
func TestNonexistentStorageProof(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Trigger a panic by applying a storage proof for a nonexistent file
	// contract.
	defer func() {
		r := recover()
		if r != errNilItem {
			t.Error("no panic occurred when misusing applyI3vcoinInput")
		}
	}()
	txn := types.Transaction{
		StorageProofs: []types.StorageProof{{}},
	}
	cst.cs.applyStorageProofs(pb, txn)
}

// TestDuplicateStorageProof applies a storage proof which has already been
// applied.
func TestDuplicateStorageProof(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Create a file contract for the storage proof to prove.
	txn0 := types.Transaction{
		FileContracts: []types.FileContract{
			{
				Payout: types.NewCurrency64(300e3),
				ValidProofOutputs: []types.I3vcoinOutput{
					{Value: types.NewCurrency64(290e3)},
				},
			},
		},
	}
	cst.cs.applyFileContracts(pb, txn0)
	fcid := txn0.FileContractID(0)

	// Apply a single storage proof.
	txn1 := types.Transaction{
		StorageProofs: []types.StorageProof{{ParentID: fcid}},
	}
	cst.cs.applyStorageProofs(pb, txn1)

	// Trigger a panic by applying the storage proof again.
	defer func() {
		r := recover()
		if r != ErrDuplicateValidProofOutput {
			t.Error("failed to trigger ErrDuplicateValidProofOutput:", r)
		}
	}()
	cst.cs.applyFileContracts(pb, txn0) // File contract was consumed by the first proof.
	cst.cs.applyStorageProofs(pb, txn1)
}

// TestApplyI3vfundInputs probes the applyI3vfundInputs method of the consensus
// set.
func TestApplyI3vfundInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Fetch the output id's of each i3vcoin output in the consensus set.
	var ids []types.I3vfundOutputID
	cst.cs.db.forEachI3vfundOutputs(func(sfoid types.I3vfundOutputID, sfo types.I3vfundOutput) {
		ids = append(ids, sfoid)
	})

	// Apply a transaction with a single i3vfund input.
	txn := types.Transaction{
		I3vfundInputs: []types.I3vfundInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applyI3vfundInputs(pb, txn)
	exists := cst.cs.db.inI3vfundOutputs(ids[0])
	if exists {
		t.Error("Failed to conusme a i3vfund output")
	}
	if cst.cs.db.lenI3vfundOutputs() != 2 {
		t.Error("i3vfund outputs not correctly updated", cst.cs.db.lenI3vfundOutputs())
	}
	if len(pb.I3vfundOutputDiffs) != 1 {
		t.Error("block node was not updated for single transaction")
	}
	if pb.I3vfundOutputDiffs[0].Direction != modules.DiffRevert {
		t.Error("wrong diff direction applied when consuming a i3vfund output")
	}
	if pb.I3vfundOutputDiffs[0].ID != ids[0] {
		t.Error("wrong id used when consuming a i3vfund output")
	}
	if cst.cs.db.lenDelayedI3vcoinOutputsHeight(cst.cs.height()+types.MaturityDelay) != 2 { // 1 for a block subsidy, 1 for the i3vfund claim.
		t.Error("i3vfund claim was not created")
	}
}

// TestMisuseApplyI3vfundInputs misuses applyI3vfundInputs and checks that a
// panic was triggered.
func TestMisuseApplyI3vfundInputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)
	pb.Height = cst.cs.height()

	// Fetch the output id's of each i3vcoin output in the consensus set.
	var ids []types.I3vfundOutputID
	cst.cs.db.forEachI3vfundOutputs(func(sfoid types.I3vfundOutputID, sfo types.I3vfundOutput) {
		ids = append(ids, sfoid)
	})

	// Apply a transaction with a single i3vfund input.
	txn := types.Transaction{
		I3vfundInputs: []types.I3vfundInput{
			{ParentID: ids[0]},
		},
	}
	cst.cs.applyI3vfundInputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r != ErrMisuseApplyI3vfundInput {
			t.Error("no panic occurred when misusing applyI3vcoinInput")
			t.Error(r)
		}
	}()
	cst.cs.applyI3vfundInputs(pb, txn)
}

// TestApplyI3vfundOutputs probes the applyI3vfundOutputs method of the
// consensus set.
func TestApplyI3vfundOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()
	cst.cs.i3vfundPool = types.NewCurrency64(101)

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single i3vfund output.
	txn := types.Transaction{
		I3vfundOutputs: []types.I3vfundOutput{{}},
	}
	cst.cs.applyI3vfundOutputs(pb, txn)
	sfoid := txn.I3vfundOutputID(0)
	exists := cst.cs.db.inI3vfundOutputs(sfoid)
	if !exists {
		t.Error("Failed to create i3vfund output")
	}
	if cst.cs.db.lenI3vfundOutputs() != 4 {
		t.Error("i3vfund outputs not correctly updated")
	}
	if len(pb.I3vfundOutputDiffs) != 1 {
		t.Error("block node was not updated for single element transaction")
	}
	if pb.I3vfundOutputDiffs[0].Direction != modules.DiffApply {
		t.Error("wrong diff direction applied when creating a i3vfund output")
	}
	if pb.I3vfundOutputDiffs[0].ID != sfoid {
		t.Error("wrong id used when creating a i3vfund output")
	}
	if pb.I3vfundOutputDiffs[0].I3vfundOutput.ClaimStart.Cmp64(101) != 0 {
		t.Error("claim start set incorrectly when creating a i3vfund output")
	}

	// Apply a transaction with 2 i3vcoin outputs.
	txn = types.Transaction{
		I3vfundOutputs: []types.I3vfundOutput{
			{Value: types.NewCurrency64(1)},
			{Value: types.NewCurrency64(2)},
		},
	}
	cst.cs.applyI3vfundOutputs(pb, txn)
	sfoid0 := txn.I3vfundOutputID(0)
	sfoid1 := txn.I3vfundOutputID(1)
	exists = cst.cs.db.inI3vfundOutputs(sfoid0)
	if !exists {
		t.Error("Failed to create i3vfund output")
	}
	exists = cst.cs.db.inI3vfundOutputs(sfoid1)
	if !exists {
		t.Error("Failed to create i3vfund output")
	}
	if cst.cs.db.lenI3vfundOutputs() != 6 {
		t.Error("i3vfund outputs not correctly updated")
	}
	if len(pb.I3vfundOutputDiffs) != 3 {
		t.Error("block node was not updated for single element transaction")
	}
}

// TestMisuseApplyI3vfundOutputs misuses applyI3vfundOutputs and checks that a
// panic was triggered.
func TestMisuseApplyI3vfundOutputs(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	cst, err := createConsensusSetTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cst.closeCst()

	// Create a block node to use with application.
	pb := new(processedBlock)

	// Apply a transaction with a single i3vcoin output.
	txn := types.Transaction{
		I3vfundOutputs: []types.I3vfundOutput{{}},
	}
	cst.cs.applyI3vfundOutputs(pb, txn)

	// Trigger the panic that occurs when an output is applied incorrectly, and
	// perform a catch to read the error that is created.
	defer func() {
		r := recover()
		if r != ErrMisuseApplyI3vfundOutput {
			t.Error("no panic occurred when misusing applyI3vfundInput")
		}
	}()
	cst.cs.applyI3vfundOutputs(pb, txn)
}
*/
