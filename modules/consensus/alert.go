package consensus

import (
	"gitlab.com/I3VNetDisk/I3v/modules"
)

// Alerts implements the Alerter interface for the consensusset.
func (c *ConsensusSet) Alerts() []modules.Alert {
	return []modules.Alert{}
}
