package consensus

// applytransaction.go handles applying a transaction to the consensus set.
// There is an assumption that the transaction has already been verified.

import (
	"gitlab.com/I3VNetDisk/bolt"

	"gitlab.com/I3VNetDisk/I3v/build"
	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/types"
)

// applyI3vcoinInputs takes all of the i3vcoin inputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applyI3vcoinInputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	// Remove all i3vcoin inputs from the unspent i3vcoin outputs list.
	for _, sci := range t.I3vcoinInputs {
		sco, err := getI3vcoinOutput(tx, sci.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}
		scod := modules.I3vcoinOutputDiff{
			Direction:     modules.DiffRevert,
			ID:            sci.ParentID,
			I3vcoinOutput: sco,
		}
		pb.I3vcoinOutputDiffs = append(pb.I3vcoinOutputDiffs, scod)
		commitI3vcoinOutputDiff(tx, scod, modules.DiffApply)
	}
}

// applyI3vcoinOutputs takes all of the i3vcoin outputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applyI3vcoinOutputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	// Add all i3vcoin outputs to the unspent i3vcoin outputs list.
	for i, sco := range t.I3vcoinOutputs {
		scoid := t.I3vcoinOutputID(uint64(i))
		scod := modules.I3vcoinOutputDiff{
			Direction:     modules.DiffApply,
			ID:            scoid,
			I3vcoinOutput: sco,
		}
		pb.I3vcoinOutputDiffs = append(pb.I3vcoinOutputDiffs, scod)
		commitI3vcoinOutputDiff(tx, scod, modules.DiffApply)
	}
}

// applyFileContracts iterates through all of the file contracts in a
// transaction and applies them to the state, updating the diffs in the proccesed
// block.
func applyFileContracts(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for i, fc := range t.FileContracts {
		fcid := t.FileContractID(uint64(i))
		fcd := modules.FileContractDiff{
			Direction:    modules.DiffApply,
			ID:           fcid,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)

		// Get the portion of the contract that goes into the i3vfund pool and
		// add it to the i3vfund pool.
		sfp := getI3vfundPool(tx)
		sfpd := modules.I3vfundPoolDiff{
			Direction: modules.DiffApply,
			Previous:  sfp,
			Adjusted:  sfp.Add(types.Tax(blockHeight(tx), fc.Payout)),
		}
		pb.I3vfundPoolDiffs = append(pb.I3vfundPoolDiffs, sfpd)
		commitI3vfundPoolDiff(tx, sfpd, modules.DiffApply)
	}
}

// applyTxFileContractRevisions iterates through all of the file contract
// revisions in a transaction and applies them to the state, updating the diffs
// in the processed block.
func applyFileContractRevisions(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, fcr := range t.FileContractRevisions {
		fc, err := getFileContract(tx, fcr.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}

		// Add the diff to delete the old file contract.
		fcd := modules.FileContractDiff{
			Direction:    modules.DiffRevert,
			ID:           fcr.ParentID,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)

		// Add the diff to add the revised file contract.
		newFC := types.FileContract{
			FileSize:           fcr.NewFileSize,
			FileMerkleRoot:     fcr.NewFileMerkleRoot,
			WindowStart:        fcr.NewWindowStart,
			WindowEnd:          fcr.NewWindowEnd,
			Payout:             fc.Payout,
			ValidProofOutputs:  fcr.NewValidProofOutputs,
			MissedProofOutputs: fcr.NewMissedProofOutputs,
			UnlockHash:         fcr.NewUnlockHash,
			RevisionNumber:     fcr.NewRevisionNumber,
		}
		fcd = modules.FileContractDiff{
			Direction:    modules.DiffApply,
			ID:           fcr.ParentID,
			FileContract: newFC,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)
	}
}

// applyTxStorageProofs iterates through all of the storage proofs in a
// transaction and applies them to the state, updating the diffs in the processed
// block.
func applyStorageProofs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, sp := range t.StorageProofs {
		fc, err := getFileContract(tx, sp.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}

		// Add all of the outputs in the ValidProofOutputs of the contract.
		for i, vpo := range fc.ValidProofOutputs {
			spoid := sp.ParentID.StorageProofOutputID(types.ProofValid, uint64(i))
			dscod := modules.DelayedI3vcoinOutputDiff{
				Direction:      modules.DiffApply,
				ID:             spoid,
				I3vcoinOutput:  vpo,
				MaturityHeight: pb.Height + types.MaturityDelay,
			}
			pb.DelayedI3vcoinOutputDiffs = append(pb.DelayedI3vcoinOutputDiffs, dscod)
			commitDelayedI3vcoinOutputDiff(tx, dscod, modules.DiffApply)
		}

		fcd := modules.FileContractDiff{
			Direction:    modules.DiffRevert,
			ID:           sp.ParentID,
			FileContract: fc,
		}
		pb.FileContractDiffs = append(pb.FileContractDiffs, fcd)
		commitFileContractDiff(tx, fcd, modules.DiffApply)
	}
}

// applyTxI3vfundInputs takes all of the i3vfund inputs in a transaction and
// applies them to the state, updating the diffs in the processed block.
func applyI3vfundInputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for _, sfi := range t.I3vfundInputs {
		// Calculate the volume of i3vcoins to put in the claim output.
		sfo, err := getI3vfundOutput(tx, sfi.ParentID)
		if build.DEBUG && err != nil {
			panic(err)
		}
		claimPortion := getI3vfundPool(tx).Sub(sfo.ClaimStart).Div(types.I3vfundCount).Mul(sfo.Value)

		// Add the claim output to the delayed set of outputs.
		sco := types.I3vcoinOutput{
			Value:      claimPortion,
			UnlockHash: sfi.ClaimUnlockHash,
		}
		sfoid := sfi.ParentID.I3vClaimOutputID()
		dscod := modules.DelayedI3vcoinOutputDiff{
			Direction:      modules.DiffApply,
			ID:             sfoid,
			I3vcoinOutput:  sco,
			MaturityHeight: pb.Height + types.MaturityDelay,
		}
		pb.DelayedI3vcoinOutputDiffs = append(pb.DelayedI3vcoinOutputDiffs, dscod)
		commitDelayedI3vcoinOutputDiff(tx, dscod, modules.DiffApply)

		// Create the i3vfund output diff and remove the output from the
		// consensus set.
		sfod := modules.I3vfundOutputDiff{
			Direction:     modules.DiffRevert,
			ID:            sfi.ParentID,
			I3vfundOutput: sfo,
		}
		pb.I3vfundOutputDiffs = append(pb.I3vfundOutputDiffs, sfod)
		commitI3vfundOutputDiff(tx, sfod, modules.DiffApply)
	}
}

// applyI3vfundOutput applies a i3vfund output to the consensus set.
func applyI3vfundOutputs(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	for i, sfo := range t.I3vfundOutputs {
		sfoid := t.I3vfundOutputID(uint64(i))
		sfo.ClaimStart = getI3vfundPool(tx)
		sfod := modules.I3vfundOutputDiff{
			Direction:     modules.DiffApply,
			ID:            sfoid,
			I3vfundOutput: sfo,
		}
		pb.I3vfundOutputDiffs = append(pb.I3vfundOutputDiffs, sfod)
		commitI3vfundOutputDiff(tx, sfod, modules.DiffApply)
	}
}

// applyTransaction applies the contents of a transaction to the ConsensusSet.
// This produces a set of diffs, which are stored in the blockNode containing
// the transaction. No verification is done by this function.
func applyTransaction(tx *bolt.Tx, pb *processedBlock, t types.Transaction) {
	applyI3vcoinInputs(tx, pb, t)
	applyI3vcoinOutputs(tx, pb, t)
	applyFileContracts(tx, pb, t)
	applyFileContractRevisions(tx, pb, t)
	applyStorageProofs(tx, pb, t)
	applyI3vfundInputs(tx, pb, t)
	applyI3vfundOutputs(tx, pb, t)
}
