package modules

import (
	"testing"

	"gitlab.com/I3VNetDisk/errors"
)

var (
	// TestGlobalI3vPathVar tests that the NewGlobalI3vPath initialization
	// works.
	TestGlobalI3vPathVar I3vPath = NewGlobalI3vPath("/testdir")
)

// TestGlobalI3vPath checks that initializing a new global i3vpath does not
// cause any issues.
func TestGlobalI3vPath(t *testing.T) {
	sp, err := TestGlobalI3vPathVar.Join("testfile")
	if err != nil {
		t.Fatal(err)
	}
	mirror, err := NewI3vPath("/testdir")
	if err != nil {
		t.Fatal(err)
	}
	expected, err := mirror.Join("testfile")
	if err != nil {
		t.Fatal(err)
	}
	if !sp.Equals(expected) {
		t.Error("the separately spawned i3vpath should equal the global i3vpath")
	}
}

// TestI3vpathValidate verifies that the validate function correctly validates
// I3vPaths.
func TestI3vpathValidate(t *testing.T) {
	var pathtests = []struct {
		in    string
		valid bool
	}{
		{"valid/i3vpath", true},
		{"../../../directory/traversal", false},
		{"testpath", true},
		{"valid/i3vpath/../with/directory/traversal", false},
		{"validpath/test", true},
		{"..validpath/..test", true},
		{"./invalid/path", false},
		{".../path", true},
		{"valid./path", true},
		{"valid../path", true},
		{"valid/path./test", true},
		{"valid/path../test", true},
		{"test/path", true},
		{"/leading/slash", false},
		{"foo/./bar", false},
		{"", false},
		{"blank/end/", false},
		{"double//dash", false},
		{"../", false},
		{"./", false},
		{".", false},
	}
	for _, pathtest := range pathtests {
		i3vPath := I3vPath{
			Path: pathtest.in,
		}
		err := i3vPath.Validate(false)
		if err != nil && pathtest.valid {
			t.Fatal("validateI3vpath failed on valid path: ", pathtest.in)
		}
		if err == nil && !pathtest.valid {
			t.Fatal("validateI3vpath succeeded on invalid path: ", pathtest.in)
		}
	}
}

// TestI3vpath tests that the NewI3vPath, LoadString, and Join methods function correctly
func TestI3vpath(t *testing.T) {
	var pathtests = []struct {
		in    string
		valid bool
	}{
		{"valid/i3vpath", true},
		{"\\some\\windows\\path", true}, // clean converts OS separators
		{"../../../directory/traversal", false},
		{"testpath", true},
		{"valid/i3vpath/../with/directory/traversal", false},
		{"validpath/test", true},
		{"..validpath/..test", true},
		{"./invalid/path", false},
		{".../path", true},
		{"valid./path", true},
		{"valid../path", true},
		{"valid/path./test", true},
		{"valid/path../test", true},
		{"test/path", true},
		{"/leading/slash", true}, // clean will trim leading slashes so this is a valid input
		{"foo/./bar", false},
		{"", false},
		{"blank/end/", true}, // clean will trim trailing slashes so this is a valid input
		{"double//dash", false},
		{"../", false},
		{"./", false},
		{".", false},
		{"dollar$sign", true},
		{"and&sign", true},
		{"single`quote", true},
		{"full:colon", true},
		{"semi;colon", true},
		{"hash#tag", true},
		{"percent%sign", true},
		{"at@sign", true},
		{"less<than", true},
		{"greater>than", true},
		{"equal=to", true},
		{"question?mark", true},
		{"open[bracket", true},
		{"close]bracket", true},
		{"open{bracket", true},
		{"close}bracket", true},
		{"carrot^top", true},
		{"pipe|pipe", true},
		{"tilda~tilda", true},
		{"plus+sign", true},
		{"minus-sign", true},
		{"under_score", true},
		{"comma,comma", true},
		{"apostrophy's", true},
		{`quotation"marks`, true},
	}

	// Test NewI3vPath
	for _, pathtest := range pathtests {
		_, err := NewI3vPath(pathtest.in)
		// Verify expected Error
		if err != nil && pathtest.valid {
			t.Fatal("validateI3vpath failed on valid path: ", pathtest.in)
		}
		if err == nil && !pathtest.valid {
			t.Fatal("validateI3vpath succeeded on invalid path: ", pathtest.in)
		}
	}

	// Test LoadString
	var sp I3vPath
	for _, pathtest := range pathtests {
		err := sp.LoadString(pathtest.in)
		// Verify expected Error
		if err != nil && pathtest.valid {
			t.Fatal("validateI3vpath failed on valid path: ", pathtest.in)
		}
		if err == nil && !pathtest.valid {
			t.Fatal("validateI3vpath succeeded on invalid path: ", pathtest.in)
		}
	}

	// Test Join
	sp, err := NewI3vPath("test")
	if err != nil {
		t.Fatal(err)
	}
	for _, pathtest := range pathtests {
		_, err = sp.Join(pathtest.in)
		// Verify expected Error
		if err != nil && pathtest.valid {
			t.Fatal("validateI3vpath failed on valid path: ", pathtest.in)
		}
		if err == nil && !pathtest.valid {
			t.Fatal("validateI3vpath succeeded on invalid path: ", pathtest.in)
		}
	}
}

// TestI3vpathRebase tests the I3vPath.Rebase method.
func TestI3vpathRebase(t *testing.T) {
	var rebasetests = []struct {
		oldBase string
		newBase string
		i3vPath string
		result  string
	}{
		{"a/b", "a", "a/b/myfile", "a/myfile"}, // basic rebase
		{"a/b", "", "a/b/myfile", "myfile"},    // newBase is root
		{"", "b", "myfile", "b/myfile"},        // oldBase is root
		{"a/a", "a/b", "a/a", "a/b"},           // folder == oldBase
	}

	for _, test := range rebasetests {
		var oldBase, newBase I3vPath
		var err1, err2 error
		if test.oldBase == "" {
			oldBase = RootI3vPath()
		} else {
			oldBase, err1 = newI3vPath(test.oldBase)
		}
		if test.newBase == "" {
			newBase = RootI3vPath()
		} else {
			newBase, err2 = newI3vPath(test.newBase)
		}
		file, err3 := newI3vPath(test.i3vPath)
		expectedPath, err4 := newI3vPath(test.result)
		if err := errors.Compose(err1, err2, err3, err4); err != nil {
			t.Fatal(err)
		}
		// Rebase the path
		res, err := file.Rebase(oldBase, newBase)
		if err != nil {
			t.Fatal(err)
		}
		// Check result.
		if !res.Equals(expectedPath) {
			t.Fatalf("'%v' doesn't match '%v'", res.String(), expectedPath.String())
		}
	}
}

// TestI3vpathDir probes the Dir function for I3vPaths.
func TestI3vpathDir(t *testing.T) {
	var pathtests = []struct {
		path string
		dir  string
	}{
		{"one/dir", "one"},
		{"many/more/dirs", "many/more"},
		{"nodir", ""},
		{"/leadingslash", ""},
		{"./leadingdotslash", ""},
		{"", ""},
		{".", ""},
	}
	for _, pathtest := range pathtests {
		i3vPath := I3vPath{
			Path: pathtest.path,
		}
		dir, err := i3vPath.Dir()
		if err != nil {
			t.Errorf("Dir should not return an error %v, path %v", err, pathtest.path)
			continue
		}
		if dir.Path != pathtest.dir {
			t.Errorf("Dir %v not the same as expected dir %v ", dir.Path, pathtest.dir)
			continue
		}
	}
}

// TestI3vpathName probes the Name function for I3vPaths.
func TestI3vpathName(t *testing.T) {
	var pathtests = []struct {
		path string
		name string
	}{
		{"one/dir", "dir"},
		{"many/more/dirs", "dirs"},
		{"nodir", "nodir"},
		{"/leadingslash", "leadingslash"},
		{"./leadingdotslash", "leadingdotslash"},
		{"", ""},
		{".", ""},
	}
	for _, pathtest := range pathtests {
		i3vPath := I3vPath{
			Path: pathtest.path,
		}
		name := i3vPath.Name()
		if name != pathtest.name {
			t.Errorf("name %v not the same as expected name %v ", name, pathtest.name)
		}
	}
}
