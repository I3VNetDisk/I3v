package wallet

import (
	"gitlab.com/I3VNetDisk/I3v/modules"
)

// Alerts implements the Alerter interface for the wallet.
func (w *Wallet) Alerts() []modules.Alert {
	return []modules.Alert{}
}
