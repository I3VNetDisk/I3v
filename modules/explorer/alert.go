package explorer

import "gitlab.com/I3VNetDisk/I3v/modules"

// Alerts implements the modules.Alerter interface for the explorer.
func (e *Explorer) Alerts() []modules.Alert {
	return []modules.Alert{}
}
