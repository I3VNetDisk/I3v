package client

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/I3VNetDisk/errors"

	"gitlab.com/I3VNetDisk/I3v/modules"
	"gitlab.com/I3VNetDisk/I3v/node/api"
	"gitlab.com/I3VNetDisk/I3v/types"
)

type (
	// AllowanceRequestPost is a helper type to be able to build an allowance
	// request.
	AllowanceRequestPost struct {
		c      *Client
		sent   bool
		values url.Values
	}
)

// RenterPostPartialAllowance starts an allowance request which can be extended
// using its methods.
func (c *Client) RenterPostPartialAllowance() *AllowanceRequestPost {
	return &AllowanceRequestPost{c: c, values: make(url.Values)}
}

// WithFunds adds the funds field to the request.
func (a *AllowanceRequestPost) WithFunds(funds types.Currency) *AllowanceRequestPost {
	a.values.Set("funds", funds.String())
	return a
}

// WithHosts adds the hosts field to the request.
func (a *AllowanceRequestPost) WithHosts(hosts uint64) *AllowanceRequestPost {
	a.values.Set("hosts", fmt.Sprint(hosts))
	return a
}

// WithPeriod adds the period field to the request.
func (a *AllowanceRequestPost) WithPeriod(period types.BlockHeight) *AllowanceRequestPost {
	a.values.Set("period", fmt.Sprint(period))
	return a
}

// WithRenewWindow adds the renewwindow field to the request.
func (a *AllowanceRequestPost) WithRenewWindow(renewWindow types.BlockHeight) *AllowanceRequestPost {
	a.values.Set("renewwindow", fmt.Sprint(renewWindow))
	return a
}

// WithPaymentContractInitialFunding adds the paymentcontractinitialfunding
// field to the request.
func (a *AllowanceRequestPost) WithPaymentContractInitialFunding(price types.Currency) *AllowanceRequestPost {
	a.values.Set("paymentcontractinitialfunding", price.String())
	return a
}

// WithExpectedStorage adds the expected storage field to the request.
func (a *AllowanceRequestPost) WithExpectedStorage(expectedStorage uint64) *AllowanceRequestPost {
	a.values.Set("expectedstorage", fmt.Sprint(expectedStorage))
	return a
}

// WithExpectedUpload adds the expected upload field to the request.
func (a *AllowanceRequestPost) WithExpectedUpload(expectedUpload uint64) *AllowanceRequestPost {
	a.values.Set("expectedupload", fmt.Sprint(expectedUpload))
	return a
}

// WithExpectedDownload adds the expected download field to the request.
func (a *AllowanceRequestPost) WithExpectedDownload(expectedDownload uint64) *AllowanceRequestPost {
	a.values.Set("expecteddownload", fmt.Sprint(expectedDownload))
	return a
}

// WithExpectedRedundancy adds the expected redundancy field to the request.
func (a *AllowanceRequestPost) WithExpectedRedundancy(expectedRedundancy float64) *AllowanceRequestPost {
	a.values.Set("expectedredundancy", fmt.Sprint(expectedRedundancy))
	return a
}

// WithMaxPeriodChurn adds the expected redundancy field to the request.
func (a *AllowanceRequestPost) WithMaxPeriodChurn(maxPeriodChurn uint64) *AllowanceRequestPost {
	a.values.Set("maxperiodchurn", fmt.Sprint(maxPeriodChurn))
	return a
}

// WithMaxRPCPrice adds the maxrpcprice field to the request.
func (a *AllowanceRequestPost) WithMaxRPCPrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxrpcprice", price.String())
	return a
}

// WithMaxContractPrice adds the maxcontract field to the request.
func (a *AllowanceRequestPost) WithMaxContractPrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxcontractprice", price.String())
	return a
}

// WithMaxDownloadBandwidthPrice adds the maxdownloadbandwidthprice field to the request.
func (a *AllowanceRequestPost) WithMaxDownloadBandwidthPrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxdownloadbandwidthprice", price.String())
	return a
}

// WithMaxSectorAccessPrice adds the maxsectoraccessprice field to the request.
func (a *AllowanceRequestPost) WithMaxSectorAccessPrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxsectoraccessprice", price.String())
	return a
}

// WithMaxStoragePrice adds the maxstorageprice field to the request.
func (a *AllowanceRequestPost) WithMaxStoragePrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxstorageprice", price.String())
	return a
}

// WithMaxUploadBandwidthPrice adds the maxuploadbandwidthprice field to the request.
func (a *AllowanceRequestPost) WithMaxUploadBandwidthPrice(price types.Currency) *AllowanceRequestPost {
	a.values.Set("maxuploadbandwidthprice", price.String())
	return a
}

// Send finalizes and sends the request.
func (a *AllowanceRequestPost) Send() (err error) {
	if a.sent {
		return errors.New("Error, request already sent")
	}
	a.sent = true
	err = a.c.post("/renter", a.values.Encode(), nil)
	return
}

// escapeI3vPath escapes the i3vpath to make it safe to use within a URL. This
// should only be used on I3vPaths which are used as part of the URL path.
// Paths within the query have to be escaped with url.PathEscape.
func escapeI3vPath(i3vPath modules.I3vPath) string {
	sp := i3vPath.String()
	pathSegments := strings.Split(sp, "/")

	escapedSegments := make([]string, 0, len(pathSegments))
	for _, segment := range pathSegments {
		escapedSegments = append(escapedSegments, url.PathEscape(segment))
	}
	return strings.Join(escapedSegments, "/")
}

// RenterContractorChurnStatus uses the /renter/contractorchurnstatus endpoint
// to get the current contractor churn status.
func (c *Client) RenterContractorChurnStatus() (churnStatus modules.ContractorChurnStatus, err error) {
	err = c.get("/renter/contractorchurnstatus", &churnStatus)
	return
}

// RenterContractCancelPost uses the /renter/contract/cancel endpoint to cancel
// a contract
func (c *Client) RenterContractCancelPost(id types.FileContractID) (err error) {
	values := url.Values{}
	values.Set("id", id.String())
	err = c.post("/renter/contract/cancel", values.Encode(), nil)
	return
}

// RenterAllContractsGet requests the /renter/contracts resource with all
// options set to true
func (c *Client) RenterAllContractsGet() (rc api.RenterContracts, err error) {
	values := url.Values{}
	values.Set("disabled", fmt.Sprint(true))
	values.Set("expired", fmt.Sprint(true))
	values.Set("recoverable", fmt.Sprint(true))
	err = c.get("/renter/contracts?"+values.Encode(), &rc)
	return
}

// RenterContractsGet requests the /renter/contracts resource and returns
// Contracts and ActiveContracts
func (c *Client) RenterContractsGet() (rc api.RenterContracts, err error) {
	err = c.get("/renter/contracts", &rc)
	return
}

// RenterContractStatus requests the /watchdog/contractstatus resource and returns
// the status of a contract.
func (c *Client) RenterContractStatus(fcID types.FileContractID) (status modules.ContractWatchStatus, err error) {
	values := url.Values{}
	values.Set("id", fcID.String())
	err = c.get("/renter/contractstatus?"+values.Encode(), &status)
	return
}

// RenterDisabledContractsGet requests the /renter/contracts resource with the
// disabled flag set to true
func (c *Client) RenterDisabledContractsGet() (rc api.RenterContracts, err error) {
	values := url.Values{}
	values.Set("disabled", fmt.Sprint(true))
	err = c.get("/renter/contracts?"+values.Encode(), &rc)
	return
}

// RenterInactiveContractsGet requests the /renter/contracts resource with the
// inactive flag set to true
func (c *Client) RenterInactiveContractsGet() (rc api.RenterContracts, err error) {
	values := url.Values{}
	values.Set("inactive", fmt.Sprint(true))
	err = c.get("/renter/contracts?"+values.Encode(), &rc)
	return
}

// RenterInitContractRecoveryScanPost initializes a contract recovery scan
// using the /renter/recoveryscan endpoint.
func (c *Client) RenterInitContractRecoveryScanPost() (err error) {
	err = c.post("/renter/recoveryscan", "", nil)
	return
}

// RenterContractRecoveryProgressGet returns information about potentially
// ongoing contract recovery scans.
func (c *Client) RenterContractRecoveryProgressGet() (rrs api.RenterRecoveryStatusGET, err error) {
	err = c.get("/renter/recoveryscan", &rrs)
	return
}

// RenterExpiredContractsGet requests the /renter/contracts resource with the
// expired flag set to true
func (c *Client) RenterExpiredContractsGet() (rc api.RenterContracts, err error) {
	values := url.Values{}
	values.Set("expired", fmt.Sprint(true))
	err = c.get("/renter/contracts?"+values.Encode(), &rc)
	return
}

// RenterRecoverableContractsGet requests the /renter/contracts resource with the
// recoverable flag set to true
func (c *Client) RenterRecoverableContractsGet() (rc api.RenterContracts, err error) {
	values := url.Values{}
	values.Set("recoverable", fmt.Sprint(true))
	err = c.get("/renter/contracts?"+values.Encode(), &rc)
	return
}

// RenterCancelDownloadPost requests the /renter/download/cancel endpoint to
// cancel an ongoing doing.
func (c *Client) RenterCancelDownloadPost(id modules.DownloadID) (err error) {
	values := url.Values{}
	values.Set("id", string(id))
	err = c.post("/renter/download/cancel", values.Encode(), nil)
	return
}

// RenterFileDeleteRootPost uses the /renter/delete endpoint to delete a file.
// It passes the `root=true` flag to indicate an absolute path.
func (c *Client) RenterFileDeleteRootPost(i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/delete/%s?root=true", sp), "", nil)
	return
}

// RenterFileDeletePost uses the /renter/delete endpoint to delete a file.
func (c *Client) RenterFileDeletePost(i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/delete/%s", sp), "", nil)
	return
}

// RenterDownloadGet uses the /renter/download endpoint to download a file to a
// destination on disk.
func (c *Client) RenterDownloadGet(i3vPath modules.I3vPath, destination string, offset, length uint64, async bool, disableLocalFetch bool) (modules.DownloadID, error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("destination", destination)
	values.Set("disablelocalfetch", fmt.Sprint(disableLocalFetch))
	values.Set("offset", fmt.Sprint(offset))
	values.Set("length", fmt.Sprint(length))
	values.Set("async", fmt.Sprint(async))
	h, _, err := c.getRawResponse(fmt.Sprintf("/renter/download/%s?%s", sp, values.Encode()))
	if err != nil {
		return "", err
	}
	return modules.DownloadID(h.Get("ID")), nil
}

// RenterDownloadInfoGet uses the /renter/downloadinfo endpoint to fetch
// information about a download from the history.
func (c *Client) RenterDownloadInfoGet(uid modules.DownloadID) (di api.DownloadInfo, err error) {
	err = c.get(fmt.Sprintf("/renter/downloadinfo/%s", uid), &di)
	return
}

// RenterBackups lists the backups the renter has uploaded to hosts.
func (c *Client) RenterBackups() (ubs api.RenterBackupsGET, err error) {
	err = c.get("/renter/backups", &ubs)
	return
}

// RenterBackupsOnHost lists the backups that the renter has uploaded to a
// specific host.
func (c *Client) RenterBackupsOnHost(host types.I3vPublicKey) (ubs api.RenterBackupsGET, err error) {
	values := url.Values{}
	values.Set("host", host.String())
	err = c.get("/renter/backups?"+values.Encode(), &ubs)
	return
}

// RenterCreateBackupPost creates a backup of the I3vFiles of the renter and
// uploads it to hosts.
func (c *Client) RenterCreateBackupPost(name string) (err error) {
	values := url.Values{}
	values.Set("name", name)
	err = c.post("/renter/backups/create", values.Encode(), nil)
	return
}

// RenterRecoverBackupPost downloads and restores the specified backup.
func (c *Client) RenterRecoverBackupPost(name string) (err error) {
	values := url.Values{}
	values.Set("name", name)
	err = c.post("/renter/backups/restore", values.Encode(), nil)
	return
}

// RenterCreateLocalBackupPost creates a local backup of the I3vFiles of the
// renter.
//
// Deprecated: Use RenterCreateBackupPost instead.
func (c *Client) RenterCreateLocalBackupPost(dst string) (err error) {
	values := url.Values{}
	values.Set("destination", dst)
	err = c.post("/renter/backup", values.Encode(), nil)
	return
}

// RenterRecoverLocalBackupPost restores the specified backup.
//
// Deprecated: Use RenterCreateBackupPost instead.
func (c *Client) RenterRecoverLocalBackupPost(src string) (err error) {
	values := url.Values{}
	values.Set("source", src)
	err = c.post("/renter/recoverbackup", values.Encode(), nil)
	return
}

// RenterDownloadFullGet uses the /renter/download endpoint to download a full
// file.
func (c *Client) RenterDownloadFullGet(i3vPath modules.I3vPath, destination string, async bool) (modules.DownloadID, error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("destination", destination)
	values.Set("httpresp", fmt.Sprint(false))
	values.Set("async", fmt.Sprint(async))
	h, _, err := c.getRawResponse(fmt.Sprintf("/renter/download/%s?%s", sp, values.Encode()))
	if err != nil {
		return "", err
	}
	return modules.DownloadID(h.Get("ID")), nil
}

// RenterClearAllDownloadsPost requests the /renter/downloads/clear resource
// with no parameters
func (c *Client) RenterClearAllDownloadsPost() (err error) {
	err = c.post("/renter/downloads/clear", "", nil)
	return
}

// RenterClearDownloadsAfterPost requests the /renter/downloads/clear resource
// with only the after timestamp provided
func (c *Client) RenterClearDownloadsAfterPost(after time.Time) (err error) {
	values := url.Values{}
	values.Set("after", strconv.FormatInt(after.UnixNano(), 10))
	err = c.post("/renter/downloads/clear", values.Encode(), nil)
	return
}

// RenterClearDownloadsBeforePost requests the /renter/downloads/clear resource
// with only the before timestamp provided
func (c *Client) RenterClearDownloadsBeforePost(before time.Time) (err error) {
	values := url.Values{}
	values.Set("before", strconv.FormatInt(before.UnixNano(), 10))
	err = c.post("/renter/downloads/clear", values.Encode(), nil)
	return
}

// RenterClearDownloadsRangePost requests the /renter/downloads/clear resource
// with both before and after timestamps provided
func (c *Client) RenterClearDownloadsRangePost(after, before time.Time) (err error) {
	values := url.Values{}
	values.Set("before", strconv.FormatInt(before.UnixNano(), 10))
	values.Set("after", strconv.FormatInt(after.UnixNano(), 10))
	err = c.post("/renter/downloads/clear", values.Encode(), nil)
	return
}

// RenterDownloadsGet requests the /renter/downloads resource
func (c *Client) RenterDownloadsGet() (rdq api.RenterDownloadQueue, err error) {
	err = c.get("/renter/downloads", &rdq)
	return
}

// RenterDownloadHTTPResponseGet uses the /renter/download endpoint to download
// a file and return its data.
func (c *Client) RenterDownloadHTTPResponseGet(i3vPath modules.I3vPath, offset, length uint64, disableLocalFetch bool) (modules.DownloadID, []byte, error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("offset", fmt.Sprint(offset))
	values.Set("length", fmt.Sprint(length))
	values.Set("httpresp", fmt.Sprint(true))
	values.Set("disablelocalfetch", fmt.Sprint(disableLocalFetch))
	h, resp, err := c.getRawResponse(fmt.Sprintf("/renter/download/%s?%s", sp, values.Encode()))
	if err != nil {
		return "", nil, err
	}
	return modules.DownloadID(h.Get("ID")), resp, nil
}

// RenterFileRootGet uses the /renter/file/:i3vpath endpoint to query a file.
// It passes the `root=true` flag to indicate an absolute path.
func (c *Client) RenterFileRootGet(i3vPath modules.I3vPath) (rf api.RenterFile, err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.get("/renter/file/"+sp+"?root=true", &rf)
	return
}

// RenterFileGet uses the /renter/file/:i3vpath endpoint to query a file.
func (c *Client) RenterFileGet(i3vPath modules.I3vPath) (rf api.RenterFile, err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.get("/renter/file/"+sp, &rf)
	return
}

// RenterFilesGet requests the /renter/files resource.
func (c *Client) RenterFilesGet(cached bool) (rf api.RenterFiles, err error) {
	err = c.get("/renter/files?cached="+fmt.Sprint(cached), &rf)
	return
}

// RenterGet requests the /renter resource.
func (c *Client) RenterGet() (rg api.RenterGET, err error) {
	err = c.get("/renter", &rg)
	return
}

// RenterPostAllowance uses the /renter endpoint to change the renter's allowance
func (c *Client) RenterPostAllowance(allowance modules.Allowance) error {
	a := c.RenterPostPartialAllowance()
	a = a.WithFunds(allowance.Funds)
	a = a.WithHosts(allowance.Hosts)
	a = a.WithPeriod(allowance.Period)
	a = a.WithRenewWindow(allowance.RenewWindow)
	a = a.WithExpectedStorage(allowance.ExpectedStorage)
	a = a.WithExpectedUpload(allowance.ExpectedUpload)
	a = a.WithExpectedDownload(allowance.ExpectedDownload)
	a = a.WithExpectedRedundancy(allowance.ExpectedRedundancy)
	a = a.WithMaxPeriodChurn(allowance.MaxPeriodChurn)
	return a.Send()
}

// RenterAllowanceCancelPost uses the /renter/allowance/cancel endpoint to cancel
// the allowance.
func (c *Client) RenterAllowanceCancelPost() (err error) {
	err = c.post("/renter/allowance/cancel", "", nil)
	return
}

// RenterPricesGet requests the /renter/prices endpoint's resources.
func (c *Client) RenterPricesGet(allowance modules.Allowance) (rpg api.RenterPricesGET, err error) {
	query := fmt.Sprintf("?funds=%v&hosts=%v&period=%v&renewwindow=%v",
		allowance.Funds, allowance.Hosts, allowance.Period, allowance.RenewWindow)
	err = c.get("/renter/prices"+query, &rpg)
	return
}

// RenterRateLimitPost uses the /renter endpoint to change the renter's bandwidth rate
// limit.
func (c *Client) RenterRateLimitPost(readBPS, writeBPS int64) (err error) {
	values := url.Values{}
	values.Set("maxdownloadspeed", strconv.FormatInt(readBPS, 10))
	values.Set("maxuploadspeed", strconv.FormatInt(writeBPS, 10))
	err = c.post("/renter", values.Encode(), nil)
	return
}

// RenterRenamePost uses the /renter/rename/:i3vpath endpoint to rename a file.
func (c *Client) RenterRenamePost(i3vPathOld, i3vPathNew modules.I3vPath) (err error) {
	spo := escapeI3vPath(i3vPathOld)
	values := url.Values{}
	values.Set("newi3vpath", fmt.Sprintf("/%s", i3vPathNew.String()))
	err = c.post(fmt.Sprintf("/renter/rename/%s", spo), values.Encode(), nil)
	return
}

// RenterSetStreamCacheSizePost uses the /renter endpoint to change the renter's
// streamCacheSize for streaming
func (c *Client) RenterSetStreamCacheSizePost(cacheSize uint64) (err error) {
	values := url.Values{}
	values.Set("streamcachesize", fmt.Sprint(cacheSize))
	err = c.post("/renter", values.Encode(), nil)
	return
}

// RenterSetCheckIPViolationPost uses the /renter endpoint to enable/disable the IP
// violation check in the renter.
func (c *Client) RenterSetCheckIPViolationPost(enabled bool) (err error) {
	values := url.Values{}
	values.Set("checkforipviolation", fmt.Sprint(enabled))
	err = c.post("/renter", values.Encode(), nil)
	return
}

// RenterStreamGet uses the /renter/stream endpoint to download data as a
// stream.
func (c *Client) RenterStreamGet(i3vPath modules.I3vPath, disableLocalFetch bool) (resp []byte, err error) {
	values := url.Values{}
	values.Set("disablelocalfetch", fmt.Sprint(disableLocalFetch))
	sp := escapeI3vPath(i3vPath)
	_, resp, err = c.getRawResponse(fmt.Sprintf("/renter/stream/%s?%s", sp, values.Encode()))
	return
}

// RenterStreamPartialGet uses the /renter/stream endpoint to download a part
// of data as a stream.
func (c *Client) RenterStreamPartialGet(i3vPath modules.I3vPath, start, end uint64, disableLocalFetch bool) (resp []byte, err error) {
	values := url.Values{}
	values.Set("disablelocalfetch", fmt.Sprint(disableLocalFetch))
	sp := escapeI3vPath(i3vPath)
	resp, err = c.getRawPartialResponse(fmt.Sprintf("/renter/stream/%s?%s", sp, values.Encode()), start, end)
	return
}

// RenterSetRepairPathPost uses the /renter/tracking endpoint to set the repair
// path of a file to a new location. The file at newPath must exists.
func (c *Client) RenterSetRepairPathPost(i3vPath modules.I3vPath, newPath string) (err error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("trackingpath", newPath)
	err = c.post(fmt.Sprintf("/renter/file/%v", sp), values.Encode(), nil)
	return
}

// RenterSetFileStuckPost sets the 'stuck' field of the i3vfile at i3vPath to
// stuck.
func (c *Client) RenterSetFileStuckPost(i3vPath modules.I3vPath, stuck bool) (err error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("stuck", fmt.Sprint(stuck))
	err = c.post(fmt.Sprintf("/renter/file/%v", sp), values.Encode(), nil)
	return
}

// RenterUploadPost uses the /renter/upload endpoint to upload a file
func (c *Client) RenterUploadPost(path string, i3vPath modules.I3vPath, dataPieces, parityPieces uint64) (err error) {
	return c.RenterUploadForcePost(path, i3vPath, dataPieces, parityPieces, false)
}

// RenterUploadForcePost uses the /renter/upload endpoint to upload a file
// and to overwrite if the file already exists
func (c *Client) RenterUploadForcePost(path string, i3vPath modules.I3vPath, dataPieces, parityPieces uint64, force bool) (err error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("source", path)
	values.Set("datapieces", strconv.FormatUint(dataPieces, 10))
	values.Set("paritypieces", strconv.FormatUint(parityPieces, 10))
	values.Set("force", strconv.FormatBool(force))
	err = c.post(fmt.Sprintf("/renter/upload/%s", sp), values.Encode(), nil)
	return
}

// RenterUploadDefaultPost uses the /renter/upload endpoint with default
// redundancy settings to upload a file.
func (c *Client) RenterUploadDefaultPost(path string, i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("source", path)
	err = c.post(fmt.Sprintf("/renter/upload/%s", sp), values.Encode(), nil)
	return
}

// RenterUploadStreamPost uploads data using a stream.
func (c *Client) RenterUploadStreamPost(r io.Reader, i3vPath modules.I3vPath, dataPieces, parityPieces uint64, force bool) error {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("datapieces", strconv.FormatUint(dataPieces, 10))
	values.Set("paritypieces", strconv.FormatUint(parityPieces, 10))
	values.Set("force", strconv.FormatBool(force))
	values.Set("stream", strconv.FormatBool(true))
	_, _, err := c.postRawResponse(fmt.Sprintf("/renter/uploadstream/%s?%s", sp, values.Encode()), r)
	return err
}

// RenterUploadStreamRepairPost a i3vfile using a stream. If the data provided
// by r is not the same as the previously uploaded data, the data will be
// corrupted.
func (c *Client) RenterUploadStreamRepairPost(r io.Reader, i3vPath modules.I3vPath) error {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("repair", strconv.FormatBool(true))
	values.Set("stream", strconv.FormatBool(true))
	_, _, err := c.postRawResponse(fmt.Sprintf("/renter/uploadstream/%s?%s", sp, values.Encode()), r)
	return err
}

// RenterDirCreatePost uses the /renter/dir/ endpoint to create a directory for the
// renter
func (c *Client) RenterDirCreatePost(i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/dir/%s", sp), "action=create", nil)
	return
}

// RenterDirCreateWithModePost uses the /renter/dir/ endpoint to create a
// directory for the renter with the specified permissions.
func (c *Client) RenterDirCreateWithModePost(i3vPath modules.I3vPath, mode os.FileMode) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/dir/%s?mode=%d", sp, mode), "action=create", nil)
	return
}

// RenterDirDeleteRootPost uses the /renter/dir/ endpoint to delete a directory
// for the renter. It passes the `root=true` flag to indicate an absolute path.
func (c *Client) RenterDirDeleteRootPost(i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/dir/%s?root=true", sp), "action=delete", nil)
	return
}

// RenterDirDeletePost uses the /renter/dir/ endpoint to delete a directory
// for the renter
func (c *Client) RenterDirDeletePost(i3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.post(fmt.Sprintf("/renter/dir/%s", sp), "action=delete", nil)
	return
}

// RenterDirRenamePost uses the /renter/dir/ endpoint to rename a directory for the
// renter
func (c *Client) RenterDirRenamePost(i3vPath, newI3vPath modules.I3vPath) (err error) {
	sp := escapeI3vPath(i3vPath)
	nsp := escapeI3vPath(newI3vPath)
	err = c.post(fmt.Sprintf("/renter/dir/%s?newi3vpath=%s", sp, nsp), "action=rename", nil)
	return
}

// RenterDirRootGet uses the /renter/dir/ endpoint to query a directory,
// starting from the root path.
func (c *Client) RenterDirRootGet(i3vPath modules.I3vPath) (rd api.RenterDirectory, err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.get(fmt.Sprintf("/renter/dir/%s?root=true", sp), &rd)
	return
}

// RenterDirGet uses the /renter/dir/ endpoint to query a directory
func (c *Client) RenterDirGet(i3vPath modules.I3vPath) (rd api.RenterDirectory, err error) {
	sp := escapeI3vPath(i3vPath)
	err = c.get(fmt.Sprintf("/renter/dir/%s", sp), &rd)
	return
}

// RenterValidateI3vPathPost uses the /renter/validatei3vpath endpoint to
// validate a potential i3vpath
//
// NOTE: This function specifically takes a string as an argument not a type
// I3vPath
func (c *Client) RenterValidateI3vPathPost(i3vPathStr string) (err error) {
	err = c.post(fmt.Sprintf("/renter/validatei3vpath/%s", i3vPathStr), "", nil)
	return
}

// RenterUploadReadyGet uses the /renter/uploadready endpoint to determine if
// the renter is ready for upload.
func (c *Client) RenterUploadReadyGet(dataPieces, parityPieces uint64) (rur api.RenterUploadReadyGet, err error) {
	strDataPieces := strconv.FormatUint(dataPieces, 10)
	strParityPieces := strconv.FormatUint(parityPieces, 10)
	query := fmt.Sprintf("?datapieces=%v&paritypieces=%v",
		strDataPieces, strParityPieces)
	err = c.get("/renter/uploadready"+query, &rur)
	return
}

// RenterUploadReadyDefaultGet uses the /renter/uploadready endpoint to
// determine if the renter is ready for upload.
func (c *Client) RenterUploadReadyDefaultGet() (rur api.RenterUploadReadyGet, err error) {
	err = c.get("/renter/uploadready", &rur)
	return
}

// RenterFuse uses the /renter/fuse endpoint to return information about the
// current fuse mount point.
func (c *Client) RenterFuse() (fi api.RenterFuseInfo, err error) {
	err = c.get("/renter/fuse", &fi)
	return
}

// RenterFuseMount uses the /renter/fuse/mount endpoint to mount a fuse
// filesystem serving the provided i3vpath.
func (c *Client) RenterFuseMount(mount string, i3vPath modules.I3vPath, opts modules.MountOptions) (err error) {
	sp := escapeI3vPath(i3vPath)
	values := url.Values{}
	values.Set("i3vpath", sp)
	values.Set("mount", mount)
	values.Set("readonly", strconv.FormatBool(opts.ReadOnly))
	values.Set("allowother", strconv.FormatBool(opts.AllowOther))
	err = c.post("/renter/fuse/mount", values.Encode(), nil)
	return
}

// RenterFuseUnmount uses the /renter/fuse/unmount endpoint to unmount the
// currently-mounted fuse filesystem.
func (c *Client) RenterFuseUnmount(mount string) (err error) {
	values := url.Values{}
	values.Set("mount", mount)
	err = c.post("/renter/fuse/unmount", values.Encode(), nil)
	return
}

// RenterUploadsPausePost uses the /renter/uploads/pause endpoint to pause the
// renter's uploads and repairs
func (c *Client) RenterUploadsPausePost(duration time.Duration) (err error) {
	values := url.Values{}
	values.Set("duration", fmt.Sprint(uint64(math.Round(duration.Seconds()))))
	err = c.post("/renter/uploads/pause", values.Encode(), nil)
	return
}

// RenterUploadsResumePost uses the /renter/uploads/resume endpoint to resume
// the renter's uploads and repairs
func (c *Client) RenterUploadsResumePost() (err error) {
	err = c.post("/renter/uploads/resume", "", nil)
	return
}

// RenterPost uses the /renter POST endpoint to set fields of the renter. Values
// are encoded as a query string in the body
func (c *Client) RenterPost(values url.Values) (err error) {
	err = c.post("/renter", values.Encode(), nil)
	return
}

// SkynetSkylinkGet uses the /skynet/skylink endpoint to download a skylink
// file.
func (c *Client) SkynetSkylinkGet(skylink string) ([]byte, modules.SkyfileMetadata, error) {
	getQuery := fmt.Sprintf("/skynet/skylink/%s", skylink)
	header, fileData, err := c.getRawResponse(getQuery)
	if err != nil {
		return nil, modules.SkyfileMetadata{}, errors.AddContext(err, "error fetching api response")
	}

	var sm modules.SkyfileMetadata
	strMetadata := header.Get("Skynet-File-Metadata")
	if strMetadata != "" {
		err = json.Unmarshal([]byte(strMetadata), &sm)
		if err != nil {
			return nil, modules.SkyfileMetadata{}, errors.AddContext(err, "unable to unmarshal skyfile metadata")
		}
	}
	return fileData, sm, errors.AddContext(err, "unable to fetch skylink data")
}

// SkynetSkylinkReaderGet uses the /skynet/skylink endpoint to fetch a reader of
// the file data.
func (c *Client) SkynetSkylinkReaderGet(skylink string) (io.ReadCloser, error) {
	getQuery := fmt.Sprintf("/skynet/skylink/%s", skylink)
	_, reader, err := c.getReaderResponse(getQuery)
	return reader, errors.AddContext(err, "unable to fetch skylink data")
}

// SkynetSkylinkPinPost uses the /skynet/pin endpoint to pin the file at the
// given skylink.
func (c *Client) SkynetSkylinkPinPost(skylink string, params modules.SkyfilePinParameters) error {

	// Set the url values.
	values := url.Values{}
	forceStr := fmt.Sprintf("%t", params.Force)
	values.Set("force", forceStr)
	redundancyStr := fmt.Sprintf("%v", params.BaseChunkRedundancy)
	values.Set("basechunkredundancy", redundancyStr)
	rootStr := fmt.Sprintf("%t", params.Root)
	values.Set("root", rootStr)
	values.Set("i3vpath", params.I3vPath.String())

	query := fmt.Sprintf("/skynet/pin/%s?%s", skylink, values.Encode())
	_, _, err := c.postRawResponse(query, nil)
	if err != nil {
		return errors.AddContext(err, "post call to "+query+" failed")
	}
	return nil
}

// SkynetSkyfilePost uses the /skynet/skyfile endpoint to upload a skyfile.  The
// resulting skylink is returned along with an error.
func (c *Client) SkynetSkyfilePost(params modules.SkyfileUploadParameters) (string, api.SkynetSkyfileHandlerPOST, error) {
	// Set the url values.
	values := url.Values{}
	values.Set("filename", params.FileMetadata.Filename)
	forceStr := fmt.Sprintf("%t", params.Force)
	values.Set("force", forceStr)
	modeStr := fmt.Sprintf("%o", params.FileMetadata.Mode)
	values.Set("mode", modeStr)
	redundancyStr := fmt.Sprintf("%v", params.BaseChunkRedundancy)
	values.Set("basechunkredundancy", redundancyStr)
	rootStr := fmt.Sprintf("%t", params.Root)
	values.Set("root", rootStr)

	// Make the call to upload the file.
	query := fmt.Sprintf("/skynet/skyfile/%s?%s", params.I3vPath.String(), values.Encode())
	_, resp, err := c.postRawResponse(query, params.Reader)
	if err != nil {
		return "", api.SkynetSkyfileHandlerPOST{}, errors.AddContext(err, "post call to "+query+" failed")
	}

	// Parse the response to get the skylink.
	var rshp api.SkynetSkyfileHandlerPOST
	err = json.Unmarshal(resp, &rshp)
	if err != nil {
		return "", api.SkynetSkyfileHandlerPOST{}, errors.AddContext(err, "unable to parse the skylink upload response")
	}
	return rshp.Skylink, rshp, err
}

// SkynetSkyfileMultiPartPost uses the /skynet/skyfile endpoint to upload a
// skyfile using multipart form data.  The resulting skylink is returned along
// with an error.
func (c *Client) SkynetSkyfileMultiPartPost(params modules.SkyfileMultipartUploadParameters) (string, api.SkynetSkyfileHandlerPOST, error) {
	// Set the url values.
	values := url.Values{}
	values.Set("filename", params.Filename)
	forceStr := fmt.Sprintf("%t", params.Force)
	values.Set("force", forceStr)
	redundancyStr := fmt.Sprintf("%v", params.BaseChunkRedundancy)
	values.Set("basechunkredundancy", redundancyStr)
	rootStr := fmt.Sprintf("%t", params.Root)
	values.Set("root", rootStr)

	// Make the call to upload the file.
	query := fmt.Sprintf("/skynet/skyfile/%s?%s", params.I3vPath.String(), values.Encode())

	headers := map[string]string{"Content-Type": params.ContentType}
	_, resp, err := c.postRawResponseWithHeaders(query, params.Reader, headers)
	if err != nil {
		return "", api.SkynetSkyfileHandlerPOST{}, errors.AddContext(err, "post call to "+query+" failed")
	}

	// Parse the response to get the skylink.
	var rshp api.SkynetSkyfileHandlerPOST
	err = json.Unmarshal(resp, &rshp)
	if err != nil {
		return "", api.SkynetSkyfileHandlerPOST{}, errors.AddContext(err, "unable to parse the skylink upload response")
	}
	return rshp.Skylink, rshp, err
}

// SkynetConvertI3vfileToSkyfilePost uses the /skynet/skyfile endpoint to
// convert an existing i3vfile to a skyfile. The input I3vPath 'convert' is the
// i3vpath of the i3vfile that should be converted. The i3vpath provided inside
// of the upload params is the name that will be used for the base sector of the
// skyfile.
func (c *Client) SkynetConvertI3vfileToSkyfilePost(lup modules.SkyfileUploadParameters, convert modules.I3vPath) (string, error) {
	// Set the url values.
	values := url.Values{}
	values.Set("filename", lup.FileMetadata.Filename)
	forceStr := fmt.Sprintf("%t", lup.Force)
	values.Set("force", forceStr)
	modeStr := fmt.Sprintf("%o", lup.FileMetadata.Mode)
	values.Set("mode", modeStr)
	redundancyStr := fmt.Sprintf("%v", lup.BaseChunkRedundancy)
	values.Set("redundancy", redundancyStr)
	values.Set("convertpath", convert.String())

	// Make the call to upload the file.
	query := fmt.Sprintf("/skynet/skyfile/%s?%s", lup.I3vPath.String(), values.Encode())
	_, resp, err := c.postRawResponse(query, lup.Reader)
	if err != nil {
		return "", errors.AddContext(err, "post call to "+query+" failed")
	}

	// Parse the response to get the skylink.
	var rshp api.SkynetSkyfileHandlerPOST
	err = json.Unmarshal(resp, &rshp)
	if err != nil {
		return "", errors.AddContext(err, "unable to parse the skylink upload response")
	}
	return rshp.Skylink, err
}

// SkynetBlacklistGet requests the /skynet/blacklist Get endpoint
func (c *Client) SkynetBlacklistGet() (blacklist api.SkynetBlacklistGET, err error) {
	err = c.get("/skynet/blacklist", &blacklist)
	return
}

// SkynetBlacklistPost requests the /skynet/blacklist Post endpoint
func (c *Client) SkynetBlacklistPost(additions, removals []string) (err error) {
	sbp := api.SkynetBlacklistPOST{
		Add:    additions,
		Remove: removals,
	}
	data, err := json.Marshal(sbp)
	if err != nil {
		return err
	}
	err = c.post("/skynet/blacklist", string(data), nil)
	return
}
