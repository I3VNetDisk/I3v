I3vc Usage
==========

`i3vc` is the command line interface to I3v, for use by power users and
those on headless servers. It comes as a part of the command line
package, and can be run as `./i3vc` from the same folder, or just by
calling `i3vc` if you move the binary into your path.

Most of the following commands have online help. For example, executing
`i3vc wallet send help` will list the arguments for that command,
while `i3vc host help` will list the commands that can be called
pertaining to hosting. `i3vc help` will list all of the top level
command groups that can be used.

You can change the address of where i3vd is pointing using the `-a`
flag. For example, `i3vc -a :9000 status` will display the status of
the i3vd instance launched on the local machine with `i3vd -a :9000`.

Common tasks
------------
* `i3vc consensus` view block height

Wallet:
* `i3vc wallet init [-p]` initialize a wallet
* `i3vc wallet unlock` unlock a wallet
* `i3vc wallet balance` retrieve wallet balance
* `i3vc wallet address` get a wallet address
* `i3vc wallet send [amount] [dest]` sends i3vcoin to an address

Renter:
* `i3vc renter ls` list all renter files
* `i3vc renter upload [filepath] [nickname]` upload a file
* `i3vc renter download [nickname] [filepath]` download a file


Full Descriptions
-----------------

#### Wallet tasks

* `i3vc wallet init [-p]` encrypts and initializes the wallet. If the
`-p` flag is provided, an encryption password is requested from the
user. Otherwise the initial seed is used as the encryption
password. The wallet must be initialized and unlocked before any
actions can be performed on the wallet.

Examples:
```bash
user@hostname:~$ i3vc -a :9920 wallet init
Seed is:
 cider sailor incur sober feast unhappy mundane sadness hinder aglow imitate amaze duties arrow gigantic uttered inflamed girth myriad jittery hexagon nail lush reef sushi pastry southern inkling acquire

Wallet encrypted with password: cider sailor incur sober feast unhappy mundane sadness hinder aglow imitate amaze duties arrow gigantic uttered inflamed girth myriad jittery hexagon nail lush reef sushi pastry southern inkling acquire
```

```bash
user@hostname:~$ i3vc -a :9920 wallet init -p
Wallet password:
Seed is:
 potato haunted fuming lordship library vane fever powder zippers fabrics dexterity hoisting emails pebbles each vampire rockets irony summon sailor lemon vipers foxes oneself glide cylinder vehicle mews acoustic

Wallet encrypted with given password
```

* `i3vc wallet unlock` prompts the user for the encryption password
to the wallet, supplied by the `init` command. The wallet must be
initialized and unlocked before any actions can take place.

* `i3vc wallet balance` prints information about your wallet.

Example:
```bash
user@hostname:~$ i3vc wallet balance
Wallet status:
Encrypted, Unlocked
Confirmed Balance:   61516458.00 SC
Unconfirmed Balance: 64516461.00 SC
Exact:               61516457999999999999999999999999 H
```

* `i3vc wallet address` returns a never seen before address for sending
i3vcoins to.

* `i3vc wallet send [amount] [dest]` Sends `amount` i3vcoins to
`dest`. `amount` is in the form XXXXUU where an X is a number and U is
a unit, for example MS, S, mS, ps, etc. If no unit is given hastings
is assumed. `dest` must be a valid i3vcoin address.

* `i3vc wallet lock` locks a wallet. After calling, the wallet must be unlocked
using the encryption password in order to use it further

* `i3vc wallet seeds` returns the list of secret seeds in use by the
wallet. These can be used to regenerate the wallet

* `i3vc wallet addseed` prompts the user for his encryption password,
as well as a new secret seed. The wallet will then incorporate this
seed into itself. This can be used for wallet recovery and merging.

#### Host tasks
* `host config [setting] [value]`

is used to configure hosting.

In version `1.4.3.0`, i3v hosting is configured as follows:

| Setting                    | Value                                           |
| ---------------------------|-------------------------------------------------|
| acceptingcontracts         | Yes or No                                       |
| collateral                 | in SC / TB / Month, 10-1000                     |
| collateralbudget           | in SC                                           |
| ephemeralaccountexpiry     | in seconds                                      |
| maxcollateral              | in SC, max per contract                         |
| maxduration                | in weeks, at least 12                           |
| maxephemeralaccountbalance | in SC                                           |
| maxephemeralaccountrisk    | in SC                                           |
| mincontractprice           | minimum price in SC per contract                |
| mindownloadbandwidthprice  | in SC / TB                                      |
| minstorageprice            | in SC / TB                                      |
| minuploadbandwidthprice    | in SC / TB                                      |

You can call this many times to configure you host before
announcing. Alternatively, you can manually adjust these parameters
inside the `host/config.json` file.

* `i3vc host announce` makes an host announcement. You may optionally
supply a specific address to be announced; this allows you to announce a domain
name. Announcing a second time after changing settings is not necessary, as the
announcement only contains enough information to reach your host.

* `i3vc host -v` outputs some of your hosting settings.

Example:
```bash
user@hostname:~$ i3vc host -v
Host settings:
Storage:      2.0000 TB (1.524 GB used)
Price:        0.000 SC per GB per month
Collateral:   0
Max Filesize: 10000000000
Max Duration: 8640
Contracts:    32
```

* `i3vc hostdb -v` prints a list of all the know active hosts on the
network.

#### Renter tasks
* `i3vc renter upload [filename] [nickname]` uploads a file to the i3v
network. `filename` is the path to the file you want to upload, and
nickname is what you will use to refer to that file in the
network. For example, it is common to have the nickname be the same as
the filename.

* `i3vc renter ls` displays a list of the your uploaded files
currently on the i3v network by nickname, and their filesizes.

* `i3vc renter download [nickname] [destination]` downloads a file
from the i3v network onto your computer. `nickname` is the name used
to refer to your file in the i3v network, and `destination` is the
path to where the file will be. If a file already exists there, it
will be overwritten.

* `i3vc renter rename [nickname] [newname]` changes the nickname of a
  file.

* `i3vc renter delete [nickname]` removes a file from your list of
stored files. This does not remove it from the network, but only from
your saved list.

* `i3vc renter queue` shows the download queue. This is only relevant
if you have multiple downloads happening simultaneously.

#### Skynet tasks
* `i3vc skynet upload [source filepath] [destination i3vpath]` uploads a file to
  Skynet. A skylink will be produced which can be shared and used to retrieve
  the file. The file that gets uploaded will be pinned to this I3v node, meaning
  that this node will pay for storage and repairs until the file is manually
  deleted.

* `i3vc skynet ls` lists all skyfiles that the user has pinned along with the
  corresponding skylinks. By default, only files in var/skynet/ will be
  displayed.

* `i3vc skynet download [skylink] [destination]` downloads a file from Skynet
  using a skylink.

* `i3vc skynet pin [skylink] [destination i3vpath]` pins the file associated
  with this skylink by re-uploading an exact copy. This ensures that the file
  will still be available on skynet as long as you continue maintaining the file
  in your renter.

* `i3vc skynet unpin [i3vpath]` unpins a skyfile, deleting it from your list of
  stored files.

* `i3vc skynet convert [source i3vPath] [destination i3vPath]` converts a
  i3vfile to a skyfile and then generates its skylink. A new skylink will be
  created in the user's skyfile directory. The skyfile and the original i3vfile
  are both necessary to pin the file and keep the skylink active. The skyfile
  will consume an additional 40 MiB of storage.

* `i3vc skynet blacklist [skylink]` will add or remove a skylink from the
  Renter's Skynet Blacklist

#### Gateway tasks
* `i3vc gateway` prints info about the gateway, including its address and how
many peers it's connected to.

* `i3vc gateway list` prints a list of all currently connected peers.

* `i3vc gateway connect [address:port]` manually connects to a peer and adds it
to the gateway's node list.

* `i3vc gateway disconnect [address:port]` manually disconnects from a peer, but
leaves it in the gateway's node list.

#### Miner tasks
* `i3vc miner status` returns information about the miner. It is only
valid for when i3vd is running.

* `i3vc miner start` starts running the CPU miner on one thread. This
is virtually useless outside of debugging.

* `i3vc miner stop` halts the CPU miner.

#### General commands
* `i3vc consensus` prints the current block ID, current block height, and
current target.

* `i3vc stop` sends the stop signal to i3vd to safely terminate. This
has the same affect as C^c on the terminal.

* `i3vc version` displays the version string of i3vc.

* `i3vc update` checks the server for updates.
