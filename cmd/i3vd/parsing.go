package main

import (
	"strings"

	"gitlab.com/I3VNetDisk/I3v/node"
)

// createNodeParams parses the provided config and creates the corresponding
// node params for the server.
func parseModules(config Config) node.NodeParams {
	params := node.NodeParams{}
	// Parse the modules.
	if strings.Contains(config.I3vd.Modules, "g") {
		params.CreateGateway = true
	}
	if strings.Contains(config.I3vd.Modules, "c") {
		params.CreateConsensusSet = true
	}
	if strings.Contains(config.I3vd.Modules, "e") {
		params.CreateExplorer = true
	}
	if strings.Contains(config.I3vd.Modules, "t") {
		params.CreateTransactionPool = true
	}
	if strings.Contains(config.I3vd.Modules, "w") {
		params.CreateWallet = true
	}
	if strings.Contains(config.I3vd.Modules, "m") {
		params.CreateMiner = true
	}
	if strings.Contains(config.I3vd.Modules, "h") {
		params.CreateHost = true
	}
	if strings.Contains(config.I3vd.Modules, "r") {
		params.CreateRenter = true
	}
	// Parse remaining fields.
	params.Bootstrap = !config.I3vd.NoBootstrap
	params.HostAddress = config.I3vd.HostAddr
	params.RPCAddress = config.I3vd.RPCaddr
	params.I3vMuxAddress = config.I3vd.I3vMuxAddr
	params.Dir = config.I3vd.I3vDir
	return params
}
