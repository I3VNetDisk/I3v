package types

import (
	"testing"

	"gitlab.com/I3VNetDisk/I3v/crypto"
)

// TestTransactionIDs probes all of the ID functions of the Transaction type.
func TestIDs(t *testing.T) {
	// Create every type of ID using empty fields.
	txn := Transaction{
		I3vcoinOutputs: []I3vcoinOutput{{}},
		FileContracts:  []FileContract{{}},
		I3vfundOutputs: []I3vfundOutput{{}},
	}
	tid := txn.ID()
	scoid := txn.I3vcoinOutputID(0)
	fcid := txn.FileContractID(0)
	spidT := fcid.StorageProofOutputID(ProofValid, 0)
	spidF := fcid.StorageProofOutputID(ProofMissed, 0)
	sfoid := txn.I3vfundOutputID(0)
	scloid := sfoid.I3vClaimOutputID()

	// Put all of the ids into a slice.
	var ids []crypto.Hash
	ids = append(ids,
		crypto.Hash(tid),
		crypto.Hash(scoid),
		crypto.Hash(fcid),
		crypto.Hash(spidT),
		crypto.Hash(spidF),
		crypto.Hash(sfoid),
		crypto.Hash(scloid),
	)

	// Check that each id is unique.
	knownIDs := make(map[crypto.Hash]struct{})
	for i, id := range ids {
		_, exists := knownIDs[id]
		if exists {
			t.Error("id repeat for index", i)
		}
		knownIDs[id] = struct{}{}
	}
}

// TestTransactionI3vcoinOutputSum probes the I3vcoinOutputSum method of the
// Transaction type.
func TestTransactionI3vcoinOutputSum(t *testing.T) {
	// Create a transaction with all types of i3vcoin outputs.
	txn := Transaction{
		I3vcoinOutputs: []I3vcoinOutput{
			{Value: NewCurrency64(1)},
			{Value: NewCurrency64(20)},
		},
		FileContracts: []FileContract{
			{Payout: NewCurrency64(300)},
			{Payout: NewCurrency64(4000)},
		},
		MinerFees: []Currency{
			NewCurrency64(50000),
			NewCurrency64(600000),
		},
	}
	if txn.I3vcoinOutputSum().Cmp(NewCurrency64(654321)) != 0 {
		t.Error("wrong i3vcoin output sum was calculated, got:", txn.I3vcoinOutputSum())
	}
}
