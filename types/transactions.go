package types

// transaction.go defines the transaction type and all of the sub-fields of the
// transaction, as well as providing helper functions for working with
// transactions. The various IDs are designed such that, in a legal blockchain,
// it is cryptographically unlikely that any two objects would share an id.

import (
	"errors"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/encoding"
)

const (
	// UnlockHashChecksumSize is the size of the checksum used to verify
	// human-readable addresses. It is not a crypytographically secure
	// checksum, it's merely intended to prevent typos. 6 is chosen because it
	// brings the total size of the address to 38 bytes, leaving 2 bytes for
	// potential version additions in the future.
	UnlockHashChecksumSize = 6
)

// These Specifiers are used internally when calculating a type's ID. See
// Specifier for more details.
var (
	ErrTransactionIDWrongLen = errors.New("input has wrong length to be an encoded transaction id")

	SpecifierClaimOutput          = NewSpecifier("claim output")
	SpecifierFileContract         = NewSpecifier("file contract")
	SpecifierFileContractRevision = NewSpecifier("file contract re")
	SpecifierMinerFee             = NewSpecifier("miner fee")
	SpecifierMinerPayout          = NewSpecifier("miner payout")
	SpecifierI3vcoinInput         = NewSpecifier("i3vcoin input")
	SpecifierI3vcoinOutput        = NewSpecifier("i3vcoin output")
	SpecifierI3vfundInput         = NewSpecifier("i3vfund input")
	SpecifierI3vfundOutput        = NewSpecifier("i3vfund output")
	SpecifierStorageProofOutput   = NewSpecifier("storage proof")
)

type (
	// IDs are used to refer to a type without revealing its contents. They
	// are constructed by hashing specific fields of the type, along with a
	// Specifier. While all of these types are hashes, defining type aliases
	// gives us type safety and makes the code more readable.

	// TransactionID uniquely identifies a transaction
	TransactionID crypto.Hash
	// I3vcoinOutputID uniquely identifies a i3vcoin output
	I3vcoinOutputID crypto.Hash
	// I3vfundOutputID uniquely identifies a i3vfund output
	I3vfundOutputID crypto.Hash
	// FileContractID uniquely identifies a file contract
	FileContractID crypto.Hash
	// OutputID uniquely identifies an output
	OutputID crypto.Hash

	// A Transaction is an atomic component of a block. Transactions can contain
	// inputs and outputs, file contracts, storage proofs, and even arbitrary
	// data. They can also contain signatures to prove that a given party has
	// approved the transaction, or at least a particular subset of it.
	//
	// Transactions can depend on other previous transactions in the same block,
	// but transactions cannot spend outputs that they create or otherwise be
	// self-dependent.
	Transaction struct {
		I3vcoinInputs         []I3vcoinInput         `json:"i3vcoininputs"`
		I3vcoinOutputs        []I3vcoinOutput        `json:"i3vcoinoutputs"`
		FileContracts         []FileContract         `json:"filecontracts"`
		FileContractRevisions []FileContractRevision `json:"filecontractrevisions"`
		StorageProofs         []StorageProof         `json:"storageproofs"`
		I3vfundInputs         []I3vfundInput         `json:"i3vfundinputs"`
		I3vfundOutputs        []I3vfundOutput        `json:"i3vfundoutputs"`
		MinerFees             []Currency             `json:"minerfees"`
		ArbitraryData         [][]byte               `json:"arbitrarydata"`
		TransactionSignatures []TransactionSignature `json:"transactionsignatures"`
	}

	// A I3vcoinInput consumes a I3vcoinOutput and adds the i3vcoins to the set of
	// i3vcoins that can be spent in the transaction. The ParentID points to the
	// output that is getting consumed, and the UnlockConditions contain the rules
	// for spending the output. The UnlockConditions must match the UnlockHash of
	// the output.
	I3vcoinInput struct {
		ParentID         I3vcoinOutputID  `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
	}

	// A I3vcoinOutput holds a volume of i3vcoins. Outputs must be spent
	// atomically; that is, they must all be spent in the same transaction. The
	// UnlockHash is the hash of the UnlockConditions that must be fulfilled
	// in order to spend the output.
	I3vcoinOutput struct {
		Value      Currency   `json:"value"`
		UnlockHash UnlockHash `json:"unlockhash"`
	}

	// A I3vfundInput consumes a I3vfundOutput and adds the i3vfunds to the set of
	// i3vfunds that can be spent in the transaction. The ParentID points to the
	// output that is getting consumed, and the UnlockConditions contain the rules
	// for spending the output. The UnlockConditions must match the UnlockHash of
	// the output.
	I3vfundInput struct {
		ParentID         I3vfundOutputID  `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
		ClaimUnlockHash  UnlockHash       `json:"claimunlockhash"`
	}

	// A I3vfundOutput holds a volume of i3vfunds. Outputs must be spent
	// atomically; that is, they must all be spent in the same transaction. The
	// UnlockHash is the hash of a set of UnlockConditions that must be fulfilled
	// in order to spend the output.
	//
	// When the I3vfundOutput is spent, a I3vcoinOutput is created, where:
	//
	//     I3vcoinOutput.Value := (I3vfundPool - ClaimStart) / 10,000 * Value
	//     I3vcoinOutput.UnlockHash := I3vfundInput.ClaimUnlockHash
	//
	// When a I3vfundOutput is put into a transaction, the ClaimStart must always
	// equal zero. While the transaction is being processed, the ClaimStart is set
	// to the value of the I3vfundPool.
	I3vfundOutput struct {
		Value      Currency   `json:"value"`
		UnlockHash UnlockHash `json:"unlockhash"`
		ClaimStart Currency   `json:"claimstart"`
	}

	// An UnlockHash is a specially constructed hash of the UnlockConditions type.
	// "Locked" values can be unlocked by providing the UnlockConditions that hash
	// to a given UnlockHash. See UnlockConditions.UnlockHash for details on how the
	// UnlockHash is constructed.
	UnlockHash crypto.Hash
)

// ID returns the id of a transaction, which is taken by marshalling all of the
// fields except for the signatures and taking the hash of the result.
func (t Transaction) ID() TransactionID {
	// Get the transaction id by hashing all data minus the signatures.
	var txid TransactionID
	h := crypto.NewHash()
	t.marshalI3vNoSignatures(h)
	h.Sum(txid[:0])
	return txid
}

// I3vcoinOutputID returns the ID of a i3vcoin output at the given index,
// which is calculated by hashing the concatenation of the I3vcoinOutput
// Specifier, all of the fields in the transaction (except the signatures),
// and output index.
func (t Transaction) I3vcoinOutputID(i uint64) I3vcoinOutputID {
	// Create the id.
	var id I3vcoinOutputID
	h := crypto.NewHash()
	h.Write(SpecifierI3vcoinOutput[:])
	t.marshalI3vNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])
	return id
}

// FileContractID returns the ID of a file contract at the given index, which
// is calculated by hashing the concatenation of the FileContract Specifier,
// all of the fields in the transaction (except the signatures), and the
// contract index.
func (t Transaction) FileContractID(i uint64) FileContractID {
	var id FileContractID
	h := crypto.NewHash()
	h.Write(SpecifierFileContract[:])
	t.marshalI3vNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])
	return id
}

// I3vfundOutputID returns the ID of a I3vfundOutput at the given index, which
// is calculated by hashing the concatenation of the I3vfundOutput Specifier,
// all of the fields in the transaction (except the signatures), and output
// index.
func (t Transaction) I3vfundOutputID(i uint64) I3vfundOutputID {
	var id I3vfundOutputID
	h := crypto.NewHash()
	h.Write(SpecifierI3vfundOutput[:])
	t.marshalI3vNoSignatures(h) // Encode non-signature fields into hash.
	encoding.WriteUint64(h, i)  // Writes index of this output.
	h.Sum(id[:0])
	return id
}

// I3vcoinOutputSum returns the sum of all the i3vcoin outputs in the
// transaction, which must match the sum of all the i3vcoin inputs. I3vcoin
// outputs created by storage proofs and i3vfund outputs are not considered, as
// they were considered when the contract responsible for funding them was
// created.
func (t Transaction) I3vcoinOutputSum() (sum Currency) {
	// Add the i3vcoin outputs.
	for _, sco := range t.I3vcoinOutputs {
		sum = sum.Add(sco.Value)
	}

	// Add the file contract payouts.
	for _, fc := range t.FileContracts {
		sum = sum.Add(fc.Payout)
	}

	// Add the miner fees.
	for _, fee := range t.MinerFees {
		sum = sum.Add(fee)
	}

	return
}

// I3vClaimOutputID returns the ID of the I3vcoinOutput that is created when
// the i3vfund output is spent. The ID is the hash the I3vfundOutputID.
func (id I3vfundOutputID) I3vClaimOutputID() I3vcoinOutputID {
	return I3vcoinOutputID(crypto.HashObject(id))
}
