package typesutil

import (
	"fmt"
	"strings"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/types"
)

// SprintTxnWithObjectIDs creates a string representing this Transaction in human-readable form with all
// object IDs included to allow for easy dependency matching (by humans) in
// debug-logs.
func SprintTxnWithObjectIDs(t types.Transaction) string {
	var str strings.Builder
	txIDString := crypto.Hash(t.ID()).String()
	fmt.Fprintf(&str, "\nTransaction ID: %s", txIDString)

	if len(t.I3vcoinInputs) != 0 {
		fmt.Fprintf(&str, "\nI3vcoinInputs:\n")
		for i, input := range t.I3vcoinInputs {
			parentIDString := crypto.Hash(input.ParentID).String()
			fmt.Fprintf(&str, "\t%d: %s\n", i, parentIDString)
		}
	}
	if len(t.I3vcoinOutputs) != 0 {
		fmt.Fprintf(&str, "I3vcoinOutputs:\n")
		for i := range t.I3vcoinOutputs {
			oidString := crypto.Hash(t.I3vcoinOutputID(uint64(i))).String()
			fmt.Fprintf(&str, "\t%d: %s\n", i, oidString)
		}
	}
	if len(t.FileContracts) != 0 {
		fmt.Fprintf(&str, "FileContracts:\n")
		for i := range t.FileContracts {
			fcIDString := crypto.Hash(t.FileContractID(uint64(i))).String()
			fmt.Fprintf(&str, "\t%d: %s\n", i, fcIDString)
		}
	}
	if len(t.FileContractRevisions) != 0 {
		fmt.Fprintf(&str, "FileContractRevisions:\n")
		for _, fcr := range t.FileContractRevisions {
			parentIDString := crypto.Hash(fcr.ParentID).String()
			fmt.Fprintf(&str, "\t%d, %s\n", fcr.NewRevisionNumber, parentIDString)
		}
	}
	if len(t.StorageProofs) != 0 {
		fmt.Fprintf(&str, "StorageProofs:\n")
		for _, sp := range t.StorageProofs {
			parentIDString := crypto.Hash(sp.ParentID).String()
			fmt.Fprintf(&str, "\t%s\n", parentIDString)
		}
	}
	if len(t.I3vfundInputs) != 0 {
		fmt.Fprintf(&str, "I3vfundInputs:\n")
		for i, input := range t.I3vfundInputs {
			parentIDString := crypto.Hash(input.ParentID).String()
			fmt.Fprintf(&str, "\t%d: %s\n", i, parentIDString)
		}
	}
	if len(t.I3vfundOutputs) != 0 {
		fmt.Fprintf(&str, "I3vfundOutputs:\n")
		for i := range t.I3vfundOutputs {
			oidString := crypto.Hash(t.I3vfundOutputID(uint64(i))).String()
			fmt.Fprintf(&str, "\t%d: %s\n", i, oidString)
		}
	}
	return str.String()
}
