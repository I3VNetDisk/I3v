package typesutil

import (
	"fmt"

	"gitlab.com/I3VNetDisk/I3v/types"

	"gitlab.com/I3VNetDisk/errors"
)

var (
	// AnyoneCanSpendUnlockHash is the unlock hash of unlock conditions that are
	// trivially spendable.
	AnyoneCanSpendUnlockHash types.UnlockHash = types.UnlockConditions{}.UnlockHash()
)

var (
	// ErrI3vcoinSourceAlreadyAdded is the error returned when a user tries to
	// provide the same source i3vcoin input multiple times.
	ErrI3vcoinSourceAlreadyAdded = errors.New("source i3vcoin input has already been used")

	// ErrI3vcoinInputAlreadyUsed warns a user that a i3vcoin input has already
	// been used in the transaction graph.
	ErrI3vcoinInputAlreadyUsed = errors.New("cannot use the same i3vcoin input twice in a graph")

	// ErrNoSuchI3vcoinInput warns a user that they are trying to reference a
	// i3vcoin input which does not yet exist.
	ErrNoSuchI3vcoinInput = errors.New("no i3vcoin input exists with that index")

	// ErrI3vcoinInputsOutputsMismatch warns a user that they have constructed a
	// transaction which does not spend the same amount of i3vcoins that it
	// consumes.
	ErrI3vcoinInputsOutputsMismatch = errors.New("i3vcoin input value to transaction does not match i3vcoin output value of transaction")
)

// i3vcoinInput defines a i3vcoin input within the transaction graph, containing
// the input itself, the value of the input, and a flag indicating whether or
// not the input has been used within the graph already.
type i3vcoinInput struct {
	input types.I3vcoinInput
	used  bool
	value types.Currency
}

// TransactionGraph is a helper tool to allow a user to easily construct
// elaborate transaction graphs. The transaction tool will handle creating valid
// transactions, providing the user with a clean interface for building
// transactions.
type TransactionGraph struct {
	// A map that tracks which source inputs have been consumed, to double check
	// that the user is not supplying the same source inputs multiple times.
	usedI3vcoinInputSources map[types.I3vcoinOutputID]struct{}

	i3vcoinInputs []i3vcoinInput

	transactions []types.Transaction
}

// SimpleTransaction specifies what outputs it spends, and what outputs it
// creates, by index. When passed in TransactionGraph, it will be automatically
// transformed into a valid transaction.
//
// Currently, there is only support for I3vcoinInputs, I3vcoinOutputs, and
// MinerFees, however the code has been structured so that support for I3vfunds
// and FileContracts can be easily added in the future.
type SimpleTransaction struct {
	I3vcoinInputs  []int            // Which inputs to use, by index.
	I3vcoinOutputs []types.Currency // The values of each output.

	/*
		I3vfundInputs  []int            // Which inputs to use, by index.
		I3vfundOutputs []types.Currency // The values of each output.

		FileContracts         int   // The number of file contracts to create.
		FileContractRevisions []int // Which file contracts to revise.
		StorageProofs         []int // Which file contracts to create proofs for.
	*/

	MinerFees []types.Currency // The fees used.

	/*
		ArbitraryData [][]byte // Arbitrary data to include in the transaction.
	*/
}

// AddI3vcoinSource will add a new source of i3vcoins to the transaction graph,
// returning the index that this source can be referenced by. The provided
// output must have the address AnyoneCanSpendUnlockHash.
//
// The value is used as an input so that the graph can check whether all
// transactions are spending as many i3vcoins as they create.
func (tg *TransactionGraph) AddI3vcoinSource(scoid types.I3vcoinOutputID, value types.Currency) (int, error) {
	// Check if this scoid has already been used.
	_, exists := tg.usedI3vcoinInputSources[scoid]
	if exists {
		return -1, ErrI3vcoinSourceAlreadyAdded
	}

	i := len(tg.i3vcoinInputs)
	tg.i3vcoinInputs = append(tg.i3vcoinInputs, i3vcoinInput{
		input: types.I3vcoinInput{
			ParentID: scoid,
		},
		value: value,
	})
	tg.usedI3vcoinInputSources[scoid] = struct{}{}
	return i, nil
}

// AddTransaction will add a new transaction to the transaction graph, following
// the guide of the input. The indexes of all the outputs created will be
// returned.
func (tg *TransactionGraph) AddTransaction(st SimpleTransaction) (newI3vcoinInputs []int, err error) {
	var txn types.Transaction
	var totalIn types.Currency
	var totalOut types.Currency

	// Consume all of the inputs.
	for _, sci := range st.I3vcoinInputs {
		if sci >= len(tg.i3vcoinInputs) {
			return nil, ErrNoSuchI3vcoinInput
		}
		if tg.i3vcoinInputs[sci].used {
			return nil, ErrI3vcoinInputAlreadyUsed
		}
		txn.I3vcoinInputs = append(txn.I3vcoinInputs, tg.i3vcoinInputs[sci].input)
		totalIn = totalIn.Add(tg.i3vcoinInputs[sci].value)
	}

	// Create all of the outputs.
	for _, scov := range st.I3vcoinOutputs {
		txn.I3vcoinOutputs = append(txn.I3vcoinOutputs, types.I3vcoinOutput{
			UnlockHash: AnyoneCanSpendUnlockHash,
			Value:      scov,
		})
		totalOut = totalOut.Add(scov)
	}

	// Add all of the fees.
	txn.MinerFees = st.MinerFees
	for _, fee := range st.MinerFees {
		totalOut = totalOut.Add(fee)
	}

	// Check that the transaction is consistent.
	if totalIn.Cmp(totalOut) != 0 {
		valuesErr := fmt.Errorf("total input: %s, total output: %s", totalIn, totalOut)
		extendedErr := errors.Extend(ErrI3vcoinInputsOutputsMismatch, valuesErr)
		return nil, extendedErr
	}

	// Update the set of i3vcoin inputs that have been used successfully. This
	// must be done after all error checking is complete.
	for _, sci := range st.I3vcoinInputs {
		tg.i3vcoinInputs[sci].used = true
	}
	tg.transactions = append(tg.transactions, txn)
	for i, sco := range txn.I3vcoinOutputs {
		newI3vcoinInputs = append(newI3vcoinInputs, len(tg.i3vcoinInputs))
		tg.i3vcoinInputs = append(tg.i3vcoinInputs, i3vcoinInput{
			input: types.I3vcoinInput{
				ParentID: txn.I3vcoinOutputID(uint64(i)),
			},
			value: sco.Value,
		})
	}
	return newI3vcoinInputs, nil
}

// Transactions will return the transactions that were built up in the graph.
func (tg *TransactionGraph) Transactions() []types.Transaction {
	return tg.transactions
}

// NewTransactionGraph will return a blank transaction graph that is ready for
// use.
func NewTransactionGraph() *TransactionGraph {
	return &TransactionGraph{
		usedI3vcoinInputSources: make(map[types.I3vcoinOutputID]struct{}),
	}
}
