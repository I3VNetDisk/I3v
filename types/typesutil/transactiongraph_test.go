package typesutil

import (
	"testing"

	"gitlab.com/I3VNetDisk/I3v/types"

	"gitlab.com/I3VNetDisk/errors"
)

// TestTransactionGraph will check that the basic construction of a transaction
// graph works as expected.
func TestTransactionGraph(t *testing.T) {
	// Make a basic transaction.
	var source types.I3vcoinOutputID
	tg := NewTransactionGraph()
	index, err := tg.AddI3vcoinSource(source, types.I3vcoinPrecision.Mul64(3))
	if err != nil {
		t.Fatal(err)
	}
	_, err = tg.AddI3vcoinSource(source, types.I3vcoinPrecision.Mul64(3))
	if !errors.Contains(err, ErrI3vcoinSourceAlreadyAdded) {
		t.Fatal("should not be able to add the same i3vcoin input source multiple times")
	}
	newIndexes, err := tg.AddTransaction(SimpleTransaction{
		I3vcoinInputs:  []int{index},
		I3vcoinOutputs: []types.Currency{types.I3vcoinPrecision.Mul64(2)},
		MinerFees:      []types.Currency{types.I3vcoinPrecision},
	})
	if err != nil {
		t.Fatal(err)
	}
	txns := tg.Transactions()
	if len(txns) != 1 {
		t.Fatal("expected to get one transaction")
	}
	// Check that the transaction is standalone valid.
	err = txns[0].StandaloneValid(0)
	if err != nil {
		t.Fatal("transactions produced by graph should be valid")
	}

	// Try to build a transaction that has a value mismatch, ensure there is an
	// error.
	_, err = tg.AddTransaction(SimpleTransaction{
		I3vcoinInputs:  []int{newIndexes[0]},
		I3vcoinOutputs: []types.Currency{types.I3vcoinPrecision.Mul64(2)},
		MinerFees:      []types.Currency{types.I3vcoinPrecision},
	})
	if !errors.Contains(err, ErrI3vcoinInputsOutputsMismatch) {
		t.Fatal("An error should be returned when a transaction's outputs and inputs mismatch")
	}
	_, err = tg.AddTransaction(SimpleTransaction{
		I3vcoinInputs:  []int{2},
		I3vcoinOutputs: []types.Currency{types.I3vcoinPrecision},
		MinerFees:      []types.Currency{types.I3vcoinPrecision},
	})
	if !errors.Contains(err, ErrNoSuchI3vcoinInput) {
		t.Fatal("An error should be returned when a transaction spends a missing input")
	}
	_, err = tg.AddTransaction(SimpleTransaction{
		I3vcoinInputs:  []int{0},
		I3vcoinOutputs: []types.Currency{types.I3vcoinPrecision},
		MinerFees:      []types.Currency{types.I3vcoinPrecision},
	})
	if !errors.Contains(err, ErrI3vcoinInputAlreadyUsed) {
		t.Fatal("Error should be returned when a transaction spends an input that has been spent before")
	}

	// Build a correct second transaction, see that it validates.
	_, err = tg.AddTransaction(SimpleTransaction{
		I3vcoinInputs:  []int{newIndexes[0]},
		I3vcoinOutputs: []types.Currency{types.I3vcoinPrecision},
		MinerFees:      []types.Currency{types.I3vcoinPrecision},
	})
	if err != nil {
		t.Fatal("Transaction was built incorrectly", err)
	}
}
