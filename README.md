# [![I3v Logo](https://i3v.tech/static/assets/svg/i3v-green-logo.svg)](http://i3v.tech)

[![Build Status](https://gitlab.com/I3VNetDisk/I3v/badges/master/build.svg)](https://gitlab.com/I3VNetDisk/I3v/commits/master)
[![Coverage Report](https://gitlab.com/I3VNetDisk/I3v/badges/master/coverage.svg)](https://gitlab.com/I3VNetDisk/I3v/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/I3VNetDisk/I3v?status.svg)](https://godoc.org/gitlab.com/I3VNetDisk/I3v)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/I3VNetDisk/I3v)](https://goreportcard.com/report/gitlab.com/I3VNetDisk/I3v)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

I3v is a decentralized cloud storage platform that radically alters the
landscape of cloud storage. By leveraging smart contracts, client-side
encryption, and sophisticated redundancy (via Reed-Solomon codes), I3v allows
users to safely store their data with hosts that they do not know or trust.
The result is a cloud storage marketplace where hosts compete to offer the
best service at the lowest price. And since there is no barrier to entry for
hosts, anyone with spare storage capacity can join the network and start
making money.

![UI](https://i.imgur.com/BFMQwhg.png)

Traditional cloud storage has a number of shortcomings. Users are limited to a
few big-name offerings: Google, Microsoft, Amazon. These companies have little
incentive to encrypt your data or make it easy to switch services later. Their
code is closed-source, and they can lock you out of your account at any time.

We believe that users should own their data. I3v achieves this by replacing
the traditional monolithic cloud storage provider with a blockchain and a
swarm of hosts, each of which stores an encrypted fragment of your data. Since
the fragments are redundant, no single host can hold your data hostage: if
they jack up their price or go offline, you can simply download from a
different host. In other words, trust is removed from the equation, and
switching to a different host is painless. Stripped of these unfair
advantages, hosts must compete solely on the quality and price of the storage
they provide.

I3v can serve as a replacement for personal backups, bulk archiving, content
distribution, and more. For developers, I3v is a low-cost alternative to
Amazon S3. Storage on I3v is a full order of magnitude cheaper than on S3,
with comparable bandwidth, latency, and durability. I3v works best for static
content, especially media like videos, music, and photos.

Distributing data across many hosts automatically confers several advantages.
The most obvious is that, just like BitTorrent, uploads and downloads are
highly parallel. Given enough hosts, I3v can saturate your bandwidth. Another
advantage is that your data is spread across a wide geographic area, reducing
latency and safeguarding your data against a range of attacks.

It is important to note that users have full control over which hosts they
use. You can tailor your host set for minimum latency, lowest price, widest
geographic coverage, or even a strict whitelist of IP addresses or public
keys.

At the core of I3v is a blockchain that closely resembles Bitcoin.
Transactions are conducted in I3vcoin, a cryptocurrency. The blockchain is
what allows I3v to enforce its smart contracts without relying on centralized
authority. To acquire i3vcoins, use an exchange such as [Binance](https://binance.com), [Bittrex](https://bittrex.com), [Shapeshift](https://shapeshift.io), or
[Poloniex](https://poloniex.com).

To get started with I3v, check out the guides below:

- [Storing files with I3v-UI](https://blog.i3v.tech/a-guide-to-i3v-ui-v1-4-0-7ec3dfcae35a)
- [How to Store Data on I3v](https://blog.i3v.tech/getting-started-with-private-decentralized-cloud-storage-c9565dc8c854)
- [How to Become a I3v Host](https://blog.i3v.tech/how-to-run-a-host-on-i3v-2159ebc4725)
- [Using the I3v API](https://blog.i3v.tech/api-quickstart-guide-f1d160c05235)


Usage
-----

I3v is ready for use with small sums of money and non-critical files, but
until the network has a more proven track record, we advise against using it
as a sole means of storing important data.

This release comes with 2 binaries, i3vd and i3vc. i3vd is a background
service, or "daemon," that runs the I3v protocol and exposes an HTTP API on
port 9980. i3vc is a command-line client that can be used to interact with
i3vd in a user-friendly way. There is also a graphical client, [I3v-UI](https://gitlab.com/I3VNetDisk/I3v-UI), which
is the preferred way of using I3v for most users. For interested developers,
the i3vd API is documented at [i3v.tech/docs](https://i3v.tech/docs/).

i3vd and i3vc are run via command prompt. On Windows, you can just double-
click i3vd.exe if you don't need to specify any command-line arguments.
Otherwise, navigate to its containing folder and click File->Open command
prompt. Then, start the i3vd service by entering `i3vd` and pressing Enter.
The command prompt may appear to freeze; this means i3vd is waiting for
requests. Windows users may see a warning from the Windows Firewall; be sure
to check both boxes ("Private networks" and "Public networks") and click
"Allow access." You can now run `i3vc` (in a separate command prompt) or I3v-
UI to interact with i3vd. From here, you can send money, upload and download
files, and advertise yourself as a host.

Building From Source
--------------------

To build from source, [Go 1.13 or above must be installed](https://golang.org/doc/install)
on the system. Clone the repo and run `make`:

```
git clone https://gitlab.com/I3VNetDisk/I3v
cd I3v && make
```

This will install the `i3vd` and `i3vc` binaries in your `$GOPATH/bin` folder.
(By default, this is `$HOME/go/bin`.)

You can also run `make test` and `make test-long` to run the short and full test
suites, respectively. Finally, `make cover` will generate code coverage reports
for each package; they are stored in the `cover` folder and can be viewed in
your browser.

### Running I3v on a Raspberry Pi

Official binaries are not provided for the Raspberry Pi, but you can easily
compile them yourself by installing the Go toolchain on your Raspberry Pi.
Alternatively, you can cross-compile by running `GOOS=linux GOARCH=arm64 make`.
Raspberry Pi compatible binaries will then be installed in
`$GOPATH/bin/linux_arm64/`.
