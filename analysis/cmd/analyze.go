package main

import (
	"gitlab.com/I3VNetDisk/I3v/analysis/jsontag"
	"gitlab.com/I3VNetDisk/I3v/analysis/lockcheck"
	"gitlab.com/I3VNetDisk/I3v/analysis/responsewritercheck"
	"golang.org/x/tools/go/analysis/multichecker"
)

func main() {
	multichecker.Main(
		lockcheck.Analyzer,
		responsewritercheck.Analyzer,
		jsontag.Analyzer,
	)
}
