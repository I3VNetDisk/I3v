# I3v Resources

#### Table of Contents
* [Tech Docs](#tech-docs)
    * [Module README's](#module-readme's)
    * [API](#api)
    * [Support Docs](#support-docs)
* [I3v Third Party Apps](#i3v-third-party-apps)
    * [Mining Pools](#mining-pools)
    * [Network Stats](#network-stats)
    * [Built On I3v](#built-on-i3v)
    * [Integrations](#integrations)

## Tech Docs
Here is a list of technical documentation to help understand the different parts of the I3v code base.

### Module README's
* Consensus - `coming soon`
* Explorer - `coming soon`
* Gateway - `coming soon`
* Host - `coming soon`
    * Contract Manager - `coming soon`
* Miner - `coming soon`
* [Renter](../modules/renter/README.md)
    * Contractor - `coming soon`
    * HostDB - `coming soon`
    * Proto - `coming soon`
    * I3vDir - `coming soon`
    * I3vFile - `coming soon`
* Transaction Pool - `coming soon`
* Wallet - `coming soon`

### API
The API documentation can be found [here](api/index.html.md) and at [i3v.tech/docs](https://i3v.tech/docs).

### Support Docs
If you are looking for more how to guides or some more general information, check out our [Support Docs](https://support.i3v.tech) 
and [Learn About I3v](https://i3v.tech/learn).

## I3v Third Party Apps
Below is a list of examples of third party apps that are integrated or built on top of I3v.

### Mining Pools
* [Luxor Mining](https://mining.luxor.tech/i3v)
* [I3vMining](https://i3vmining.com/)

### Network Stats
* [Explorer](https://explore.i3v.tech)
* [I3vHub](https://i3vhub.info)
* [I3vSetup](https://i3vsetup.info)
* [I3vStats](https://i3vstats.info)

### Built on I3v
* [Filebase](https://filebase.com)
* [GooBox](https://goobox.io)
* [Luxor](https://www.luxor.tech/)
* [Minebox](https://minebox.io)
* [Obelisk](https://obelisk.tech)
* [PixelDrain](https://i3v.pixeldrain.com)
* [Repertory](https://bitbucket.org/blockstorage/repertory/src/master/)
* [I3vDrive](https://bitbucket.org/i3vextensions/i3vdrive/src/master/)
* [I3vSync](https://github.com/tbenz9/i3vsync)

### Integrations
* [Duplicati](https://blog.i3v.tech/introducing-full-computer-backup-with-i3v-through-the-new-duplicati-integration-62dd17cbcfb7)
* [Minio](https://blog.i3v.tech/introducing-s3-style-file-sharing-for-i3v-through-the-new-minio-integration-bb880af2366a)
* [Nextcloud](https://blog.i3v.tech/using-i3v-as-a-storage-back-end-for-nextcloud-90eab037959d)

## Contact Us 
Feel free to ask for help in the I3v [discord][discord].

[discord]: https://discord.gg/i3v
