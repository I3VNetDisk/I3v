## Verifying Release Signatures

If a verification step fails, please contact https://support.i3v.tech/ for
additional information or to find a support contact before using the binary 

1. First you need to download and import the correct `gpg` key. This key will not be changed without advanced notice.
  - `wget -c https://gitlab.com/I3VNetDisk/I3v/raw/master/doc/developer-pubkeys/i3v-signing-key.asc`
  - `gpg --import i3v-signing-key.asc`

2. Download the `SHA256SUMS` file for the release.
  - `wget -c http://gitlab.com/I3VNetDisk/I3v/raw/master/doc/I3v-1.x.x-SHA256SUMS.txt.asc`

3. Verify the signature.
   - `gpg --verify I3v-1.x.x-SHA256SUMS.txt.asc`
   
   **If the output of that command fails STOP AND DO NOT USE THAT BINARY.**

4. Hash your downloaded binary.
  - `shasum256 ./i3vd` or similar, providing the path to the binary as the first argument.
	 
   **If the output of that command is not found in `I3v-1.x.x-SHA256SUMS.txt.asc` STOP AND DO NOT USE THAT BINARY.**
